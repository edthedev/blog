Artemis Space Ship Simulator
===============================
:date: 2014-01-28

About Artemis
--------------

Artemis_ - Spaceship Bridge Simulator is pretty amazing. Normally I think of card and board games as being the ideal party games. While I love LAN parties, they tend to exclude people who don't own a dedicated gaming computer. 

Artemis_ avoids this exclusion because anyone can play with just a smartphone or tablet. Each smartphone or tablet acts as a single space ship bridge station, and requires constant critical information from the other stations in order to function. This keeps communication between players high - making it a true party game.

In a clever twist, many stations can produce low quality approximations of the critical information available from other stations.
So while the helmsman is waiting for a heading from the navigator, the science officer might not-so-helpfully pipe up that 'you're going almost exactly the wrong way right now.' Even basic tasks require cooperation from at least three stations. 

Gameplay Example
-----------------

Perhaps the weapons officer mentions that the ship is low on torpedoes. Communications comes to the rescue - she has some being assembled at Deep Space 3. The science officer somewhat-helpfully describes Deep Space 3 as 'to your left a bit', and navigation eventually calls out a specific course and distance, which helm then executes. 

A few moments later, Engineering finishes some mental math based on the distance and points out that the ship does not actually have enough fuel to reach Deep Space 3. Navigation to the rescue - Deep Space 1 can refuel us and is more or less on the way. 

Eventaully this chain of cooperation is being repeated efficiently. I, as the helmsman, decided that 'north-ish' was a pretty good heading to start with, asked for extra power to engines from engineering, and used it to promptly run into a minefield while cruising at warp 4. All the pretty lights started flashing red.

The engineer yelled 'hang on, I've got this' a few times, while everyone else tried to figure out if anything at their station still worked. I tried to avoid making eye contact with anyone as I stealthily set the speed back down to 0, but it made no difference really. The engineer had prudently cut power to my station a few moments after we collided with the space mine. 

The engineer quickly got impulse power back to 20%, and then started working on making things stop exploding. While we were basically dead in the water, at least communications were unharmed, and able to let us know that Deep Space 4 desperately needed us to come defend them from an attack.

Conclusion
-----------

Artemis_ has an exceptional ability to produce memorable moments with fellow players. Highly recommended.
It is easily one of the best party games I have played. 

.. _Artemis: http://www.artemis.eochu.com/
