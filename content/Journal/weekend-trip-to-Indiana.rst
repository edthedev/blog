Our weekend trip to Indiana
===========================
:date: 2012-12-09
:status: draft

Joanna picked me up from work on Friday with the car packed with overnight bags and vuvuzelas; and we headed dierctly to University of Indiana for the Illinois/Indiana hockey game. It was a blast. We stayed overnight at the TravelLodge, and went to the second game the following afternoon.

This was Teddy's first time staying at a hotel. We've done plenty of travel since he was born, but have always been fortunate to have a friend or relative to stay with at our destination.  Teddy was anxious about staying at the hotel overnight.

Teddy's Grandparents recently moved to town, and Teddy was impressed by the idea that a house change can be permanent. I think Teddy may have worried that the tiny hotel room might become a habit.

Afer checking in at the hotel, we went out shopping for food, and I bought some Legos. Returning with full bellies and a Lego set to unwrap, the hotel finally seemed like a perfectly fine place to all of us.

The next morning, Teddy was happy, but still chattering quite a bit about going home to Teddy's house. He seemed mostly concerned with making sure that Teddy's house was still our final destination. I explained our itinerary for the day: breakfast, shopping, hockey and then home; and Teddy relaxed.

Teddy's patience still runs about ten minutes shorter than the average hockey game, but when the figure skaters who were on next started practicing jumping and spinning in the lobby, Ted was perfectly content to stay for a few more minutes.

As we loaded into the car for the trip home, I told Teddy that we were heading home now. Teddy replied by asking for 'Double, fries and cookies!'. He knows that we usually buy lunch off of the McDonalds dollar menu during long car drives. This particular request was impressive, because in the past he would normally just announce one item from the menu, but this time he carefully listed each thing he wanted. 

Teddy took an afternoon nap during most of the trip home. To break up the trip, we stopped for a walk at a random Walmart on the way back, but accidentally hit rush hour, so I wasn't willing to wait in line to buy the 97 cent book of crossword puzzles we had picked out. Teddy had found a pink barbie shopping cart and pushed it happily around the entire store.

We got home to a surprisingly empty house. It was not very late, but we were tired from driving, so we just had a nice long relaxing bedtime routine, with extra showers to wash away the road weariness*.

* 'Road weariness' is a polite term for 'potato chip fragments' and 'that ketchup stain that you only get if you eat out when you only packed a single pair of pants'.

