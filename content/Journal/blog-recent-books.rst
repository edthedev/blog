Recent Books
=================
:date: 2013-02-03

A Princess of Mars
------------------

I became interested in this book after the movie John Carter of Mars flopped in the box office last year. After finishing the book, I am very excited to see the movie. Although, it does look like the movie may inlcude books two and three, which I have not read yet. So I will try to finish those before seeing the movie, to avoid spoilers.

I don't normally care about spoilers, but this book kept me on the edge of my seat. I not only really liked the characters, but I genuinely believed the author would allow both joy and tradgedy to happen to them. Plenty of sci-fi accomplishes this, but this book did a really exceptional job of setting up exceptionally engaging moments.

I was describing this book to my wife and she said 'Oh, so it's like the Klingon Emprie'. Somehow the aliens in this story manage to be very alien, but very sympathetic at the same time.

Price: This one is free on Project Gutenberg. Recommended for the avid reader.

