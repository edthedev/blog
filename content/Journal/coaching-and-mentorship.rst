coaching and mentorship
=======================
:date: 2013-10-07
:status: draft

Building a successful technical skills team has a lot in common with coaching.

TODO: Add link to Google study.

What I learned from coaching:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Enjoyment comes from winning, 
winning grows from practice, 
practice happens due to wanting to do it (confidence)
wanting to do it grows from lots of positive feedback. 

How to successfully provide positive feedback
----------------------------------------------
1. Say it, say it, say it again. 
Repetition works, and attitudes rub off. You need confident players (practice grows from wanting to do it), so you need to express your confidence in them, over and over and over and over again.

2. Never ever provide a single insincere piece of positive feedback.
If you lose your credibility, all is lost.
This means that in order to successfully follow rule 1, you need to be paying attention, and never missing an opportunity.

3. Show off to have a valid opinion. 
If you never had credibility in the first place, all is still lost.
Credibility comes from showing you have the skills you are trying to mentor. Authority cannot substitute for credibility.
When the going gets tough, the tough show off a little.
"But what do I do when I don't *have* the skill I'm trying to mentor?"

There are going to be gaps in your skillset, and there are going to be new challenges you never faced before. Admitting that you have weaknesses, and that you're relying on your team to overcome those weaknesses, decreases your credibility, but it directly *increases* your team's *confidence* which is your end goal anyway. 

But, when you admit that you don't have a skill that your team needs, your team's confidence will only be as strong as their belief in an alternative safety net.

For new team members, that safety net may simply be a trusted senior team member.
For senior team members, the safety net often includes: an exploration phase of the project, a rollback plan, and a backup plan.
You do take the time to create these, don't you? If you have confident team members, someone is doing these things. You can benefit from sharing this plan with your team, unless it turns out that the plan is just to find a job elsewhere...
