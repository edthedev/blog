Recent Board Games
==================
:date: 2013-02-03

Flashpoint
----------

Theme: Perfect. Play as a firefighter. Rescue people and small animals. Put out fires. Dodge explosions. Run away before the building collapses.

Replay Value: I have only played the simple rules, and that only once. It plays great so far, and I suspect it will hold it's own alongside Forbidden Island. 

Quality: The art is consistently great, and the pieces are sturdy plastic and cardboard. If a deluxe edition is released, I would consider picking it up.

Price/Value: I paid $40 for this game. It comes in a sturdy and beautiful box, contains lots of pieces, and the game delivers on the theme. Recommended.

Carcassonne
-----------

Theme: Assemble tiles to build castles, roads, fields and monestaries.

Replay Value: This is the game that my wife and I have played the most total times. It's not a game we play back-to-back sessions of, but it gets played very frequently over the long run.

Quality: The box art is a bit dated, but that is because I have had my copy forever. The meeples are simple, but good quality. The tiles and paint have held up very well across many games. This is another game that, due to the frequency of play, I might spring for a deluxe version if one was available.

Price/Value: The game plays perfectly without expansions, but the game itself and several expansions are still cheaper than the base game of it's competitors like Dominion. My favorite expansions are the River and River II, since they add to the core (pretty tiles) without substantially altering the gameplay.

Forbidden Island
----------------

Theme: Gather the lost artifacts and get off the island before it sinks. It really does start to feel like the last few minutes of an action movie.

Replay Value: Very high. It's quick, it's exciting, it doesn't repeat too much, and it makes an excellent ice-breaker for groups that do not want to commit to a longer game.

Quality: Surprisingly nice. The pictures on the box don't do this one justice.

Price/Value: At $15, this was a great deal. I use it routinely to start off a game night, and it has generally always been a hit.

Dominion Prosperity
-------------------
Theme: Lot's of good combinations make you feel like you're winning at Monopoly. Buy cards and then play them to buy more cards. This is the get-rish-quick set, so the game goes faster than usual. It detracts from Dominion's normal 'responsibly build an empire' theme, but that's sort of the whole point of having a 'get rich qucik' expansion.

Replay Value: Dominion is the most replayable game I own, and this expansion keeps with the theme. I'm behind quite a few expansiosn, and plan to catch up; but I'm able to take my time picking them up because each is exceptionally replayable.

Quality: The first couple Dominion sets broke character on some cards by including very cartoon art among otherwise serious and momentous art. This expansion does a better job keeping the tone consistent - a nice improvement.

Price/Value: As my most played game, and the one I have spent the most on, I still intend to buy a lot more expansions. This game is worth every penny.

Lords of Waterdeep
-------------------

Theme: This game feels like it was meant to be a licensed Discworld product. I really enjoy the theme, and it is well executed; but only other Discworld fans really have grokked the theme. 

Quality: The parts are great, the art is great, and the box is great. The only change I would have liked would have been meeples instead of cubes for the different recruit markers. As is, most players don't 'recruit a warrior' by mid-game, they just 'buy an orange cube'.

Replay Value: We have played this game a lot since we bought it. Even players who do not get the theme still really enjoy the gameplay.

Price/Value: This game is not cheap, but it was a big win. It's great to have a deeper and longer game that is still actually enjoyed by new players and sees a lot of time on the game table.
