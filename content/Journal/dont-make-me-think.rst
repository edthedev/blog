Notes from reading the book 'Don't Make Me Think'
=================================================
:date: 2012-09-18

There is no substitute for testing. Put together some crude version of the product/site and watch ordinary people use it.

Two weeks before launch, or even two months before launch is late to start usability testing. 

Focus groups are not usability tests. - Focus groups are by nature interactive, usability testing is single and observed. Focus groups discover the abstract - usability tests discover the specific. Focus groups should be done before the first design, usability tests should be done as the design happens.

Usability testing sometimes settles arguments, but more often discovers that the arguments were about entirely the wrong things.

Testing is like having friends visit from out of town - you see the sights worth seeing, because you're showing it to their new eyes.

Testing with 1 user is 100% better than testing with none.

Have fewer testers per round, and more rounds of testing. Have at most three testers in a single round - they're going to stop at the most obvious problems, and not see past them - you need more rounds.

