Rollerblades, Skateboards and Software
===========================================
:date: 2014-02-10
:status: published

As a young skater, there was a time when the only destination worth considering on a Saturday was the skate park, and the only questions were how early we could get there and how late we could stay. 

We were working on the crucial most-important-ever trick; and we knew that it wa not going to land without hours of practice. Every trick was that important, because each one was impossible until it was suddenly old-hat. And then it was time to learn the next one.

I don't remember ever stopping to wonder 'why'. It was simply what we did: Find something nearly impossible and figure out how to do it. Then teach everyone else who could learn it. Then find another impossible thing.

I don't know if skating kindled the obsession, or the obsession led to the skating. Either way, it now serves me well as an IT professional, where there are plenty of impossible things to do.
