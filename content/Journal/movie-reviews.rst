Movie Reviews
==============
:status: draft
:date: 2014-07-06

A common theme in movies that I enjoy is 'Better living through technology'.
TODO: Dredd, Elysium 

RoboCop (2012)
---------------
I didn't expect much from this film. Everything about the trailer suggested a by-the-numbers takes-no-risks remake. 

The surprise to me was that removing the 1980s flavor from RoboCop, what's left is pure gold. I've heard it said that a piece of art is only truly finished when you cannot remove anything more without destroying it. 

2012 RoboCop removes many things from the RoboCop formula in order to reach a PG-13 rating. The impressive thing, to me, is how well the RoboCop story weathers the change. Sipmly put, it is still wonderful. I find myself glad to have a version of RoboCop that my son can watch as a young teenager.

The RoboCop armor in this film is black.

Two big changes are the human hand and the change of his armor color to black. For context, the silver RoboCop armor is a classic artistic accomplishment, that has weathered the ages and has absolutely no need to be reimagined. (I'm sure a careful analysis of the silver armor that appears in the film would reveal some extremely subtle updates to keep it fresh, but I wasn't able to spot any differences from the 1980s armor during two viewings.) 

Don't get me wrong: the new costume is a thing of beauty, and it will grow on you. But it's still an unneeded change to a classic design. 

The black RoboCop suit fits this reboot perfectly. I'm pretty sure the authors got together and dicsussed "What's the stupidest thing people do in reboots? Put the main character in a sleek black costume instead of the iconic favorite, obviously. Okay. Let's have the bad guy do that to RoboCop, then we can be sure the audience will hate him." 

I'm trying to avoid spoilers, but let's just say the film makes it clear that the film creators still love the silver armor best, and it's unlikely the black armor will appear in RoboCop II. Pick up your exclusive black RoboCop action figure now, it'll be a classic.

This retelling of RoboCop feels more grown up, but still has enjough child-like joy to have a lot of fun within it's premise.

But the thing that comes across during the film is - ruining a classic design is just one more evil deed that OCI won't shy away from. I can't say more without spoilers, but go watch the film, and you'll realize that no one was actually trying to improve on RoboCop's classic look. They just wanted to sell a second action figure and blame OCI for their greediness. To me, that feel very well aligned with the spirit of the story.

The thing is - none of this was an accident. The suit was a planned symbol of the remake itself. It's presence right at the center of the film loudly proclaims in every scene 'This is a modern remake, and this remake is better in ways that modern remakes can be, but it's worse in the ways that all remakes of classics must be. And we're okay with that, and we think you will be too.'

Story telling: The writing and story pace in this movie is essentially perfect. I signed up for a summer pop-corn film, and got a well told, through-provoking story as a bonus.

Special Effects: The RoboCop storyline still calls for levels of technology that are hard to realistically portray, even with today's special effects. One of the few flaws of this film is that it needed a lot of bullets striking a lot of different materials, and the subtle nuances between bullet strike sounds aren't completely there. I expect a 'Special Edition' in a couple decades to remaster the sound to fix this in the RoboCop I-III box set. If they do it, I'll buy it.

That said, there are a few special effects that really mattered in this film, and they were perfect. RoboCop moves and sound perfect. The ED-209 units move and sound perfect. And the rest of the effects do their respectable jobs, being background for these beautiful machines.

Acting: This movie feels like batting practice when each hitter steps up to the plate to take an easy pitch and try to send it out of the park. Each role called for something a bit different, but no one had to interact very deeply. Most of the cast play two dimensional bad guy OCI employees whose scenes just need to happen to progress the plot. This was never going to be Shakespear, but all of the performances are solid. Gary Oldman does the heavy lifting with one of his usual impossibly perfect performances. I like to think that RoboCop deserves a place in any 'Classic Gary Oldman Performances' collection, even while realizing such a collection would include a huge number of films.

Philosophy: RoboCop 2012 is a consumer product about consumerism, and it knows it. It's also a surprisingly mature movie. It takes complex discussions about consumerism vs freedom, and then tension between free market forces and free civil liberties - and it carves out a story that highlights both sides, and then - and this is the part where it shines - it just picks a good guy, and picks some bad guys - and stuff starts exploding. They could easily reboot this film as a TV series and really explore the nuances. I would actually love to watch that. But this film only had two hours, and so it didn't let itself get stuck in philosophical mode. The bad guys crossed the line from sympathetic to evil, and then got brought to justice.

Coming Soon: Pacific Rim


