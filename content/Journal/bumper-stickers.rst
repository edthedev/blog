Bumper Stickers
================
:date: 2014-07-08
:status: draft

Car bumper stickers aren't my style. I would rather the drivers near me focus on road safety.

My opinions aren't particularly well considered, and you probably don't want to hear them.

Except you're visiting this page, so you must be curious.

Politics
---------

International Politics: I was born into exceptional opportunity, and I take the corresponding responsibility seriously. My wife and I engage the world through Kiva.org microloans, sponsored kids through World Vision, and a regular amount of money set aside for international disaster relief. None of these are necessarily solutions, but I believe they make a difference. I think what Bill and Melinda Gates are doing in responsible giving is really cool.

National Politics: I live in close to proximity to Chicago Illinois, and my blue state votes blue in every presidential election, so by living in Illinois, that's my unintended influence over national politics. I'm not a fan of the current specific implementation of the U.S. electoral system, but I don't lose any sleep over it. 

Local Politics: My local community is doing some cool things, and this is somewhere I can continue to live for the foreseeable future. In particular, I'm a big fan of the long range transportation plan currently being done. My home owner's association is doing alright, as well. My wife and I regularly sponsor local elementary teacher's projects through DonorsChoose.org, and we finanically contribute to a local food pantry and shelter.

Civil Rights
-------------
A close friend encouraged me to participate in the LGBT Awareness 'Day of Silence'. It's difficult to express how moved I was by the experience. I don't feel that I'm currently doing my part in forwarding equal civil rights, but, but I hope to make a positive difference someday. Let me start by encouraging you to participate in a 'Day of Silence'.

Religion
---------
I was raised in the Christian church, and I try to keep my behavior in line with what I consider to be Judeo and Christian values. 

My beliefs about G-d and life after death are nuanced, probably wrong, and almost certainly differ greatly from yours. 

Science
--------
It works. We should all apply it more often.

Education
----------
It works. We should all invest more heavily in it in any context we can.

Programming
------------
The world needs more programmers than we can currently produce. We programmers are awesome, and a lot more people are capable of becoming programmers than ultimately do so. I like to believe that I'm helping turn this trend around in small ways every day.

Do you even lift?
-----------------
One of these days, I'm going to reclaim the term 'brogrammer' as a positive term for all of the good qualities of a person who applies the same problem solving drive it takes to write software to their own personal fitness. One of these days...
