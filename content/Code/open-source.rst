Open Source Projects
=====================
:date: 2014-01-31

Projects that I maintain
-------------------------

* Minion_ is a plugin that causes Vim_ to behave like a notebook.
* Vim-Todo_ is a plugin that quickly switches lines between TODO and DONE
* time_reporting_ is a command line interface to report time to the SOEEA Time Reporting Tool. Very useful if you already record your time in some other software.
* vim-pep8_ is a plugin that reconfigures Vim_ to encourage PEP8_ compliant Python_.
* pelican.vim_ is a plugin for working with Pelican_ blogs in Vim_.
* podcast-grabber_ is a command line tool to download Podcasts.
* vim-rst_ is a plugin for editing ReStructuredText_ in Vim_.
* incommon_ssl_ is a Python_ client library for the Comodo InCommon SSL Certificate API.
* Firefly_ is a tool for Windows and Mac to search for Social Security Numbers in common file formats. Used in the SSN Remediation project at University_of_Illinois_.

.. _University_of_Illinois: http://illinois.edu
.. _ReStructuredText: http://sphinx-doc.org/rest.html  
.. _Python: http://docs.python.org/2/tutorial/appetite.html 
.. _Vim: http://en.wikipedia.org/wiki/Vim_%28text_editor%29
.. _PEP8: http://www.python.org/dev/peps/pep-0008/ 

.. _Firefly: http://firefly.uiuc.edu/source/ 
.. _incommon_ssl: https://github.com/edthedev/incommon_ssl
.. _vim-rst: https://github.com/edthedev/vim-rst
.. _pelican.vim: https://github.com/edthedev/pelican.vim
.. _podcast-grabber: https://github.com/edthedev/podcast-grabber
.. _vim-pep8: https://github.com/edthedev/vim-pep8 
.. _time_reporting: https://github.com/edthedev/time_reporting  
.. _Vim-Todo: https://github.com/edthedev/vim-todo 
.. _Minion: https://github.com/edthedev/minion




Big projects I have contributed to in very minor ways
------------------------------------------------------
* Kivy_ is a cross platform library for creating graphical applications in Python_.
* Pelican_  converts ReStructuredText_ into modern static web sites.

.. _Pelican : http://docs.getpelican.com/en/3.3.0/
.. _kivy: http://kivy.org

