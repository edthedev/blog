rpm distutils python
====================
:date: 2013-07-29
:status: draft

For the RPM file, the .spec explodes the source tarball into a build directory::

    %setup

For the RPM file, the .spec repeats the::

    configure 
    make install

commands from it's own build root.
(See %configure in the .spec file)



Manually with distutils - set prefix and one other variable in the .spec file...


Idea::
    
    sdg_update_makefiles

OR::

    Python_compile_recursive 

which can be run by a single makefile in the top directory.

OR even better::

    ????

which is a standard industry command and can be run by the .spec file from the top directory with a single makefile...



