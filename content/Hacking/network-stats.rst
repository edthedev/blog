Network Statistics
===================
:date: 2013-07-11

I participated in a Ship_It_ day at CITES_ where we created a new Network_Statistics_Page_. It was great fun to work with talented people for a day to gather statistics about the University_of_Illinois_Network_.

.. _Ship_It: https://confluence.atlassian.com/display/DEV/ShipIt+Day+FAQ
.. _CITES: http://www.cites.illinois.edu/
.. _Network_Statistics_Page: http://netstat.cites.illinois.edu/
.. _University_of_Illinois_Network: http://www.cites.illinois.edu/maps
