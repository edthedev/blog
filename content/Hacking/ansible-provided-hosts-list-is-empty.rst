Vagrant with Ansible
====================================
:date: 2013-09-06

First we install Ansible, using apt.::

    # Ubuntu
    sudo apt-get install ansible

    # Debian
    sudo apt-get install python-dev python-pip
    sudo pip install ansible

    # Fedora Core
    sudo yum install python-dev python-pip
    sudo pip install ansible

Create a file called *playbook.yml* and add the following to it::

    ---
    - hosts: all
        tasks:
        - name: Download Vagrant
        get_url: url=http://files.vagrantup.com/packages/0224c6232394680971a69d94dd55a7436888c4cb/vagrant_1.3.   0_x86_64.deb dest=/tmp/vagrant.deb
        - name: Install Vagrant .deb 
        sudo: yes
        shell: sudo dpkg -i /tmp/vagrant.deb   

This playbook tells Ansible to download the Vagrant 1.3 Debian package for x86 64 bit computers, and then install it with dpkg.

To run this playbook on the local system, run the following::

    sudo ansible-playbook playbook.yml --connection=local

Oh, Ansible did not like that.::

    ansible provided hosts list is empty

This is because Ansible won't do anything with the local system unless it is added to the Ansible master hosts list file. Create the Ansible hosts file, if it does not already exist.::

    sudo mkdir -p /etc/ansible
    sudo touch /etc/ansible/hosts

Then open /etc/ansible/hosts and add localhost::

    [localhost]
    127.0.0.1

Since the playbook specified 'hosts: all', the 'localhost' that we just created will be included when the playbook runs.

Install Vagrant onto your Debian or Ubuntu computer by running the playbook in local mode::

    sudo ansible-playbook playbook.yml --connection=local

After entering your sudo password, you will see something like the following output, confirming that Ansible was able to successfully install Vagrant.::

    PLAY [all] ********************* 

    GATHERING FACTS ********************* 
    ok: [127.0.0.1]

    TASK: [Download Vagrant] ********************* 
    ok: [127.0.0.1]

    TASK: [Install Vagrant .deb] ********************* 
    changed: [127.0.0.1]

    PLAY RECAP ********************* 
    127.0.0.1                      : ok=3    changed=2    unreachable=0    failed=0
