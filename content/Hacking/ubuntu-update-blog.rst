Updating on Ubuntu got easier.
===============================
:date: 2014-01-24

Ubuntu's upgrade process has a very nice new feature::

    Configuration file '/etc/NetworkManager/NetworkManager.conf'
     ==> Modified (by you or by a script) since installation.
     ==> Package distributor has shipped an updated version.
       What would you like to do about it ?  Your options are:
        Y or I  : install the package maintainer's version
        N or O  : keep your currently-installed version
          D     : show the differences between the versions
          Z     : start a shell to examine the situation
     The default action is to keep your current version.
    *** NetworkManager.conf (Y/I/N/O/D/Z) [default=N] ?

This is a very good thing.
