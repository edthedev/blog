Project Minion
==============
:date: 2013-02-07

I use a command line program called Minion_ to track my notes. Minion_ is a command line application that should work anywhere that Python_ does. Minion makes it easy to author documents, sort documents, find documents, and archive documents.

I have used a lot of TODO list applications, but none of them previously met my needs.

I need a note tracker that: 
    - Allows a task to be a single sentence or an entire document.
    - Quickly sorts my tasks without taking my hands off of the keyboard. 
    - Tracks tasks by list and keyword.
    - Quickly moves tasks between lists. 
    - Find tasks by any keyword associated with the task.
    - Never delete a task, archive them by date, instead.
    - Easily backup all tasks to a service like DropBox_. 
    - Stores new tasks as .txt files, but does not choke if I ask it to track a PDF file once in awhile.

.. _DropBox: http://dropbox.com
.. _Getting_Things_Done: http://www.43folders.com/2004/09/08/getting-started-with-getting-things-done

The majority of these requirements are met by a surprisingly mature pair of technologies: A file folder and a text editor.

In a moment of desperation, between tools, I started keeping track of some notes in some text files in a folder. When I needed to retrieve a note, my knowledge of command line 'grep', did the job. 

Grep is great for retrieving notes, but eventually I needed to sort them. I wrote a quick Python_ script to interactively sort the notes one folder into neighboring folders. 

Once I was using Python_ anyway, I realized that a lot of the basic components of the system could be drastically streamlined with just a touch more Python. That streamlining became Minion_. Minion_ doesn't replace text notes on the filesystem, but it makes them quicker and fiendlier.  

I have found that relatively minor improvements to note tracking can result in great overall benefits. 

For example, composing a new document requires immediate overhead that is not immediately important:
    - What should this file be named?
    - Where should this file be stored?
    - Will I ever care what date I created this file?

Minion_ picks good defaults, and let's the user start writing notes immediately. When it comes time to rename and relocate the file later, Minion_ will help with that too. This allows the user to focus on the most important thing when inspiration strikes - writing. 

.. _Minion: http://github.com/edthedev/Minion
.. _Python: http://python.org
