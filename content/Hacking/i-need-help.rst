Getting Help in Python
======================
:date: 2012-09-01

:status: draft

In Java, the best way to learn about a class object is divination. But in Python, there's a really nice method called 'help'.

.. code-block::python

    import os
    help(os)
