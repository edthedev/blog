Freedunio
===============
:date: 2013-09-21

What follows is just some notes I made for myself when hacking on a Freeduino brand Arduino device, many specific to that device. This information is placed here in the hopes that it will simplify the path of some other Freeduino hacker looking for similar information.

Connecting a 9V Battery 
-------------------------
Some Arduino models (like my Freeduino) have a jumper pin that switches between the external 9V power and the USB power. The jumper pin is just like you would find in an old IDE harddrive for setting master or slave. In this case, it will be set to 'USB' by default, and you need to move it over to 'EXT' in order to use the 9V battery adapter.

Building and Uploading with INO
--------------------------------

List the board models, and guess which works for you.::

    ino list-models

Use the --board-model flag::

    ino build --board-model=atmega328
    ino upload --board-model=atmega328  

FTDI Driver
------------
The Freeduino is FTDI, so you may need to install an additional driver (Mac OS users).

Gear you probably want
-----------------------
An LED light on a swivel.
A magnifying glass on a swivel.
A 9V battery adapter for the Arduino.

Gear you might want if you have a modern Android phone or tablet
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Android App: ArduinoDroid - works great for uploading sketches from the Nexus 7 to either an Arduino UNO or the Freeduino
Android App: Arduino Commander - Control your Arduino pins with a convenient graphical interface.
Android App: USB Host Diagnostics - Can help determine if your Android device is likely to support ArduinoDroid.
OTG Adapter - Connect USB devices, such as an Arduino, to your Android device USB port.

