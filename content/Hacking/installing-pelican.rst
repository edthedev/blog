Installing Pelican
==================
:date: 2012-01-27
:status: published

I spent some of this evening setting up the ability to update by blog on several of my personal computers. One surprise about using Pelican_ is that it actually takes a decent amout of installed software to write and publish a new article.

.. _Pelican: http://blog.notmyidea.org/pelican-a-simple-static-blog-generator-in-python.html 

When I was using WordPress, all I needed was Vim and a quick Python script.

To use Pelcian, I had to install all of the following:

.. code-block:: bash 

    sudo yum install make python python-sphinx python-docutil pip
    sudo easy-install pip
    sudo pip install hieroglyph
    sudo pip install pelican 

Remember, none of this is required on the server, it only goes wherever you intend to pre-generate the blog content.

Make isn't strictly necessary, but it simplifies the actual process of building the HTML files quite a bit.

Interstingly, the Makefile gets a bit confused if the 'output' directory doesn't exist.

.. code-block:: bash

    mkdir ~/blog/output

As usual, you can grab the latest update of `this script`_ from `my GitHub`_.

.. _this script: https://github.com/edthedev/dotfiles/blob/master/scripts/blog-install
.. _my GitHub: https://github.com/edthedev/

