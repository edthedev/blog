Ansible script for Testable JavaScript training
================================================
:date: 2014-04-25

This is nice. I've created an automated setup for a training sesson that I am taking on Monday. It uses Vagrant and Ansible to provision a virtual machine running CentOS with Ruby, Ruby Gems, Node, and various other software installed.

You can check it out at my GitHub account, if you're curious how it works: https://github.com/edthedev/testable_javascript_setup
