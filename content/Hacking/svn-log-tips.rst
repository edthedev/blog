Get more from your Subversion log command
==========================================
:date: 2013-08-14

SVN revisions support date formating as well as revision number::

    svn log -r{2013-08-13}:{2013-08-15}

Adding the -v flag to an svn log command will display the files affected as well as the commit messages.::

    svn log -r{2013-08-13}:{2013-08-15} -v

    ------------------------------------------------------------------------
    r36135 | delaport | 2013-08-14 09:22:36 -0500 (Wed, 14 Aug 2013) | 2 lines
    Changed paths:
       M /example/models.py
       M /example/tests.py
       M /example/views.py

    Added code review notes.

