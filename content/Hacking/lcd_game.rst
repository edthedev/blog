Building a 1990's Style LCD Game
=================================
:date: 2014-02-26
:status: draft

Vision
-------

When the world changes, we get a unique opportunity to define ourselves through our response to the change.

It is becoming dramatically easier to become a Maker_.

My response is a combination of different aspects_ of my background_. I am going to create a hand held LCD_ game.

I played a lot of these games during the hour long bus ride to school, but the two greatest were TMNT_ and Dino_Attack_.

They had a few things in common:
 - You got to play as a dinosaur/ninja mutant turtle.
 - Very difficult.
 - Much longer and more nuanced than the average competing game.
 - Open world choices.

I intend to focus on the last two - especially the open world aspect - since I have a lot more computer processing power available than these games originally did.

.. _TMNT: http://www.youtube.com/watch?v=w5N1e1g3dc4 
.. _aspects: http://engineering.illinois.edu/  
.. _background: http://www.youtube.com/watch?v=9kX__T4X2Ds  
.. _Maker: http://hackeducation.com/2012/11/21/top-ed-tech-trends-of-2012-maker-movement/i
.. _LCD: http://www.handheldmuseum.com/Tiger/Cat5.htm
.. _Dino_Attack: http://www.handheldmuseum.com/Tandy/DinoAttack.htm

Materials
----------
I have needed to stop this project many times to order additional and different parts.

Processor
~~~~~~~~~~

While not strictly necessary, it was great to discover that the core Arduino_  processor is available in a micro_ for factor. The smaller micro_ chip should significantly improve my ability to fit the game into a case.

.. image:: http://edward.delaporte.us/images/arduino_micro.jpg
	:alt: Arduino Micro

.. _Arduino: url_goes_here
.. _micro: url_goes_here

LCD Screen
~~~~~~~~~~~

Obviously an LCD game needs an LCD screen. My first LCD was a 4 by 20 character display, ordered mainly to provide detailed information from the chip directly to the user. When the chip is attached to a computer by USB, the serial monitor performs this function. But my particular combination of Freeduino and Apple vesion of Arduino software causes the chip to become unresponsive at times after using the serial monitor, so I tend to avoid becoming too depdendant on it.

I chose the 4 by 20 display, because it is the same model used by the ATMega16 chips we learned with in school. I learned to really appreciate the two extra rows of available output for debugging purposes. But as it turns out, very few Arduino hackers are having success using the 4 by 20 display, right now. It is much simpler to go with the crowd and use one of the cheaper 2 by 16 displays.

The 2 by 16 display I ordered came in a dedicated Arduino shield form factor, and worked almost immediately. (I may have configured the inputs backwards on the first try...but otherwise it was easy to hook up.)

.. image:: arduino lcd shield
	:alt: arduino lcd shield

Now that my proof-of-concept work is approaching a real game, I decided it was time to order a real screen. I now have a 200 by 200 (ish) touch screen built into an Arduino shield form factor coming in the mail. I have learned better sources of Arduino parts, and this screen actually cost less than the 4 by 20 display did.

.. image:: big arduino screen
	:alt: big arduino LCD screen

Wires
~~~~~~
I have probably had more delays due to needing to mail order another wire than to any other cause. Male-to-male, female-to-female, and male-to-female are all needed, You can get around the need for female-to-female using two female-to-male and a breadboard. But the other needs aren't very forgiving.

After ordering a lot of wire, I started using electrical tape to create 2 channel, 3 channel, 4 channel, 5 channel and 8 channel versions of each combination. Two and four are especially common. Having a single 'piece' of wire that matches the width of each data channel is a huge sanity saver.

Battery
~~~~~~~~
One great advantage of the Arduino processor is that it can be powered directly from a standard 9 volt battery. A standard Arduino 9 volt battery adapter is available for about a dollar.

.. image:: standard 9 volt arduino adapater
	:alt: standard 9 volt arduino adapater

In order to power the Arduino micro_, I made a very minor moditication to the standard 9 volt adapter. I used wire cutters, male-to-male breadboard pin adapters, and hot glue. Someone more skilled might have used solder, but I appreciated the error forgiveness that the non-conductive hot glue provided.

.. image:: 9 volt adapter altered for adruino micro
	:alt: 9 volt adapter altered for adruino micro

Buttons
~~~~~~~~
I have ordered a two axis joystick, and have plenty of touch contact buttons on hand. If I decide to order anything more special, I will update this section.

Technical Challenges
---------------------
Coming soon...discussion of technical challenges overcome...
Battery
Display
Controls

Source Code
------------
Coming soon...link to source code.
