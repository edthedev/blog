Tracking DotFiles with BitBucket
================================
:date: 2013-02-01

I use version control to keep my Linux configuration files synced between computers, and to be able to roll back changes when I have a problem.

This will work on any Unix-like system (Apple, Ubuntu, Fedora Core, Cygwin).

Here's a quick guide.

1. Create a free account at BitBucket_. 

.. _BitBucket: http://bitbucket.org

2. Generate an SSH key pair.::

    ssh-keygen

3. Upload your private key to BitBucket_.::

    ~/.ssh/id_rsa.pub

4. On the BitBucket_ website, create a repository called 'dotfiles'. You may want to set it to private.

5. Install git_. Instructions below are for Fedora Core, your commands may vary::

    sudo yum install git

.. _git: http://git-scm.com/downloads

6. Install Python_::

    sudo yum install python

.. _Python: http://python.org

7. Clone the dotfiles repository to a local folder::

    git clone ssh://git@bitbucket.org/<username>/dotfiles.git 

8. Download the builddotfiles.py script into that folder::

    cd ~/dotfiles
    wget https://github.com/edthedev/dotfiles/blob/master/builddotfiles.py

9. Move some configuration files into the dotfiles directory::

    mv ~/.profile ~/dotfiles/profile
    mv ~/.bashrc ~/doftfiles/bashrc
    mv ~/.vimrc ~/doftiles/vimrc

10. Run the builddotfiles.py script in test mode.

.. code-block:: bash

    python builddotfiles.py

11. If everything looks alright, run the script to create the symbolic links.

.. code-block:: bash

    python builddotfiles.py -l

12. Test that it worked.

.. code-block:: bash

    ls ~ -al | grep profile

If you moved your .profile file into dotfiles, you should see something like this:

.. code-block:: bash

    lrwxrwxrwx  1 delaport delaport        39 2011-12-30 11:19 .profile -> ~/dotfiles/profile

13. Now confirm that it worked by opening ~/.bashrc with your favorite editor. Your editor will show that you actually have ~/dotfiles/bashrc open.

.. code-block:: bash

    vim ~/.bashrc

14. Now git will detect changes when you update your settings in your dotfiles. Remember to periodically check in and push the changes.

.. code-block:: bash
   
    cd ~/dotfiles
    git status
    git commit -a
    git push

