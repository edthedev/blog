How a Linux User May Survive on Windows 
=======================================
:status: draft
:date: 2012-12-01

Steps to bootstrap a Windows installation:

Setup Browser
1. Install Firefox.
2. Setup Sync
a. Install OmniBar for Firefox
https://addons.mozilla.org/en-US/firefox/addon/omnibar/
b. Install TamperData
c. Install Web Developer
d. Install CSS Inspector?

Or:
1. Install Google-Chrome
2. Setup Sync.
3. ????
4. Profit!!

Setup Command Prompt

1. Install Python 2.x
firefox http://www.python.org/getit/
2. Install Mercurial
firefox http://mercurial.selenic.com/

3. Pull dotfiles
hg clone https://bitbucket.org/edthedev/edthedev
4. Link dotfiles
?????

3. Configure Vim
a. setup backupdir in .vimrc
set backupdir=./.backup,/tmp,c:\temp,c:\tmp
set directory=./.backup,/tmp,c:\temp,c:\tmp
On Windows 7 you also need to:
mkdir c:\temp

Setup shared copy/paste for gvim

