Using Vagrant with Ansible, part II: Ansible Base Box
=======================================================
:date: 07-09-2013

UPDATE
-------

Per a Google groups discussion, there is no need whatsoever to create an Ansible base box for Vagrant. Ansible minions (computers controlled by Ansible) do not need to have Ansible installed; they only need to have SSH installed. So any Vagrant base image will work just fine with Ansible, so long as the Vagrant host machine has Ansible installed.

So to get started with Vagrant and Ansible, all you need to do is install Ansible on the Vagrant VM host machine.::

    $ sudo apt-get install python-pip python-dev
    $ sudo pip install ansible
    $ ansible --version
    ansible 1.2.3

The recipe below, however, is still terrific for creating new base images. I just recommend installing something other than Ansible, since there's no need for it.

Recipe
-------

This post covers installing Ansible_ into a VM managed by Vagrant_. This post is mostly about creating new Vagrant_base_boxes_; Ansible_ happens to (have seemed to) be a practical example of something you might want to add to a Vagrant base box.

.. _Ansible: http://ansibleworks.com
.. _Vagrant: http://vagrantup.com
.. _Vagrant_base_boxes: http://docs.vagrantup.com/v2/boxes.html


If you're more interested in learned how to use Ansible_, check out my other post on Using_an_Ansible_Playbook_to_Install_Vagrant_. That post is mostly about Ansible_, using Vagrant_ as an example piece of software that you might want to install on a host.

.. _Using_an_Ansible_Playbook_to_Install_Vagrant: http://edward.delaporte.us/vagrant-with-ansible.html
.. _CentOS: http://www.centos.org/

If you're completely new to both Vagrant_ and Ansible_, you can follow the instructions in Using_an_Ansible_Playbook_to_Install_Vagrant_ to get your Vagrant environment setup, and then follow this article to create an Ansible_ ready base box for use with Vagrant_.

Start by setting up a new Vagrant image from an existing base box::

    mkdir -p ~/ansible_dir
    cd ~/ansible_dir
    vagrant init

Optionally, if you want to start from a different base box than the default, modify the generated Vagrantfile now. I have changed mine to use a CentOS_ 64 bit base box image.::

  # config.vm.box = "base"
  config.vm.box = "debian64"

If you want to follow along exactly, and you haven't already added a 'debian64' base box, you can add it by running this command on the shell::

    vagrant box add debian64 http://f.willianfernandes.com.br/vagrant-boxes/DebianSqueeze64.box

You can find more options for base boxes at Vagrant_Boxes_.

.. _Vagrant_Boxes: http://www.vagrantbox.es/

To install Ansible_ on the box, simply use Vagrant's built in SSH to connect as root, and install Ansible as normal.::

    >cd ~/ansible_dir
    >vagrant up
    >vagrant ssh
    Welcome to your Vagrant-built virtual machine.
    [vagrant@localhost ~]$ sudo apt-get install python-pip python-dev
    [vagrant@localhost ~]$ sudo pip install ansible
    [vagrant@localhost ~]$ ansible --version
    ansible 1.2.3
    [vagrant@localhost ~]$ exit
    logout
    Connection to 127.0.0.1 closed.

As a courtesy to your future self, update the Vagrantfile to use a local Ansible playbook.yml by default.::

    config.vm.provision :ansible do |ansible|
      ansible.playbook = "playbook.yml"
    end

Now create the updated base box.::

    cd ~/ansible_dir
    vagrant package --vagrantfile Vagrantfile

There now exists a new base box at ~/ansible_dir/package.box. Relocate the .box file if needed, and then import it into your vagrant environment.::

    vagrant box add debian64_ansible ~/ansible_dir/package.box

Now use the new ansible base_box for future Ansible based Vagrant VMs.::
    
    mkdir -p ~/new_vagrant_project
    cd ~/new_vagrant project
    vagrant init debian64_ansible
    vim playbook.yml # <-- Add your Ansible configuration here.
    vagrant up

For more on Ansible_ configuration, see the Ansible_Docs_.

.. _Ansible_Docs: http://www.ansibleworks.com/docs/playbooks.html

.. _Vagrant_Guide_: http://docs-v1.vagrantup.com/v1/docs/getting-started/packaging.html
