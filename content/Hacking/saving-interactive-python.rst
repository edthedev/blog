Saving Interactive Python
=========================
:date: 2012-12-01

When I am writing these blog posts, I frequently want to include a bit of Python_ code off of the top of my head. This is a terrible idea, because my Python code usually has some tiny mistake that will make it not run, unless I test it before sharing it.

.. _Python: http://python.org

I have a nifty little solution, that I will now share with you. As you may be aware, I use Vim_ to do all of my code and blog editing. This trick is useful for interactively testing a bit of code, whether I intend to share it in my blog, or include it in a code file I am editing.

.. _Vim: http://edward.delaporte.us/vim 

Within Vim_, I start an interactive Python session::
    
    :!python

Python is now running from within Vim, so my currently open files and cursor position are unaffected. Within the interactive Python session, I write the code I intend to test. For example:

.. code-block:: python

    import logging
    dir(logging)

Once I am done making and fixing mistakes; I do the following:

.. code-block:: python

    import readline
    readline.write_history_file('temp.py')
    exit()

Now I have the Python commands saved in 'temp.py', and after pressing ENTER, I am returned to my Vim_ session. To bring the commands in temp.py into the current Vim_ buffer, I can run the following command.

.. code-block:: vim

    :.!cat temp.py

The above command will dump the contents of temp.py after the current cursor location in Vim_. I prefer to separate my work into many Vim_Buffers_, and so the command I typically use instead is:

.. _Vim_Buffers: http://eseth.org/2007/vim-buffers.html

.. code-block:: vim

    :badd temp.py
    :buffers
    :bn
    3yd
    :bp
    p
 
Translated, that says:
    1. Open temp.py into another buffer.
    2. List all buffers.
    3. Switch to the next buffer.
    4. Yank (copy) three lines downward from the current line.
    5. Switch to the previous buffer.
    6. Paste the previously yanked lines.

This makes the code available to copy and paste without dumping it all into the current buffer. Now I can copy over only the most relevant bits from my previously saved interactive Python session.
