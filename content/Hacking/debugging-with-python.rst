debugging with python
=====================
:date: 2013-07-31

Python Debugger (Pdb)
----------------------

Invoke the Python_debugger_ (Pdb) when running a Python script from the command line::

    import pdb; pdb.set_trace()

.. _Python_debugger: http://docs.python.org/2/library/pdb.html

Invoke the Python_debugger_ only if your script encounters an unhandled exception::

    import sys, pdb, traceback
    def my_excepthook(exception, inner, trace):
        traceback.print_exception(exception, inner, trace)
        pdb.pm()
    sys.excepthook = my_excepthook

    raise Exception('Testing break!')

When the script executes, you will see a prompt that looks like this::

    (Pdb) 

Display a list of available Pdb commands::

    (Pdb) help

    Documented commands (type help <topic>):
    ========================================
    EOF    bt         cont      enable  jump  pp       run      unt   
    a      c          continue  exit    l     q        s        until 
    alias  cl         d         h       list  quit     step     up    
    args   clear      debug     help    n     r        tbreak   w     
    b      commands   disable   ignore  next  restart  u        whatis
    break  condition  down      j       p     return   unalias  where 

    Miscellaneous help topics:
    ==========================
    exec  pdb

    Undocumented commands:
    ======================
    retval  rv

Print a stack trace::

    (Pdb) where
    > /home/delaport/Minion/minion(267)<module>()
    -> print match_files

Show the source code around the current break point::

    (Pdb) list
    262             archives=args['--archives'])
    263     
    264         # from IPython import embed; embed()
    265         import pdb; pdb.set_trace()
    266     
    267  ->     print match_files
    268         (choice_path, filename) = brain_of_minion.select_file(match_files)
    269         data = dict()
    270         data.update(args)
    271         data['date'] = datetime.datetime.today().strftime(get_date_format())
    272         data['time'] = datetime.datetime.today().strftime("%H:%M")

Set an additional breakpoint::

    (Pdb) tbreak 272
    Breakpoint 1 at /home/delaport/Minion/minion:272

Continue until the next break point::

    (Pdb) continue

Execute the current line, stop at the first possible occasion::

    (Pdb) step

Continue execution until the next line in the current function is reached or it returns::

    (Pdb) next

Jump to another line and execute it next::

    (Pdb) jump 272

Display the list of variables currently in memory::

    (Pdb) !dir()
    ['CLEAN_INBOX_MESSAGE', '__builtins__', '__doc__', '__file__', '__name__', '__package__', 'args', 'brain', 'brain_of_minion', 'date_sort', 'datetime', 'directory', 'docopt', 'filter', 'get_date_format', 'inbox_files', 'match_files', 'optparse', 'os', 'pdb', 'random', 'should_collect', 'shutil', 'sys']

Display the contents of a single variable::

    (Pdb) !print CLEAN_INBOX_MESSAGE
    *~*~*~*~*~*~*
     Clean Inbox
     *~*~*~*~*~*~*

Debugging with IPython
-----------------------

Before you can debug with IPython_ you will need to install_IPython_.

.. _IPython: http://ipython.org/
.. _install_IPython: http://ipython.org/install.html

Invoke IPython when running a Python script from the command line::

    from IPython import embed; embed()

    Python 2.7.3 (default, Aug  1 2012, 05:16:07) 
    Type "copyright", "credits" or "license" for more information.

    IPython 0.13.1 -- An enhanced Interactive Python.
    ?         -> Introduction and overview of IPython's features.
    %quickref -> Quick reference.
    help      -> Python's own help system.
    object?   -> Details about 'object', use 'object??' for extra details.

    In [1]: 




In IPython, simply use Python commands::

    # .i.e. Display the list of variables currently in memory
    print dir()

To reload a module into memory that you have changed on the disk::

    import my_module
    # Fix a bug in my_module.py and hit save.
    my_module = reload(my_module)

To exit IPython and resume the script::

    Ctrl+D
    Do you really want to exit ([y]/n)? y


Debugging with IPython Debugger
--------------------------------

The best of both worlds, if you have IPython available and are debugging a command line Python script.

Install IPython Debugger::

    pip install ipdb

Invoke IPython debugger when an unhandled exception is raised::

    # Looks just like the example above, but with 'ipdb' instead of just 'pdb'
    import sys, ipdb, traceback
    def my_excepthook(exception, inner, trace):
        traceback.print_exception(exception, inner, trace)
        ipdb.pm()
    sys.excepthook = my_excepthook

    raise Exception('Testing break!')

Alternatively::

    import ipdb
    with launch_ipdb_on_exception():
        # Do some stuff....
        raise Exception('Testing break!')

And then it looks something like this when the exception is hit::

    Traceback (most recent call last):
      File "/home/delaport/Minion/minion", line 276, in <module>
        raise Exception('Testing break!')
    Exception: Testing break!
    > /home/delaport/Minion/minion(276)<module>()
        275     # print match_files
    --> 276     raise Exception('Testing break!')
        277 

    ipdb> 

