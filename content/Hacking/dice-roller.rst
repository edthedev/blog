Dice Roller
===================
:date: 2013-08-11

Everyone should write a dice_roller_ once in awhile. My dice_roller_ back online, in case you have any use for it. It was written in Bottle.py_ and is being served by Nginx_ from within a VirtualBox_ virtual machine. My Source_Code_ and Configuration_ are available on GitHub_ for your reading amusement.

.. _dice_roller: http://delaporte.us/dice
.. _Source_Code: https://github.com/edthedev/dotfiles/tree/master/vagrant/bottle_dice_roller/source
.. _Configuration: https://github.com/edthedev/dotfiles/tree/master/vagrant/bottle_dice_roller

.. _Bottle.py: http://bottlepy.org/docs/dev/
.. _Nginx: http://wiki.nginx.org/Main
.. _VirtualBox: https://www.virtualbox.org/
.. _GitHub: https://github.com/explore

