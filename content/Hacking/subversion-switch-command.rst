About the Subversion Switch command
====================================
:date: 2013-10-22

We've all been there, you're just trying to switch branches after a pleasant merge, when a wild conflict appears::

	svn switch https://example.com/foo/branches/new_hotness_branch
	A conflict occurred...
	C - FileThatHatesEverything.txt

And now for the rest of time you get to enjoy the status command producing spare 'S' characters::
	svn st
	S thisfile
	S thatone

Naturally, you resovled the conflict::
	
	rm FileThatHatesEverything.txt
	svn export https://example.com/foo/branches/new_hotness_branch/FileThatHatesEverything.txt .
	svn resolved FileThatHatesEverything.txt
	svn ci

But the 'S' won't go away on it's own::
	svn st
	S thisfile
	S thatone

The solution, it turns out, is simply to re-run the switch.::

	svn switch https://example.com/foo/branches/new_hotness_branch
	svn st

	???
	Profit!

Aren't you glad we had this little talk? I know I am.
