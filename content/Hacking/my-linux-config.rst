My Software Configuration 2012
==============================
:date: 2012-11-01

The following is a brief overview of my current most-used software packages.

Operating System: Ubuntu_
    .. _Ubuntu: http://www.ubuntu.com/ubuntu

Window Manager: i3wm_
    .. _i3wm: http://i3wm.org

Code Editor: Vim_ with PyLint_
    .. _Vim: http://vim.org
    .. _PyLint: http://www.logilab.org/card/pylint_tutorial 

Blog: Plain HTML generated using Pelican_ from ReStructuredText_, edited in Vim_, served by Apache_ on DreamHost_.

.. _DreamHost: http://dreamhost.com
.. _Vim: http://vim.org
.. _ReStructuredText: http://docutils.sourceforge.net/docs/user/rst/quickstart.html
.. _Pelican: http://docs.getpelican.com 
.. _Apache: http://apache.org

File Organizer: Minion_
    .. _Minion: http://github.com/edthedev/minion

Terminal: Gnome Terminal, dtach, dtvm

