About my Vimrc
==============
:date: 2012-12-01

I live on the home row in 'Normal' mode. It's very freeing.
I've done some strategic key-mapping, which I will share below.

I keep the 'Leader' key mapped to the comma key::

    let mapleader=","

F4 key to build HTML pages from ReStructuredText markup files from Python-Sphinx.
This is how this website itself, is generated::

    map <F4> :!make html

F5 key to run the current python file.
The ':r.py' allows me to run script.py while editing script.kv or script.html::

    map <F5> :!python %:r.py

When refactoring, I often need to find other instances of the word under the cursor (in all other files).::

    map <Leader>f :!grep <C-r><C-w> . -R -A1 -B1 -n <bar> grep -v .svn <bar> grep -v Binary

This is a complex one, so here is the breakdown:

Find the current word in other files::
    grep <C-r>C-w> . -R

Include context - line before, line after, and line number::
    -B1 -A1 -n

Do not include files in the .svn directory, or binary results.::
    <bar> grep -v .svn <bar> grep -v Binary


I keep my Python files PEP8_ compliant.

.. _PEP8: http://www.python.org/dev/peps/pep-0008/

.. code-block:: vim

    " PEP8
    au FileType python setlocal expandtab
    au FileType python setlocal tabstop=4
    au FileType python setlocal shiftwidth=4

    " Automatically highlight lines over 80 characters.
    au FileType python let w:m1=matchadd('Search', '\%<81v.\%>77v', -1)
    au BufWinEnter *.py let w:m1=matchadd('Search', '\%<81v.\%>77v', -1)
    " Color lines that are too long them red.
    au FileType python let w:m2=matchadd('ErrorMsg', '\%>80v.\+', -1)
    au BufWinEnter *.py let w:m2=matchadd('ErrorMsg', '\%>80v.\+', -1)

PyLint_ also helps maintain clean code.

.. _PyLint: http://www.logilab.org/card/pylint_manual#what-is-pylint  

.. code-block:: vim

    " Run PyLint
    map <F6> :!pylint %:r.py | less<Cr>

I like to see line numbers, but not in my text documents.::

    " Show line numbers
    au Filetype python setlocal number

And some default settings are just annoying in code files.::
    " Never wrap lines...
    au FileType python setlocal textwidth=0

I enjoy coding through an SSH connection on my phone, and phones do not have an Escape key. So I map ii to ESC, so I can easily return to Normal mode.::
    inoremap ii <esc>
