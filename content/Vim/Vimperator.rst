Enable or disable Vimperator
================================================
:date: 2013-02-07

Vimperator_ is a plugin for Firefox that makes common Vim keyboard shortcuts work in Firefox. There are several such add-ons, but Vimperator_ interferes the least with Firefox  and follows it's philosophy.

Vimperator_ adds keyboard commands for navigation tasks. Vimperator_ interprets all keypresses - and for any keypress that has a Vimperator_ command equivalent, Vimperator_ will act on the key press rather than sending it to the webpage. 

Most web pages don't respond to key presses anyway, so you usually this works fine. Some pages do respond to keypresses (Google Mail, or Outlook WebMail, for example). For such pages, press 'Insert' or 'Shift+Escape' to turn off Vimperator_. From then on, Vimperator_ will only listen for another press of 'Insert' or 'Shift+Escape' to re-enable itself.

.. _vimperator: https://addons.mozilla.org/en-us/firefox/addon/vimperator/
