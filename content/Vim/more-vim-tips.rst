more vim tips
=============
:date: 2013-02-21

terminal
---------
Run the current line of the file as a shell command and dump the output into the file.::

    yyp!!sh

The 'yyp' copies and pastes the current line so it will not be lost.::

    yyp

The '!!sh' runs the line with sh. You could also use a different shell, if needed.::

    !!bash

selecting text
------------------
Reselect the last visually selected area.::

    gv

Repetitive Tasks
~~~~~~~~~~~~~~~~~
Record a macro into a buffer and repeat it later::

    Press 'q'
    Press a letter to name the macro buffer.
    Enter commands as normal.
    Press 'q' when the task is complete.

Run the macro::
    
    Press '@' and then the letter chosen earlier.

Run the macro multiple times::

    Press a number and then @ and then the letter chosen earlier.

Put it all together:: 

    gUw - Changes the word under the cursor to uppercase.
    ww - Jumps two words to the right.
    gUwww - Changes the word to uppercase, then jumps two words to the right.
    qrgUwww - Records that sequence into buffer 'r'.
    22@r - Uppercases words and jumps right 22 times in a row.

15 tabs and indenting
----------------------
16 folding
-----------
17 diff mode
-------------
18 mapping
-----------
19 reading and writing files
-----------------------------
20 the swap file
-----------------
21 command line editing
------------------------
22 executing external commands
-------------------------------
23 running make and jumping to errors
--------------------------------------
24 language specific
---------------------
25 multi-byte characters
-------------------------
26 various
-----------

Repeat Commands
~~~~~~~~~~~~~~~~

Bring up a j/k scrollable menu of past commands with::

    q:

Vim sessions!
~~~~~~~~~~~~~~

:mks ~/project.vim
vim -S ~/project.vim

Compare Current Buffer to File
--------------------------------
::
    
    :w !diff % -

Reload the Current Buffer from the Original File::

   :edit 

Vim as File Manager
--------------------

::

    :ex . - file explorer mode
    mf - mark file
    mu - marks undo
    D - delete marked files
    Shift-v, j/k - visuall mark files
    R - rename
    mt <change directories> mc - copy and paste
    mt <change directories> mm - cut and paste
    md - diff the marked files
    mx - execute a command on all marked files
       use % for filename
    o - open in horizontal split
    mz - compress or decompress with gzip


Vim Recipes: Using Vim as a File Manager: http://vim.runpaint.org/other-uses-vim/using-vim-as-file-manager/


Reload the Current Buffer::

    :edit

