PyLint with Vim
===============
:date: 2013-03-06

Some notes about using Pylint_ with Vim_.
------------------------------------------

.. _vim: http://vim.org
.. _pylint: https://pypi.python.org/pypi/pylint  

In the .vimrc::

    " Make :make call PyLint                                                
    set makeprg=pylint\ --reports=n\ --output-format=parseable\ %:p
    set errorformat=%f:%l:\ %m
    " Use :cnext and :cprevious to page through results.

To get rid of 'Unable to import' errors::
    
    pylint --generate-rcfile > ~/.pylintrc
    vim ~/.pylintrc

    [MESSAGES CONTROL]
    disable=FO401

To run PyLint::
    
    :make

Then use these commands to page through the results, jumping to the correct location in the current file.::
    :cnext
    :cprevious
    :cn
    :cp
    :clist
