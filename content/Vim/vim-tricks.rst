Vim Tricks
=============
:date: 2013-07-09

This is Edward Delaporte's collection of Vim tricks.

The topics are organized around the 'Basic Editing' and 'Advanced Editing' sections of the Vim :help command.

starting Vim, Vim command arguments, initialisation
----------------------------------------------------
editing and writing files
--------------------------
commands for moving around
---------------------------

Jump to line 77 in the file, then return to edit mode::

    ESC 77 G 

Jump to end of line, then return to edit mode::

    ESC $ 

Start editing at the end of the current line.::

    ESC A

scrolling the text in the window
---------------------------------
Insert and Replace mode
------------------------
deleting and replacing text
----------------------------

Capitalize a word::

    gUw

Undo and Redo
--------------

Undo the last command::

    u

Redo an undone command::

    Ctrl+R

repeating commands, Vim scripts and debugging
----------------------------------------------

Reuse a previous command
~~~~~~~~~~~~~~~~~~~~~~~~~

Bring up a j/k scrollable menu of previously used commands with::

    q:

Do Repetitive Tasks
~~~~~~~~~~~~~~~~~~~~
Record a macro into a buffer and repeat it later::

    Press 'q'
    Press a letter to name the macro buffer.
    Enter commands as normal.
    Press 'q' when the task is complete.

Run the macro::
    
    Press '@' and then the letter chosen earlier.

Run the macro multiple times::

    Press a number and then @ and then the letter chosen earlier.

Put it all together:: 

    gUw - Changes the word under the cursor to uppercase.
    ww - Jumps two words to the right.
    gUwww - Changes the word to uppercase, then jumps two words to the right.
    qrgUwww - Records that sequence into buffer 'r'.
    22@r - Uppercases words and jumps right 22 times in a row.

using the Visual mode (selecting a text area)
----------------------------------------------

Reselect the last visually selected area.::

    gv


various remaining commands
---------------------------
recovering from a crash
------------------------
Command-line editing
---------------------

Simple Command Line Examples
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you know how to do something on the command line outside of Vim, there's no need to leave Vim to use it.

For example, on the command line, to count the words in a file::
    
    cat filename.txt | wc -w

Within Vim, if filename.txt is open::

    :!cat % | wc -w

Or, if you want to actually include the result in the file itself::

    :.!cat % | wc -w

(Incidentally, the file this post was generated from had 526 words.)

Test a shell script while editing it within Vim
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Run the current line of the file as a shell command and dump the output into the file.::

    yyp!!sh

The 'yyp' copies and pastes the current line so it will not be lost.::

    yyp

The '!!sh' runs the line with sh. You could also use a different shell, if needed.::

    !!bash

description of all options
---------------------------
regexp patterns and search commands
------------------------------------

Replace every instance of the regex with the given expression.::

	:% s/regex/replacement/g

key mapping and abbreviations
------------------------------
tags and special searches
--------------------------

commands for a quick edit-compile-fix cycle
--------------------------------------------

Run the current file with Python::

    :!python %

Check the current file into version control.::

    :! svn ci %
    :! git commit %

commands for using multiple windows and buffers
------------------------------------------------

Buffers
~~~~~~~~~~~~
I use Vim buffers extensively because they allow me to share copy and paste buffers between files, as well as to do targeted semi-global replaces with Sed.

Open multiple files in separate Vim buffers::

    vim foo.txt bar.rst baz.js

Buffer Commands::	
    :badd <filename> - adds a new buffer
    :buffers - lists all open buffers by number
    :b3 - jump to third buffer
    :bp - jump to previous buffer
    :bn - jump to next buffer
    :bd - close ('delete') current buffer

Compare  the current buffer to the file on disk::
    
    :w !diff % -

Reload the current buffer from the file on disk::

    :edit


Marks
~~~~~~~~~~

Jump to last edit position::
    ''

Set a mark named 'a'::
    ma

Jumpt to mark 'a'::
    'a

List all marks::
    :marks

Sessions
~~~~~~~~~

Save your marks and buffers for later use::
    :mks!

(Creates a file called Session.vim)

Open vim with previous marks and buffers::
    vim -S Sesison.vim

commands for using multiple tab pages
--------------------------------------
syntax highlighting
--------------------
spell checking
---------------
working with two to four versions of the same file
---------------------------------------------------
automatically executing commands on an event
---------------------------------------------
settings done specifically for a type of file
----------------------------------------------
expression evaluation, conditional commands
--------------------------------------------
hide (fold) ranges of lines
----------------------------

Vim as File Manager
--------------------

In a pinch, Vim makes a pretty decent file manager.::

    :ex . - file explorer mode
    mf - mark file
    mu - marks undo
    D - delete marked files
    Shift-v, j/k - visuall mark files
    R - rename
    mt <change directories> mc - copy and paste
    mt <change directories> mm - cut and paste
    md - diff the marked files
    mx - execute a command on all marked files
       use % for filename
    o - open in horizontal split
    mz - compress or decompress with gzip

`Vim Recipes: Using Vim as a File Manager`_

.. _`Vim Recipes: Using Vim as a File Manager`: http://vim.runpaint.org/other-uses-vim/using-vim-as-file-manager/

Ranger_ is a vim-like terminal file manager. 

.. _Ranger: http://ranger.nongnu.org/

