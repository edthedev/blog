Vim as a Wiki
=============
:date: 2012-11-01

Navigation
----------
A Wiki_ is a great way to organize and navigate stored information. Vim_ actually works pretty well as a Wiki right out of the box.

.. _Wiki: http://en.wikipedia.org/wiki/Wiki
.. _Vim: http://www.vim.org/about.php

Open a 'Start Page' in a new buffer.

.. code-block:: vim

    " Add this to your ~/.vimrc
    map <Leader>wh :e ~/wiki_home.txt

Open the file under the cursor (per Wikia_).

.. _Wikia: http://vim.wikia.com/wiki/Open_file_under_cursor

.. code-block:: vim

    gf  open in the same window ("goto file")
    <c-w>f  open in a new window (Ctrl-w f)
    <c-w>gf     open in a new tab (Ctrl-w gf) 

For files with spaces in the name, you will need to use Visual_Mode_.

.. _Visual_Mode: http://stackoverflow.com/questions/5370991/vims-visual-mode-guidance

Open a web link_.

.. _link: http://stackoverflow.com/questions/9458294/open-url-under-cursor-in-vim-with-browser

.. code-block:: vim

    " Add this to your ~/.vimrc
    function! HandleURL()
      let s:uri = matchstr(getline("."), '[a-z]*:\/\/[^ >,;]*')
      echo s:uri
      if s:uri != ""
        silent exec "!gnome-open '".s:uri."'"
      else
        echo "No URI found in line."
      endif
    endfunction
    map <leader>u :call HandleURL()<cr>

Page History
------------
Another great feature of a Wiki is seeing which documents you have recently visited.

If you are using ':e' and 'gf' to open files in Vim, your previous files are still open in other Vim buffers.

.. code-block:: vim

    :buffers    " List open buffers
    :bn         " Next open buffer
    :bp         " Previous open buffer
    :bd         " Close current buffer

A really good Wiki provides assistance finding other documents to link to. In Vim, an appoximation of this feature can be had by pasing a local directory conent listing into the current document.

.. code-block:: vim

    :.!ls

A good Wiki also makes it easy to copy information from one document to another. 

.. code-block:: vim

    " Copy the contents of the linked file right here.
    :.!cat <cfile>

Removing a document should also be possible from within the editor.

.. code-block:: vim

    " Remove the linked file.
    :.!rm <cfile>

In conclusion, I'm not sure Vim_ is the best technology for a Wiki, but it is surprisingly functional if you, like me, happen to already be using it.
