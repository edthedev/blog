Unescapable Vim
================================================
:date: 2013-09-25

Vim is modal editor, and this is what tends to throw people. Vim is an editor, after all, but only has *one mode* for entering text, 'insert' mode, and it is not the default. 

Although I once thought of editors as being exclusively for entering text, the reality I have since learned is that I spend a lot more time modifying, correcting, and nuancing existing text than entering new text. Vim's default mode, 'normal' mode is much better at modifying, correcting and nuancing text than 'insert' mode is, and that is why it is the default mode.

To switch into 'edit' mode, press the 'i' key.::

    i

There was a time when I would have just opened Vim and switched to 'edit' mode and never switched back. But I could not do so, because in 'edit' mode, it is impossible to save the document, so I had to return to 'normal' mode eventually. I didn't realize how healthy this was at the time, but it forced me to spend enough time in 'normal' mode to learn a few things about it.

Now that I know 'normal' mode well, I look for ways to return to 'normal' mode as quickly as possible after entering text. But to get started, all a new user needs to know is:

To return to 'normal' mode, press the escape key.::

    ESC

To save the document, press ':w'::

    :w myfilename.txt

To exit Vim, press ':q'::
    
    :q

The escape key will return you to 'normal' mode from *almost* any other mode in Vim.

That 'almost' is important, though. There are two modes in Vim from which the escape key cannot escape: 'help' mode and 'record' mode.

There are good reasons for these modes to ignore the escape key, this, but it can be very frustrating for a new Vim user to find themselves trapped in a foreign mode, and unable to edit text, save their document, or even to exit the application.

These aren't normally modes that anyone introduces new Vim users to, but that is a disservice, because these two modes share something in common: neither mode can be escaped from using the Escape (ESC) key.

Help Mode
----------
To enter 'help' mode, press :help from 'normal' mode.::
    
    <ESC>:help

What the escape key does: The escape key toggles between 'help (insert)' mode and 'help (normal)' mode, but does not exit 'help' mode.
To exit 'help' mode, press :q::

    <ESC>:q

Record Mode
------------
To enter 'record' mode, press 'q' and then any other letter.::

    qe

What the escape key does: The escape key functions as normal, exiting any other mode. Recorded actions are able to include steps that switch modes, so pressing escape does not halt the recording. To exit record mode, press escape and then q::

    <ESC>q

Incidentally, if you want to replay a recorded sequence, you can press '@' and then the letter you chose above ('e').::

    @e

