A quick guide to using Marks in Vim
====================================
:date: 2013-03-05

List all marks::

    :marks

Jump to last edit position::

    ''

Set a mark named 'a'::

    ma

Jumpt to mark 'a'::

    'a

Save your marks and buffers for later use::

    :mks!

(Creates a file called Session.vim)

Open vim with previous marks and buffers::

    vim -S Sesison.vim
