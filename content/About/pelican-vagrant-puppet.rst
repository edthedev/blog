Published from a Virtual Machine
---------------------------------
:date: 2013-08-01

This article was published from a Virtual Machine that was created with Vagrant_ and Puppet_.

.. _Vagrant: http://docs.vagrantup.com/v2/getting-started/

.. _Puppet: http://docs.puppetlabs.com/references/latest/type.html

My_Vagrant_configuration_ simply tells Vagrant to create a headless Ubuntu server and then to turn over configuration to Puppet_.

.. _My_Vagrant_configuration: https://raw.github.com/edthedev/dotfiles/master/vagrant/Vagrantfile

My_Puppet_configuration_ installs all of the necessary prerequisite software to run Pelican, then installs my SSH private key, and then checks out my blog source files from a BitBucket_ repository. I use BitBucket_ for this blog rather than GitHub_ because BitBucket_ provides a limited amount of free private hosting. But in principle, this_recipe_ will work with either.

.. _My_Puppet_configuration: https://raw.github.com/edthedev/dotfiles/master/vagrant/manifests/pelican_blog.pp

.. _BitBucket: https://bitbucket.org/ 
.. _GitHub: https://github.com/

.. _this_recipe: https://github.com/edthedev/dotfiles/tree/master/vagrant
