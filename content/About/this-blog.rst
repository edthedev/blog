:date: 2012-12-3
:slug: about-this-blog

About this Blog
===============

My original blog is EdTheDev.com_. I recently registered delaporte.us; and decided that it would be nice to have a blog at edward.delaporte.us_. DreamHost_ provides an excellent WordPress_ service, and I highly recommend it to anyone who needs a blog. 

.. _EdTheDev.com: http://edthedev.com
.. _edward.delaporte.us: http://edward.delaporte.us
.. _DreamHost: http://dreamhost.com
.. _WordPress: http:/wordpress.org

But as they said in 'Ocean's 13'; 'You don't do the same gig again, you do the next gig.' So I decided that it was time to find a blog technology that plays well with my latest suite of tools, while providing some minor theoretical improvement. 

My requirements:
    - Must allow me to compose my blog in either MarkDown_ or ReStructuredText_.
    - Must allow me to easily edit my blog entries in my favorite editor, Vim_.
    - Must still generate blog features that I rely on: RSS feeds, organization by category, automatic linking to recent articles.
    - Article contents must be accessible to future descendants, this means that database files are out, and plain text files are important.

.. _MarkDown: http://daringfireball.net/projects/markdown
.. _Vim: http://vim.org
.. _ReStructuredText: http://docutils.sourceforge.net/docs/user/rst/quickstart.html

It turns out that Pelican_ does a very nice job meeting these requirements.

.. _Pelican: http://docs.getpelican.com 

So now I compose my blog entries in Vim_, use Pelican to generate HTML and RSS files, and publish with a simple Rsync_ to my DreamHost server.

.. _Rsync : http://linux.die.net/man/1/rsync

As a nice side effect, requests to my server do not go through any processing at all, so the server strain should be incredibly minimal. Between that, and how responsive DreamHost is, I expect to be well prepared if I am ever Internet-famous for fifteen minutes. This wasn't part of my requirements, but it is a cool side-effect of using Pelican.
