#!/usr/bin/env python
# -*- coding/ utf-8 -*- #

AUTHOR = u"Edward Delaporte"
SITENAME = u"Edward's Journal"
SITEURL = 'http://edward.delaporte.us'

# TIMEZONE = 'USA/Chicago'
TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = 'en'

# Blogroll
LINKS =  (
('Ed The Dev.com', 'http://edthedev.com'),
('Ben Writes', 'http://ben-writes.com'),
('Edward IV on Technology Leaderhsip', 'http://ecdiv.blogspot.com/'),
('Pelican', 'http://docs.notmyidea.org/alexis/pelican/'),
('Python.org', 'http://python.org'),
('Jinja2', 'http://jinja.pocoo.org'),
)

# Social widget
SOCIAL = (
    ('GitHub', 'https://github.com/edthedev/'),
    ('Google+', 
'https://plus.google.com/app/basic/116977822564125782474/posts?cbp=7egdqvwdfj8n'), 
    ('LinkedIn', 
        'http://www.linkedin.com/in/edwarddelaportev'),
) 

DEFAULT_PAGINATION = 3

GITHUB_ACTIVITY_FEED = 'https://github.com/edthedev.atom'

# PLUGINS = ['pelican.plugins.github']

# TODO: Tag Cloud
# https://pelican.readthedocs.org/en/latest/settings.html#tag-cloud

#Setting name (default value)    What does it do?
# TAG_CLOUD_STEPS (4) Count of different font sizes in the tag cloud.
TAG_CLOUD_STEPS = 4
TAG_CLOUD_MAX_ITEMS = 50
# TAG_CLOUD_MAX_ITEMS (100)   Maximum number of tags in the cloud.

# THEME = '/home/delaporte/etc/themes/delaporte'
THEME = 'pelican-themes/bootstrap2'
# THEME_STATIC_PATHS = ['static']
# CSS_FILE = 'main.css'

PIWIK_URL = 'traffic.delaporte.us'
PIWIK_SITE_ID = '3'

TEMPLATE_PAGES = {
        'html/daytime.html': 'Hacking/daytime.html',
        }
