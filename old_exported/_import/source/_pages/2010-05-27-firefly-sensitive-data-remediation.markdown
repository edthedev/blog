---
layout: page
title: "Firefly"
date: 2010-05-27 07:36:43
link: http://www.edthedev.com/firefly-sensitive-data-remediation/
published: true
comments: true
---
We're working hard at the University of Illinois to properly protect the sensitive data that our students, faculty, and staff store on our computer systems. Creating [Firefly](http://firefly.uiuc.edu) was part of that effort.

Firefly has been used to scan more than 7,000 computers on campus. In those runs, Firefly has scanned more than 89 million files and identified just under 2 million files that may contain sensitive data.

Firefly is a Windows program that installs seamlessly and produces a report of potential sensitive information on your computer. It also sends non-sensitive statistical data to our server so that we can try to measure the size of the problem, and how much Firefly is helping.

Finding sensitive information is like trying to find several million needles scattered in haystacks the size of the entire state. A lot of terrific people in the public sector and private sector are working on the problem. It has been my privilege to create and contribute Firefly to the effort.