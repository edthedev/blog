---
layout: page
title: "Websites"
date: 2010-05-20 20:39:16
link: http://www.edthedev.com/websites/
published: true
comments: true
---
I am supporting quite a few websites, including:

	- [Ben Writes!](http://ben-writes.com/) - Benjamin Delaporte's writing blog.
	- [Boy n' Panda .com](http://boynpanda.com) - Benjamin Delaporte wrote a story, I illustrated it, and now we're recording it as a podcast.
	- [Duane Hatch .com](http://duanehatch.com) - Grandpa needed a website, so my sister and I created one for him.
