---
layout: page
title: "Minecraft"
date: 2011-06-14 17:50:36
link: http://www.edthedev.com/?page_id=11180
published: false
comments: false
---
I run a <a href='http://minecraft.net'>Minecraft</a> game server for colleagues, friends and relatives. It's a relaxed atmosphere adventure and building server, running a ridiculous number of plugins.

Server URL: minecraft.edthedev.com
World Map: http://minecraft.edthedev.com/map
<embed type="application/x-shockwave-flash" src="https://picasaweb.google.com/s/c/bin/slideshow.swf" width="288" height="192" flashvars="host=picasaweb.google.com&captions=1&hl=en_US&feat=flashalbum&RGB=0x000000&feed=https%3A%2F%2Fpicasaweb.google.com%2Fdata%2Ffeed%2Fapi%2Fuser%2Fuiucedward%2Falbumid%2F5567496523069217025%3Falt%3Drss%26kind%3Dphoto%26hl%3Den_US" pluginspage="http://www.macromedia.com/go/getflashplayer"></embed>