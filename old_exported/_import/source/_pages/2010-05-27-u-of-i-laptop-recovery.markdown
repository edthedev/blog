---
layout: page
title: "U of I Laptop Recovery"
date: 2010-05-27 07:41:03
link: http://www.edthedev.com/u-of-i-laptop-recovery/
published: true
comments: true
---
The University of Illinois Network Security team offers a computer registration service for campus networked devices. Registering a device used on campus increases the chances that Campus Police (with help from Network Security) can recover the device if it is stolen.
I'm a big fan of this service because I wrote it for them.

[http://www.cites.illinois.edu/security/register/](http://www.cites.illinois.edu/security/register/)