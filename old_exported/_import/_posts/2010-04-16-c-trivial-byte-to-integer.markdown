---
layout: post
title: "C# Trivial Byte to Integer"
date: 2010-04-16 20:16:30
link: http://www.edthedev.com/2010/c-trivial-byte-to-integer/
categories:
- Example Code
published: false
comments: true
---
I recently needed to trivially convert a byte into an integer (any integer, in fact). This naive approach isn't good enough for most situations. Normally you're going to check the endian-ness of your operating system if you want to get the original intent of the byte back later.

That said, if your byte was generated [randomly](http://www.edthedev.com/2010/get-a-long-ran…from-devrandom/), (or maybe from [SHA](http://en.wikipedia.org/wiki/Secure_Hash_Algorithm)) and you just want to get any old integer result from it, this code may come in handy:


<code>
private static int ByteToInteger(Byte myByte)
{
  return (int)myByte;
}
</code>


This is basic grade-school C knowledge; but if you're like me, you spend enough time coding in other languages that you might someday forget how to ask where the bathroom is in your native one.