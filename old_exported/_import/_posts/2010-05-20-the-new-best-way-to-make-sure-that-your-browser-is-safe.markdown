---
layout: post
title: "The new best way to make sure that your browser is safe."
date: 2010-05-20 20:42:51
link: http://www.edthedev.com/2010/the-new-best-way-to-make-sure-that-your-browser-is-safe/
categories:
- Security
- Solutions
published: true
comments: true
---
Browser plugins are rapidly becoming the most common way to get a computer virus infection.

Normally I lecture people about switching to Firefox, installing NoScript and checking at least monthly that Java and Adobe are up to date. The folks at Mozilla seem to be on a quest to save me from forever repeating those instructions, and I thank them kindly.

My instructions are now a bit simpler:

1. Switch to Firefox

2. Use this website to make sure that you are safe.
[https://www.mozilla.com/en-US/plugincheck/](https://www.mozilla.com/en-US/plugincheck/)

3. Check back with the website weekly.