---
layout: post
title: "Recent Commits December 24, 2011"
date: 2011-12-24 17:54:17
link: http://www.edthedev.com/2011/recent-commits-december-24-2011/
categories:
- Example Code
published: true
comments: false
---
<ul class="scrd_digest">
- [Nicer to just set for current directory. If you need to set recursively...edit this file...:P](https://bitbucket.org/edthedev/edthedev/changeset/f35486fe67e9)
<div>Nicer to just set for current directory. If you need to set recursively...edit this file...:P
  
  
  
  
  
  
  - [
  coding/svn-ignore
  ](https://bitbucket.org/edthedev/edthedev/src/f35486fe67e9/coding/svn-ignore)(3 lines added, 2 lines removed)
  
  </div>

- [Added a script to make svn-ignore act like .hgignore](https://bitbucket.org/edthedev/edthedev/changeset/0894da117e77)
<div>Added a script to make svn-ignore act like .hgignore
  
  
  
  
  
  
  - [
  coding/svn-ignore
  ](https://bitbucket.org/edthedev/edthedev/src/0894da117e77/coding/svn-ignore)(5 lines added, 0 lines removed)
  
  </div>

- [Fixed the bug, made the output look a bit nicer.](https://bitbucket.org/edthedev/edthedev/changeset/ad1a6fb751f2)
<div>Fixed the bug, made the output look a bit nicer.
  
  
  
  
  
  
  - [
  coding/svn-st-all
  ](https://bitbucket.org/edthedev/edthedev/src/ad1a6fb751f2/coding/svn-st-all)(15 lines added, 4 lines removed)
  
  </div>

- [Script for making sure SVN is checked in. Doesn't work correctly; took longer than just checking the directories by hand. May revisit.](https://bitbucket.org/edthedev/edthedev/changeset/1a5b687fe6a6)
<div>Script for making sure SVN is checked in. Doesn't work correctly; took longer than just checking the directories by hand. May revisit.
  
  
  
  
  
  
  - [
  coding/svn-st-all
  ](https://bitbucket.org/edthedev/edthedev/src/1a5b687fe6a6/coding/svn-st-all)(26 lines added, 0 lines removed)
  
  </div>

- [Improvements to list questions Vim document helper.](https://bitbucket.org/edthedev/edthedev/changeset/68c3a4364f43)
<div>Improvements to list questions Vim document helper.
  
  
  
  
  
  
  - [
  text/list-questions
  ](https://bitbucket.org/edthedev/edthedev/src/68c3a4364f43/text/list-questions)(5 lines added, 2 lines removed)
  
  
  - [
  text/make_header
  ](https://bitbucket.org/edthedev/edthedev/src/68c3a4364f43/text/make_header)(23 lines added, 13 lines removed)
  
  
  - [
  vimrc_edthedev
  ](https://bitbucket.org/edthedev/edthedev/src/68c3a4364f43/vimrc_edthedev)(6 lines added, 1 lines removed)
  
  </div>

- [bugfixes in list-questions and better vim bindings for same](https://bitbucket.org/edthedev/edthedev/changeset/f8f334797823)
<div>bugfixes in list-questions and better vim bindings for same
  
  
  
  
  
  
  - [
  text/list-questions
  ](https://bitbucket.org/edthedev/edthedev/src/f8f334797823/text/list-questions)(1 lines added, 1 lines removed)
  
  
  - [
  vimrc_edthedev
  ](https://bitbucket.org/edthedev/edthedev/src/f8f334797823/vimrc_edthedev)(10 lines added, 1 lines removed)
  
  </div>

- [Merged.](https://bitbucket.org/edthedev/edthedev/changeset/3ee721472ee2)
<div>Merged.
  
  
  
  
  
  
  - [
  android/backup-droid-full
  ](https://bitbucket.org/edthedev/edthedev/src/3ee721472ee2/android/backup-droid-full)(1 lines added, 1 lines removed)
  
  
  - [
  android/backup-droid-quick
  ](https://bitbucket.org/edthedev/edthedev/src/3ee721472ee2/android/backup-droid-quick)(4 lines added, 5 lines removed)
  
  
  - [
  android/pull-droid-pictures
  ](https://bitbucket.org/edthedev/edthedev/src/3ee721472ee2/android/pull-droid-pictures)(1 lines added, 0 lines removed)
  
  
  - [
  android/sync-droid-music
  ](https://bitbucket.org/edthedev/edthedev/src/3ee721472ee2/android/sync-droid-music)(10 lines added, 6 lines removed)
  
  
  - [
  minecraft/backup_worlds
  ](https://bitbucket.org/edthedev/edthedev/src/3ee721472ee2/minecraft/backup_worlds)(20 lines added, 0 lines removed)
  
  
  - [
  minecraft/make_maps
  ](https://bitbucket.org/edthedev/edthedev/src/3ee721472ee2/minecraft/make_maps)(0 lines added, 0 lines removed)
  
  
  - [
  minecraft/merge-minecraft-worlds
  ](https://bitbucket.org/edthedev/edthedev/src/3ee721472ee2/minecraft/merge-minecraft-worlds)(72 lines added, 0 lines removed)
  
  
  - [
  minecraft/minecraft
  ](https://bitbucket.org/edthedev/edthedev/src/3ee721472ee2/minecraft/minecraft)(1 lines added, 5 lines removed)
  
  </div>

- [Removed org stuff.
Added list-questions](https://bitbucket.org/edthedev/edthedev/changeset/0becfa101370)
<div>Removed org stuff.
Added list-questions
  
  
  
  
  
  
  - [
  config/aero_snap_full
  ](https://bitbucket.org/edthedev/edthedev/src/0becfa101370/config/aero_snap_full)(14 lines added, 0 lines removed)
  
  
  - [
  config/aero_snap_left
  ](https://bitbucket.org/edthedev/edthedev/src/0becfa101370/config/aero_snap_left)(20 lines added, 0 lines removed)
  
  
  - [
  config/aero_snap_right
  ](https://bitbucket.org/edthedev/edthedev/src/0becfa101370/config/aero_snap_right)(14 lines added, 0 lines removed)
  
  
  - [
  config/cleanup-all-the-downloads
  ](https://bitbucket.org/edthedev/edthedev/src/0becfa101370/config/cleanup-all-the-downloads)(1 lines added, 0 lines removed)
  
  
  - [
  config/roku
  ](https://bitbucket.org/edthedev/edthedev/src/0becfa101370/config/roku)(97 lines added, 0 lines removed)
  
  
  - [
  edthedev/org-web
  ](https://bitbucket.org/edthedev/edthedev/src/0becfa101370/edthedev/org-web)(0 lines added, 7 lines removed)
  
  
  - [
  edthedev/org.py
  ](https://bitbucket.org/edthedev/edthedev/src/0becfa101370/edthedev/org.py)(10 lines added, 5 lines removed)
  
  
  - [
  minecraft/copy-worlds-from
  ](https://bitbucket.org/edthedev/edthedev/src/0becfa101370/minecraft/copy-worlds-from)(14 lines added, 5 lines removed)
  
  
  - [
  org/aliases
  ](https://bitbucket.org/edthedev/edthedev/src/0becfa101370/org/aliases)(0 lines added, 30 lines removed)
  
  
  - [
  org/archive
  ](https://bitbucket.org/edthedev/edthedev/src/0becfa101370/org/archive)(0 lines added, 26 lines removed)
  
  
  - [
  org/asciidown
  ](https://bitbucket.org/edthedev/edthedev/src/0becfa101370/org/asciidown)(0 lines added, 30 lines removed)
  
  
  - [
  org/cleanFileName
  ](https://bitbucket.org/edthedev/edthedev/src/0becfa101370/org/cleanFileName)(0 lines added, 26 lines removed)
  
  
  - [
  org/done
  ](https://bitbucket.org/edthedev/edthedev/src/0becfa101370/org/done)(0 lines added, 38 lines removed)
  
  
  - [
  org/due
  ](https://bitbucket.org/edthedev/edthedev/src/0becfa101370/org/due)(0 lines added, 21 lines removed)
  
  
  - [
  org/fixtags
  ](https://bitbucket.org/edthedev/edthedev/src/0becfa101370/org/fixtags)(0 lines added, 39 lines removed)
  
  
  - [
  org/hud
  ](https://bitbucket.org/edthedev/edthedev/src/0becfa101370/org/hud)(0 lines added, 4 lines removed)
  
  
  - [
  org/hud.cgi
  ](https://bitbucket.org/edthedev/edthedev/src/0becfa101370/org/hud.cgi)(0 lines added, 40 lines removed)
  
  
  - [
  org/journal
  ](https://bitbucket.org/edthedev/edthedev/src/0becfa101370/org/journal)(0 lines added, 12 lines removed)
  
  
  - [
  org/logit
  ](https://bitbucket.org/edthedev/edthedev/src/0becfa101370/org/logit)(0 lines added, 53 lines removed)
  
  
  - [
  org/merge
  ](https://bitbucket.org/edthedev/edthedev/src/0becfa101370/org/merge)(0 lines added, 80 lines removed)
  
  
  - [
  org/monday
  ](https://bitbucket.org/edthedev/edthedev/src/0becfa101370/org/monday)(0 lines added, 26 lines removed)
  
  
  - [
  org/new_file
  ](https://bitbucket.org/edthedev/edthedev/src/0becfa101370/org/new_file)(0 lines added, 42 lines removed)
  
  
  - [
  org/newnote
  ](https://bitbucket.org/edthedev/edthedev/src/0becfa101370/org/newnote)(0 lines added, 51 lines removed)
  
  
  - [
  org/next
  ](https://bitbucket.org/edthedev/edthedev/src/0becfa101370/org/next)(0 lines added, 115 lines removed)
  
  
  - [
  org/org
  ](https://bitbucket.org/edthedev/edthedev/src/0becfa101370/org/org)(0 lines added, 145 lines removed)
  
  
  - [
  org/org.html
  ](https://bitbucket.org/edthedev/edthedev/src/0becfa101370/org/org.html)(0 lines added, 72 lines removed)
  
  
  - [
  org/org_browse
  ](https://bitbucket.org/edthedev/edthedev/src/0becfa101370/org/org_browse)(0 lines added, 2 lines removed)
  
  
  - [
  org/projects
  ](https://bitbucket.org/edthedev/edthedev/src/0becfa101370/org/projects)(0 lines added, 71 lines removed)
  
  
  - [
  org/sample
  ](https://bitbucket.org/edthedev/edthedev/src/0becfa101370/org/sample)(0 lines added, 24 lines removed)
  
  
  - [
  org/status
  ](https://bitbucket.org/edthedev/edthedev/src/0becfa101370/org/status)(0 lines added, 33 lines removed)
  
  
  - [
  org/summary
  ](https://bitbucket.org/edthedev/edthedev/src/0becfa101370/org/summary)(0 lines added, 70 lines removed)
  
  
  - [
  org/tagcloud
  ](https://bitbucket.org/edthedev/edthedev/src/0becfa101370/org/tagcloud)(0 lines added, 59 lines removed)
  
  
  - [
  org/textbar
  ](https://bitbucket.org/edthedev/edthedev/src/0becfa101370/org/textbar)(0 lines added, 50 lines removed)
  
  
  - [
  org/weekend
  ](https://bitbucket.org/edthedev/edthedev/src/0becfa101370/org/weekend)(0 lines added, 3 lines removed)
  
  
  - [
  org/wordpress.py
  ](https://bitbucket.org/edthedev/edthedev/src/0becfa101370/org/wordpress.py)(0 lines added, 46 lines removed)
  
  
  - [
  org/wordpresslib.py
  ](https://bitbucket.org/edthedev/edthedev/src/0becfa101370/org/wordpresslib.py)(0 lines added, 379 lines removed)
  
  
  - [
  text/list-questions
  ](https://bitbucket.org/edthedev/edthedev/src/0becfa101370/text/list-questions)(36 lines added, 0 lines removed)
  
  </div>


Digest powered by [RSS Digest](http://www.rssdigestpro.com)
