---
layout: post
title: "Feeling Paranoid on Windows?"
date: 2008-05-01 12:59:19
link: http://www.edthedev.com/2008/OLD168/
published: true
comments: true
---
[Secunia]() provides a free [Personal Software Inspector](https://psi.secunia.com/). It's very easy to use, and brings your Windows security to a whole new level. Give it a try.

Naturally, there's no point in closing the windows if the door is open, so read and follow my [Basic Windows Security Walkthrough]() first.

Enjoy!
- Edward