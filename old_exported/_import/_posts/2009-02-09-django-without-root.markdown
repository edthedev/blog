---
layout: post
title: "Django without Root"
date: 2009-02-09 14:02:34
link: http://www.edthedev.com/?p=10386
categories:
- Random Geekiness
published: false
comments: true
---
This is example code to put in your .htaccess file if you want to run Python on a host that has Apache and mod_python, but doesn't give you root access to install Django. I'm assuming that you have full access to '/services/myProject', and that you have an Apache web root already setup for you.

Install the Django source libraries into a new directory called '/services/myProject/lib' directory; and install your django project code into '/services/myProject/src'. Then add the code below into your .htaccess file in your web root

<code>```

SetHandler python-program
PythonHandler django.core.handlers.modpython
SetEnv DJANGO_SETTINGS_MODULE myDjangoProject.settings
PythonOption django.root /services/myProject/src
PythonPath "['/services/myProject/lib'] + ['/services/myProject/src'] + sys.path"
PythonDebug On
```</code>