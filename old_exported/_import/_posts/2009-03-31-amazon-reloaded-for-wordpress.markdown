---
layout: post
title: "Amazon Reloaded for WordPress"
date: 2009-03-31 22:04:21
link: http://www.edthedev.com/2009/amazon-reloaded-for-wordpress/
published: true
comments: true
---
I love books, and the inevitable result is I love [Amazon.com](http://www.amazon.com). I've been signed up for the [Amazon Associates Program](https://affiliate-program.amazon.com/gp/associates/network/main.html) for quite awhile, just because I was curious about it. 

I never bothered linking to books I love because it took so long to navigate over to Amazon to generate the linked book cover image.
Thankfully, I've just discovered quite a jem of an add-on for WordPress. [Amazon Reloaded for WordPress](http://nickohrn.com/amazon-reloaded-for-wordpress/) by [Nick Ohrn](http://nickohrn.com/) is the WordPress plugin I've been dreaming of. It adds an Amazon search as a collapsable footer to the WordPress post edit page.

I like this plugin because it satisfies a need; and I also like it because it's interface is exactly perfectly right. Nice work Nick, and many thanks.