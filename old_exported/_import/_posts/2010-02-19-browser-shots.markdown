---
layout: post
title: "Browser Shots"
date: 2010-02-19 07:42:26
link: http://www.edthedev.com/?p=10685
categories:
- Random Geekiness
published: false
comments: true
---
http://browsershots.org/ is a tool that creates screen-shots of what your website looks like in a wide variety of browsers and platforms. Handy!