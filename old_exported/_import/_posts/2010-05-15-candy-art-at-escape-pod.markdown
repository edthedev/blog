---
layout: post
title: "Candy Art at Escape Pod"
date: 2010-05-15 00:05:41
link: http://www.edthedev.com/2010/candy-art-at-escape-pod/
categories:
- Random Geekiness
published: true
comments: true
---
[Candy Art](http://escapepod.org/2009/12/24/ep230-candy-art/) is a surprisingly beautiful gritty tale of real love in a realistic future.