---
layout: post
title: "Another Golden Rule"
date: 2008-05-06 14:59:40
link: http://www.edthedev.com/2008/OLD171/
published: true
comments: true
---
Here's a new entry in Edwards rules of Application Development:

"Only once your customer loves the way it looks, will they be ready to talk about how it should work."

This is why mock-ups are vital to a project's success. People don't naturally look critically at the functionality until the interface is cleaned up. So the sooner a clean interface can be presented, the sooner the project starts making real progress.