---
layout: post
title: "Configuring Illinois VPN on Ubuntu"
date: 2009-12-13 20:45:06
link: http://www.edthedev.com/2009/configuring-illinois-vpn-on-ubuntu/
categories:
- Security
- Solutions
published: true
comments: true
---
Need to connect to the University of Illinois VPN from an Ubuntu computer? Illinois supports a Microsoft PPTP VPN connection. CITES doesn't provide detailed instructions for connecting from Ubuntu, but between CITES' web help and the Ubuntu Wiki, you're covered.

Use the settings from CITES for your username and password:
[http://www.cites.illinois.edu/vpn/otherclients.html](http://www.cites.illinois.edu/vpn/otherclients.html)

And use the Ubuntu Wiki settings for everything else:
[https://wiki.ubuntu.com/VPN](https://wiki.ubuntu.com/VPN)
Follow the instructions at the bottom of the page, under VPN Setup under 8.10. They work for Ubuntu 9.10 as well.