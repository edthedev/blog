---
layout: post
title: "Relatively speaking, I&#039;m normal."
date: 2007-12-07 11:28:23
link: http://www.edthedev.com/2007/OLD71/
published: true
comments: true
---
[Some comics][1] are so strange that by comparison I seem perfectly normal.

[1]: http://www.beaverandsteve.com/index.php?comic=319 "A Recent Beaver and Steve Comic"