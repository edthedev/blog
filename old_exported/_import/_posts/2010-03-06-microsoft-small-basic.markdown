---
layout: post
title: "Microsoft Small Basic"
date: 2010-03-06 22:00:22
link: http://www.edthedev.com/2010/microsoft-small-basic/
categories:
- Programming
published: true
comments: true
---
I have been hunting for a language to just play with for a long time now. (Pretty much ever since Python became my primary programming language at work.) I want a language that I can write quick simple expressive toys in; and then post them (both source and executable) to share with friends. I want a language to socialize in. (You will never hear another programmer say that, I betcha.)

Microsoft Small Basic might finally be that language:
http://msdn.microsoft.com/en-us/beginner/ff384126.aspx

As an added bonus, it uses Microsoft Silverlight - the future of the internet, if you believe Rockford Lhotka.
Although the existence of Silverlight makes me gag, but the fact that even my Apple Fanboy friends can run it is actually pretty sweet.