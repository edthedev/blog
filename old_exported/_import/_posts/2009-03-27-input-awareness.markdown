---
layout: post
title: "Input Awareness"
date: 2009-03-27 16:13:03
link: http://www.edthedev.com/2009/input-awareness/
categories:
- Security
- Programming
published: true
comments: true
---
How many ways can malicious input reach your web application? According to [OWasp](https://www.owasp.org/):
• Browser input
• Cookies
• Property files
• External processes
• Data feeds
• Service responses
• Flat files
• Command line parameters
• Environment variables
Any of these inputs could be manipulated to be malicious. Be sure to validate all the inputs that your application uses.

Check out the full OWasp guide for more cool tips: https://www.owasp.org/images/2/2e/OWASP_Code_Review_Guide-V1_1.pdf