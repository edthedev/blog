---
layout: post
title: "Cool People: Hernan Ceballos"
date: 2010-06-01 16:42:24
link: http://www.edthedev.com/2010/cool-people-hernan-ceballos/
categories:
- Random Geekiness
published: true
comments: true
---
Hernan Ceballos (http://www.flickr.com/photos/zeninho/3195095523/in/photostream/) leads research to create hardier strains of cassava to fight hunger in third world countries. In many cases, the cassava crop yield is doubled where the new strains are planted. (http://www.ciat.cgiar.org)

Why are they working with cassava? Well apparently "cassava is a vital staple for about 500 million people. Cassava's starchy roots produce more food energy per unit of land than any other staple crop." And cassava grows well in hot dry climates. (http://www.cassavachips.com/cassava.html)

Many charities are working to distribute these new strains of cassava; I learned about it through World Vision. (http://worldvision.org/NewCassava)