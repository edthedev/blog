---
layout: post
title: "Google Native Client security implications"
date: 2009-05-31 15:58:38
link: http://www.edthedev.com/2009/google-native-client-security-implications/
categories:
- Security
published: true
comments: true
---
This is a great article about the security of Google's new research program that could eventually replace ActiveX: http://www.matasano.com/log/1674/the-security-implications-of-google-native-client/

For those not familiar with the subject:
ActiveX - Very useful, and very vulnerable. If you would physically hand your computer to the owner of the website, then it's okay to run ActiveX; otherwise it's not.

Java - Solves the same problems as ActiveX solves, but it's slower and safer. Not the safest, but a lot of the safety issues have been crash-tested and fixed at this point.

Adobe Flash/Flex - It's faster than Java because it does less. In theory it could be easier to secure than Java; but it's a younger product than Java and so probably has more security surprises on the horizon.

Google Native Client - Easy to port old software to; and could do even more than Java. But it still has a lot of security fixes to make to catch up to the others in terms of safety. It's still safer than ActiveX, but everything is.

Silverlight - Microsoft's new solution to replace ActiveX. It's more secure than ActiveX because everything is; but it probably suffers the same symptoms of youth as the Google Native Client.

For safety with any of these, only allow them on websites that you thoroughly trust. The [NoScript add-on for Firefox](http://noscript.net/getit) is the best way to control which websites are allowed to run these.