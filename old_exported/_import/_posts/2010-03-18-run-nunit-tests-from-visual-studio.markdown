---
layout: post
title: "Run NUnit tests from Visual Studio"
date: 2010-03-18 09:56:51
link: http://www.edthedev.com/?p=10717
categories:
- Programming
- Solutions
published: false
comments: true
---
Project - Properties - Debug:

Start External Program: 
C:\Program Files\NUnit 2.5.2\bin\net-2.0\nunit-console.exe

Command line arguments: TestSLDAP.dll /wait /labels /xmlConsole

You may want to remove xmlConsole for everyday use, but put it back in when you're getting mysterious behavior - it's the only way to get NUnit to tell you when NUnit encounters an error.

The 'wait' flag is kind enough to make the console stay around long enough for you to read.

The 'labels' flag just looks like fun.