---
layout: post
title: "Love Perl Package Manager? No You Don&#039;t."
date: 2008-07-16 14:17:35
link: http://www.edthedev.com/2008/OLD201/
published: true
comments: true
---
Thinksing of typing in <code>ppm</code>?

Try <code>
perl -MCPAN -e shell
</code> instead.

You can thank me later.

- Edward

EDIT: I ended up installing the package I needed by hand. Perl still has no package management love for me, apparently.