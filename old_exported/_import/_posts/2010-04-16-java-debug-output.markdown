---
layout: post
title: "Java Debug Output"
date: 2010-04-16 10:30:15
link: http://www.edthedev.com/2010/java-debug-output/
categories:
- Example Code
published: true
comments: true
---
Since my last several modules have been core enough to merit creating both Java and .Net, I'm getting pretty proficient at translating between the two languages. 

Here's my Java version of my previous article about [C# Debug Output.](http://www.edthedev.com/2010/c-debug-output)


<code>
  private static final boolean DEBUG = true;
  private static void DebugOutput(String message)
  {
  DebugOutput(message, null);
  }

  private static void DebugOutput(String message, Object details)
  {
  if(DEBUG)
  {
  	System.out.println(message + ": " + details.toString());
  }
  }
</code>

