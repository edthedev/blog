---
layout: post
title: "State of Accessibility Address"
date: 2008-05-23 09:18:50
link: http://www.edthedev.com/2008/OLD182/
published: true
comments: true
---
[Marco's Accessibility Blog](http://www.marcozehe.de) posted a nice article that serves as a sort of [State of Accessibility Address](http://www.marcozehe.de/2008/04/29/are-ajax-and-accessibility-mutually-exclusive/). I enjoyed it very much. Thanks Marco!

- Edward