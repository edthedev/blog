---
layout: post
title: "PC Decrapifier"
date: 2008-11-22 14:17:11
link: http://www.edthedev.com/2008/pc-decrapifier/
categories:
- Security
published: true
comments: true
---
Most Information technology geeks re-install Windows as soon as they get their new Dell, HP or Campaq computer home from the store. They do this because the big computer vendors pre-load each computer they sell with a lot of useless software that wastes space and slows down the computer. Most of this software (commonly known as 'crapware') is installed to try to sell you something.

I'm a little less purist about the whole thing - I typically just crawl through the 'installed programs' list and remove whatever I don't want, one item at a time.

And then I discovered this gem: [The PC Decrapifier](http://www.pcworld.com/downloads/file/fid,67264-order,1-page,1/description.html). The PC Decrapifier knows all of the common crapware, and offers to uninstall them for you one at a time. It you choose to remove all of the crapware it finds, it will run in the background for about half an hour, prompting you to press 'Ok' or 'Next' every couple of minutes. (The 'Ok' and 'Next' buttons are actually part of the Dell / HP / Compaq software uninstallers, not the Decrapifier itself.)

If you haven't re-installed your operating system since you bought your computer, running the PC Decrapifier once will make your computer run noticeably faster. Did I mention the price? It's free!

Enjoy!

- Edward