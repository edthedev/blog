---
layout: post
title: "DokuWiki is the Wiki You Want"
date: 2008-09-11 21:06:14
link: http://www.edthedev.com/2008/dokuwiki-is-the-wiki-you-want/
published: true
comments: true
---
Seth Kinast says [DokuWiki is the Best Wiki](http://sethkinast.com/2006/03/26/dokuwiki-an-undiscovered-gem/); and I agree. The flat-file back-end makes migration or re-purposing the files easy. The syntax is simple and lovely. The [sidebar plugin](http://wiki.jalakai.co.uk/dokuwiki/doku.php/tutorials/start) is just awesome; and is really the only plugin you need to make your wiki perfect.