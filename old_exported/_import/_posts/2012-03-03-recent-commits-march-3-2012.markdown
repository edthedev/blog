---
layout: post
title: "Recent Commits March 3, 2012"
date: 2012-03-03 17:38:14
link: http://www.edthedev.com/2012/recent-commits-march-3-2012/
categories:
- Example Code
published: true
comments: false
---
<ul class="scrd_digest">
- [Removed old crontab](https://bitbucket.org/edthedev/edthedev/changeset/e52929442463)
<div>Removed old crontab
  
  
  
  
  
  
  - [
  minecraft/cron_server
  ](https://bitbucket.org/edthedev/edthedev/src/e52929442463/minecraft/cron_server)(0 lines added, 1 lines removed)
  
  </div>

- [Updated crontab.](https://bitbucket.org/edthedev/edthedev/changeset/9b41e0334e06)
<div>Updated crontab.
  
  
  
  
  
  
  - [
  minecraft/cron.example
  ](https://bitbucket.org/edthedev/edthedev/src/9b41e0334e06/minecraft/cron.example)(3 lines added, 2 lines removed)
  
  </div>

- [Script for publsihing the minecraft map for download.](https://bitbucket.org/edthedev/edthedev/changeset/83e9309354a9)
<div>Script for publsihing the minecraft map for download.
  
  
  
  
  
  
  - [
  minecraft/publish-map
  ](https://bitbucket.org/edthedev/edthedev/src/83e9309354a9/minecraft/publish-map)(15 lines added, 0 lines removed)
  
  </div>


Digest powered by [RSS Digest](http://www.rssdigestpro.com)
