---
layout: post
title: "Firefox Mobile Thinks Security First"
date: 2009-01-20 21:25:27
link: http://www.edthedev.com/2009/firefox-mobile-thinks-security-first/
categories:
- Security
published: true
comments: true
---
I tried the alpha version of [Fennec](http://www.mozilla.org/projects/fennec/1.0a1/releasenotes/), the mobile phone version of Firefox.

I am absolutely thrilled to see that Fennec includes basic browser security from the first pass.

As  you may remember, I don't use [Google Chrome](http://www.google.com/chrome) yet; because it lacks basic security features like the ability to selectively enable scripts for trusted pages, and the ability to disable untrusted embedded objects.

Fennec has all of these in the alpha version. Their page says they're looking for feedback. My feedback is "Nice work folks!".

- Edward

PS I hope that the mouse gesture tool-bars will be added to Firefox soon.