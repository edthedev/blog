---
layout: post
title: "Podcasting on iPod - simpler without Apple"
date: 2010-03-01 19:30:30
link: http://www.edthedev.com/?p=10701
categories:
- Solutions
published: false
comments: true
---

- Step one: Install an alternative Podcaster / RSS Player (such as http://www.nextdayoff.com/)

	- Step two: Use Firefox LiveBookmarks to subscribe to all the RSS feeds of your favorite podcasts. Be sure that the RSS feed includes an mp3 link, of you've got the wrong feed.

	- Step three: Install OPML support for Firefox. https://addons.mozilla.org/en-US/firefox/addon/2625

	- Step four: Use OPML support for Firefox to export your Podcasts as bookmarks.

	- Step five: Import the OPML file using your feed reader.

	- Step six: Never bother with iTunes again; always have your feeds up to date directly on your iPod.


Update:
There doesn't seem to be a simple solution just now; unless you're me or know me. I had to make two hacks to get things working:

	-  I had to obtain the free but abandoned version of Podcaster that actually works (the version on the app store does not work at the moment).

	- I had to host the .ompl file on my own website, since a public website is the only place that Podcaster can import from. Normally I would recommend Google Reader or FeedBurner, but these only provide downloadable opml files, rather than hosted ones. This only matters because the iPod/iPhone is so limited in it's treatment of files. But there it is.
