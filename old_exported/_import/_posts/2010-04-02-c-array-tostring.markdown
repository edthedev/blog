---
layout: post
title: "C# Array.ToString"
date: 2010-04-02 08:52:48
link: http://www.edthedev.com/2010/c-array-tostring/
categories:
- Example Code
published: true
comments: true
---
Got tired of searching up this line elsewhere. Now that I'm writing this, I'll probably memorize it in the process. That'll be nice.


<code>
public override String ToString() 
{
  return "' Search scope: " + this.searchScope
  + " Filter: '" + this.getFilter()
  + "' Search base: '" + this.searchBase + "'"
  + " Returned attributes: {" + String.Join(" , ", this.returnedAttributes.ToArray()) + "}";
}
</code>

