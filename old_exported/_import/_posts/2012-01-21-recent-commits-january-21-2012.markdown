---
layout: post
title: "Recent Commits January 21, 2012"
date: 2012-01-21 17:33:29
link: http://www.edthedev.com/2012/recent-commits-january-21-2012/
categories:
- Example Code
published: true
comments: false
---
<ul class="scrd_digest">
- [Added the config files that the script now uses.](https://bitbucket.org/edthedev/edthedev/changeset/ee18130913e7)
<div>Added the config files that the script now uses.
  
  
  
  
  
  
  - [
  kindle/books.txt
  ](https://bitbucket.org/edthedev/edthedev/src/ee18130913e7/kindle/books.txt)(1 lines added, 0 lines removed)
  
  
  - [
  kindle/make-books.ini.example
  ](https://bitbucket.org/edthedev/edthedev/src/ee18130913e7/kindle/make-books.ini.example)(8 lines added, 0 lines removed)
  
  </div>

- [Seems to work nicely.
Better title.](https://bitbucket.org/edthedev/edthedev/changeset/14bc10f240fb)
<div>Seems to work nicely.
Better title.
  
  
  
  
  
  
  - [
  kindle/make-books.py
  ](https://bitbucket.org/edthedev/edthedev/src/14bc10f240fb/kindle/make-books.py)(2 lines added, 1 lines removed)
  
  </div>

- [Looks like it's working.](https://bitbucket.org/edthedev/edthedev/changeset/0703471cdd1d)
<div>Looks like it's working.
  
  
  
  
  
  
  - [
  kindle/make-books.py
  ](https://bitbucket.org/edthedev/edthedev/src/0703471cdd1d/kindle/make-books.py)(24 lines added, 19 lines removed)
  
  </div>

- [Adding email function](https://bitbucket.org/edthedev/edthedev/changeset/17e5ef88726d)
<div>Adding email function
  
  
  
  
  
  
  - [
  kindle/make-books.py
  ](https://bitbucket.org/edthedev/edthedev/src/17e5ef88726d/kindle/make-books.py)(45 lines added, 32 lines removed)
  
  </div>

- [Whatever](https://bitbucket.org/edthedev/edthedev/changeset/cbca200fbc2b)
<div>Whatever
  
  
  
  
  
  
  - [
  kindle/droids.py
  ](https://bitbucket.org/edthedev/edthedev/src/cbca200fbc2b/kindle/droids.py)(0 lines added, 16 lines removed)
  
  
  - [
  kindle/make-books.py
  ](https://bitbucket.org/edthedev/edthedev/src/cbca200fbc2b/kindle/make-books.py)(65 lines added, 0 lines removed)
  
  </div>

- [Slightly more versatile version - still a hack because I am lazy.](https://bitbucket.org/edthedev/edthedev/changeset/b19817a3b331)
<div>Slightly more versatile version - still a hack because I am lazy.
  
  
  
  
  
  
  - [
  kindle/droids.py
  ](https://bitbucket.org/edthedev/edthedev/src/b19817a3b331/kindle/droids.py)(16 lines added, 0 lines removed)
  
  </div>

- [Updated vimrc with nice commands for developing bottle.py scripts.
Fixed bugs in cardgame.py](https://bitbucket.org/edthedev/edthedev/changeset/e4f76f880818)
<div>Updated vimrc with nice commands for developing bottle.py scripts.
Fixed bugs in cardgame.py
  
  
  
  
  
  
  - [
  games/cardgame.py
  ](https://bitbucket.org/edthedev/edthedev/src/e4f76f880818/games/cardgame.py)(26 lines added, 9 lines removed)
  
  
  - [
  vimrc_edthedev
  ](https://bitbucket.org/edthedev/edthedev/src/e4f76f880818/vimrc_edthedev)(6 lines added, 0 lines removed)
  
  </div>

- [Getting teletrran copy in sync](https://bitbucket.org/edthedev/edthedev/changeset/20ba60079a48)
<div>Getting teletrran copy in sync
  
  
  
  
  
  
  - [
  add_to_your_profile
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/add_to_your_profile)(2 lines added, 0 lines removed)
  
  
  - [
  android/backup-droid-full
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/android/backup-droid-full)(1 lines added, 1 lines removed)
  
  
  - [
  android/backup-droid-quick
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/android/backup-droid-quick)(4 lines added, 5 lines removed)
  
  
  - [
  android/pull-droid-pictures
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/android/pull-droid-pictures)(1 lines added, 0 lines removed)
  
  
  - [
  android/sync-droid-music
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/android/sync-droid-music)(10 lines added, 6 lines removed)
  
  
  - [
  coding/aliases
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/coding/aliases)(2 lines added, 0 lines removed)
  
  
  - [
  coding/listmethods
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/coding/listmethods)(25 lines added, 12 lines removed)
  
  
  - [
  coding/newscript
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/coding/newscript)(0 lines added, 9 lines removed)
  
  
  - [
  coding/svn-ignore
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/coding/svn-ignore)(6 lines added, 0 lines removed)
  
  
  - [
  coding/svn-st-all
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/coding/svn-st-all)(37 lines added, 0 lines removed)
  
  
  - [
  concept/keyring.py
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/concept/keyring.py)(38 lines added, 0 lines removed)
  
  
  - [
  config/aero_snap_full
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/config/aero_snap_full)(14 lines added, 0 lines removed)
  
  
  - [
  config/aero_snap_left
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/config/aero_snap_left)(20 lines added, 0 lines removed)
  
  
  - [
  config/aero_snap_right
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/config/aero_snap_right)(14 lines added, 0 lines removed)
  
  
  - [
  config/backup-home
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/config/backup-home)(17 lines added, 0 lines removed)
  
  
  - [
  config/builddotfiles.py
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/config/builddotfiles.py)(83 lines added, 0 lines removed)
  
  
  - [
  config/cleanup-all-the-downloads
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/config/cleanup-all-the-downloads)(38 lines added, 0 lines removed)
  
  
  - [
  config/copy-pictures-from
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/config/copy-pictures-from)(13 lines added, 0 lines removed)
  
  
  - [
  config/install_sun_java
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/config/install_sun_java)(8 lines added, 3 lines removed)
  
  
  - [
  config/lock
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/config/lock)(3 lines added, 0 lines removed)
  
  
  - [
  config/roku
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/config/roku)(114 lines added, 0 lines removed)
  
  
  - [
  config/twoScreens
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/config/twoScreens)(10 lines added, 3 lines removed)
  
  
  - [
  edthedev/org.py
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/edthedev/org.py)(693 lines added, 587 lines removed)
  
  
  - [
  games/cardgame.py
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/games/cardgame.py)(105 lines added, 0 lines removed)
  
  
  - [
  kindle/website2kindle
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/kindle/website2kindle)(19 lines added, 0 lines removed)
  
  
  - [
  minecraft/backup-worlds-local
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/minecraft/backup-worlds-local)(18 lines added, 0 lines removed)
  
  
  - [
  minecraft/backup_worlds
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/minecraft/backup_worlds)(6 lines added, 4 lines removed)
  
  
  - [
  minecraft/copy-worlds-from
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/minecraft/copy-worlds-from)(29 lines added, 0 lines removed)
  
  
  - [
  minecraft/make_maps
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/minecraft/make_maps)(0 lines added, 0 lines removed)
  
  
  - [
  minecraft/merge-minecraft-worlds
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/minecraft/merge-minecraft-worlds)(72 lines added, 0 lines removed)
  
  
  - [
  minecraft/minecraft
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/minecraft/minecraft)(0 lines added, 0 lines removed)
  
  
  - [
  org/aliases
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/org/aliases)(0 lines added, 0 lines removed)
  
  
  - [
  org/archive
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/org/archive)(0 lines added, 26 lines removed)
  
  
  - [
  org/asciidown
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/org/asciidown)(0 lines added, 30 lines removed)
  
  
  - [
  org/cleanFileName
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/org/cleanFileName)(0 lines added, 26 lines removed)
  
  
  - [
  org/done
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/org/done)(0 lines added, 38 lines removed)
  
  
  - [
  org/due
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/org/due)(0 lines added, 21 lines removed)
  
  
  - [
  org/fixtags
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/org/fixtags)(0 lines added, 39 lines removed)
  
  
  - [
  org/journal
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/org/journal)(0 lines added, 11 lines removed)
  
  
  - [
  org/logit
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/org/logit)(0 lines added, 0 lines removed)
  
  
  - [
  org/merge
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/org/merge)(0 lines added, 80 lines removed)
  
  
  - [
  org/monday
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/org/monday)(0 lines added, 26 lines removed)
  
  
  - [
  org/new_file
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/org/new_file)(0 lines added, 40 lines removed)
  
  
  - [
  org/newnote
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/org/newnote)(0 lines added, 50 lines removed)
  
  
  - [
  org/next
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/org/next)(0 lines added, 113 lines removed)
  
  
  - [
  org/org
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/org/org)(0 lines added, 142 lines removed)
  
  
  - [
  org/org.html
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/org/org.html)(0 lines added, 72 lines removed)
  
  
  - [
  org/orglib.pyc
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/org/orglib.pyc)(0 lines added, 0 lines removed)
  
  
  - [
  org/projects
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/org/projects)(0 lines added, 71 lines removed)
  
  
  - [
  org/sample
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/org/sample)(... lines added, ... lines removed)
  
  
  - [
  org/status
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/org/status)(... lines added, ... lines removed)
  
  
  - [
  org/summary
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/org/summary)(... lines added, ... lines removed)
  
  
  - [
  org/tagcloud
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/org/tagcloud)(... lines added, ... lines removed)
  
  
  - [
  org/textbar
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/org/textbar)(... lines added, ... lines removed)
  
  
  - [
  org/weekend
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/org/weekend)(... lines added, ... lines removed)
  
  
  - [
  org/wordpress.py
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/org/wordpress.py)(... lines added, ... lines removed)
  
  
  - [
  org/wordpresslib.py
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/org/wordpresslib.py)(... lines added, ... lines removed)
  
  
  - [
  packaging/release-source
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/packaging/release-source)(... lines added, ... lines removed)
  
  
  - [
  text/aliases
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/text/aliases)(... lines added, ... lines removed)
  
  
  - [
  text/clean-numbers
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/text/clean-numbers)(... lines added, ... lines removed)
  
  
  - [
  text/confluence-python-api.py
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/text/confluence-python-api.py)(... lines added, ... lines removed)
  
  
  - [
  text/display_missing
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/text/display_missing)(... lines added, ... lines removed)
  
  
  - [
  text/hours_since_morning
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/text/hours_since_morning)(... lines added, ... lines removed)
  
  
  - [
  text/list-headers
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/text/list-headers)(... lines added, ... lines removed)
  
  
  - [
  text/list-questions
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/text/list-questions)(... lines added, ... lines removed)
  
  
  - [
  text/make_header
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/text/make_header)(... lines added, ... lines removed)
  
  
  - [
  text/send-to-wordpress
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/text/send-to-wordpress)(... lines added, ... lines removed)
  
  
  - [
  text/textbar
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/text/textbar)(... lines added, ... lines removed)
  
  
  - [
  vim/Proj
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/vim/Proj)(... lines added, ... lines removed)
  
  
  - [
  vim/hardcore_mode
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/vim/hardcore_mode)(... lines added, ... lines removed)
  
  
  - [
  vimrc_edthedev
  ](https://bitbucket.org/edthedev/edthedev/src/20ba60079a48/vimrc_edthedev)(... lines added, ... lines removed)
  
  </div>

- [added dokuwiki install script for ubuntu server....still messy job](https://bitbucket.org/edthedev/edthedev/changeset/5675fff795a3)
<div>added dokuwiki install script for ubuntu server....still messy job
  
  
  
  
  
  
  - [
  config/install-dokuwiki
  ](https://bitbucket.org/edthedev/edthedev/src/5675fff795a3/config/install-dokuwiki)(27 lines added, 0 lines removed)
  
  </div>


Digest powered by [RSS Digest](http://www.rssdigestpro.com)
