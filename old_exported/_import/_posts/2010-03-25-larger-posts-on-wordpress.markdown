---
layout: post
title: "Larger posts on WordPress"
date: 2010-03-25 20:56:25
link: http://www.edthedev.com/2010/larger-posts-on-wordpress/
categories:
- Solutions
published: true
comments: true
---
To support the PodCast audio book that we're adding over at [Boy N Panda .com](http://boynpanda.com), I needed to increase the allowed upload size on my server.

This article was really helpful:
http://wiki.dreamhost.com/index.php/PHP.ini