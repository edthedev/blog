---
layout: post
title: "Declarative programming"
date: 2010-04-16 21:42:34
link: http://www.edthedev.com/2010/declarative-programming/
categories:
- Programming
published: true
comments: true
---
This article has a cool description of something I've been working on in my own programming style lately: 

[If code describes intent, rather than incidentals, it will better stand the test of time. ](http://togaroga.com/2010/03/writing-better-code-its-imperative-that-you-are-declarative/)

I figure it also gives each compiler a better chance to optimize for whatever exotic chip-set the code someday runs on.