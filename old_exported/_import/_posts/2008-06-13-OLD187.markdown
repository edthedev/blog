---
layout: post
title: "Python"
date: 2008-06-13 09:22:27
link: http://www.edthedev.com/2008/OLD187/
published: true
comments: true
---
It's been awhile since I have posted here praising [Python](http://www.python.org/) for being the best programming language, scripting language, and generally the best freeware tool available.

So this post is to remedy that. 

Let the record show: I love [Python](http://wiki.python.org/moin/BeginnersGuide).

Okay, I try to have something useful in each post here, so here's a fun line of code.

Ever wonder how many characters 255 is?


'x' * 255



Aha! It looks like this:


'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'

