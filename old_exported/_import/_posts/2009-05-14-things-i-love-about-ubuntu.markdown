---
layout: post
title: "Things I love about Ubuntu"
date: 2009-05-14 12:24:35
link: http://www.edthedev.com/?p=10539
categories:
- Random Geekiness
published: false
comments: true
---
1. SecPanel
2. Compiz and Compiz configuration manager.
3. Avant Window Manager
4. Eclipse (used to hate it, then I found VWrapper for Eclipse)