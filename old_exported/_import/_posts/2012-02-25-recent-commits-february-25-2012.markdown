---
layout: post
title: "Recent Commits February 25, 2012"
date: 2012-02-25 17:32:51
link: http://www.edthedev.com/2012/recent-commits-february-25-2012/
categories:
- Example Code
published: true
comments: false
---
<ul class="scrd_digest">
- [added call to builddotfiles](https://bitbucket.org/edthedev/edthedev/changeset/a6afe425111d)
<div>added call to builddotfiles
  
  
  
  
  
  
  - [
  config/hg-sync-all
  ](https://bitbucket.org/edthedev/edthedev/src/a6afe425111d/config/hg-sync-all)(2 lines added, 1 lines removed)
  
  </div>

- [script to sync all repositories](https://bitbucket.org/edthedev/edthedev/changeset/6264d18c21f5)
<div>script to sync all repositories
  
  
  
  
  
  
  - [
  config/hg-sync-all
  ](https://bitbucket.org/edthedev/edthedev/src/6264d18c21f5/config/hg-sync-all)(16 lines added, 0 lines removed)
  
  </div>

- [added script to sync all changes](https://bitbucket.org/edthedev/edthedev/changeset/c178a47764ca)
<div>added script to sync all changes
  
  
  
  
  
  
  - [
  config/cleanup-all-the-downloads
  ](https://bitbucket.org/edthedev/edthedev/src/c178a47764ca/config/cleanup-all-the-downloads)(2 lines added, 0 lines removed)
  
  
  - [
  config/nook-build-books
  ](https://bitbucket.org/edthedev/edthedev/src/c178a47764ca/config/nook-build-books)(10 lines added, 0 lines removed)
  
  
  - [
  config/nook-resize-images
  ](https://bitbucket.org/edthedev/edthedev/src/c178a47764ca/config/nook-resize-images)(9 lines added, 0 lines removed)
  
  
  - [
  config/nook-sync
  ](https://bitbucket.org/edthedev/edthedev/src/c178a47764ca/config/nook-sync)(5 lines added, 0 lines removed)
  
  
  - [
  config/random-wallpaper
  ](https://bitbucket.org/edthedev/edthedev/src/c178a47764ca/config/random-wallpaper)(21 lines added, 0 lines removed)
  
  
  - [
  config/random_wallpaper
  ](https://bitbucket.org/edthedev/edthedev/src/c178a47764ca/config/random_wallpaper)(0 lines added, 21 lines removed)
  
  
  - [
  config/to-nook
  ](https://bitbucket.org/edthedev/edthedev/src/c178a47764ca/config/to-nook)(8 lines added, 0 lines removed)
  
  
  - [
  config/twoScreens
  ](https://bitbucket.org/edthedev/edthedev/src/c178a47764ca/config/twoScreens)(2 lines added, 1 lines removed)
  
  
  - [
  edthedev/text.py
  ](https://bitbucket.org/edthedev/edthedev/src/c178a47764ca/edthedev/text.py)(2 lines added, 2 lines removed)
  
  
  - [
  text/list-headers
  ](https://bitbucket.org/edthedev/edthedev/src/c178a47764ca/text/list-headers)(2 lines added, 1 lines removed)
  
  
  - [
  text/make-graph
  ](https://bitbucket.org/edthedev/edthedev/src/c178a47764ca/text/make-graph)(8 lines added, 0 lines removed)
  
  
  - [
  vimrc_edthedev
  ](https://bitbucket.org/edthedev/edthedev/src/c178a47764ca/vimrc_edthedev)(11 lines added, 4 lines removed)
  
  </div>

- [added private org server shell](https://bitbucket.org/edthedev/edthedev/changeset/f2e2d23618d5)
<div>added private org server shell
  
  
  
  
  
  
  - [
  config/roku
  ](https://bitbucket.org/edthedev/edthedev/src/f2e2d23618d5/config/roku)(5 lines added, 4 lines removed)
  
  
  - [
  private/notebook.py
  ](https://bitbucket.org/edthedev/edthedev/src/f2e2d23618d5/private/notebook.py)(11 lines added, 0 lines removed)
  
  
  - [
  private/private.wsgi
  ](https://bitbucket.org/edthedev/edthedev/src/f2e2d23618d5/private/private.wsgi)(10 lines added, 0 lines removed)
  
  </div>


Digest powered by [RSS Digest](http://www.rssdigestpro.com)
