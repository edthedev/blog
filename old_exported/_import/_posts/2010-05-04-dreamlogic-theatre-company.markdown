---
layout: post
title: "DreamLogic Theatre Company"
date: 2010-05-04 20:35:49
link: http://www.edthedev.com/2010/dreamlogic-theatre-company/
categories:
- Random Geekiness
published: false
comments: true
---
"DreamLogic Theater Company is sort of like the bizarre logic of a dream where you need to protect a soccer ball because it’s really your father’s memory of the time you went to the beach. It may be fantastical, but within the strange premise there is an internal consistency." - David Sweeney

Check out the [full interview](http://everythingisawesome-chicago.blogspot.com/2010/05/interview-david-sweeney-of-dreamlogic.html); and if you're going to be in Chicago on May 8, see if you can still get your tickets to the [Inaugural Gala](http://dreamlogictheatreworks.blogspot.com/2010/03/inaugural-gala.html).