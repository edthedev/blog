---
layout: post
title: "Markdown Syntax"
date: 2007-12-04 20:47:56
link: http://www.edthedev.com/2007/OLD70/
published: true
comments: true
---
I just added support for [Markdown syntax][1] to this blog. This will help me blog more fluidly, as it will be much easier to include references and links. Plus, I'll finally be able to really [learn Markdown][2].

[1]: http://daringfireball.net/projects/markdown/ "Markdown Description"
[2]: http://daringfireball.net/projects/markdown/basics "Markdown Basic Syntax"