---
layout: post
title: "Coding to Share Code - Day 1"
date: 2009-03-30 00:40:18
link: http://www.edthedev.com/?p=10473
categories:
- Example Code
- Coding to Share Code
- Programming
- Random Geekiness
published: false
comments: true
---
I'm building a web database to share code snippets on EdTheDev.com. When it's up and running, you'll find the system at [Code.EdTheDev.com](http://code.edthedev.com).

The interface will be built with [CodeIgniter](http://codeigniter.com), the database will be [MySQL](http://www.mysql.com/), and the hosting will be by [Dreamhost](http://dreamhost.com). I chose these resources because they are relatively transparent - they won't introduce any needless complexity.

Simplicity is important, because the goal of this project is to create an educational and fun to read blog series. If I just needed a web database, there are better options than building from scratch. 

When it's completed, this will be a good resource for programmers. But I hope it becomes more than that. I hope it's a really fun read for everyone else who isn't familiar with the many steps that go into building a system like this. It's really cool stuff. So stay tuned - this should get pretty interesting.