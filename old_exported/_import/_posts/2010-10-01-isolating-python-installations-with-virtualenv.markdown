---
layout: post
title: "Isolating python installations with virtualenv"
date: 2010-10-01 07:47:51
link: http://www.edthedev.com/2010/isolating-python-installations-with-virtualenv/
categories:
- Programming
- Solutions
published: true
comments: false
---
I write code for linux servers with diverse packages. 

Python2.5 is no longer available in Ubuntu Lucid.

Thankfully, Felix Krull is maintaining a repository:
https://launchpad.net/~fkrull/+archive/deadsnakes


# Setuptools make installing VirtualEnv easier, so let's install it from the Debian package:
sudo apt-get install python-setuptools

#Next, grab VirtualEnv: http://pypi.python.org/pypi/virtualenv
sudo easy_install virtualenv

#Now for some irony - we'll use setuptools to install 'pip', which is a nicer tool that does the same thing, but works better with virtualenv:
sudo easy_install pip

#Now we setup a project (named edthedev for example):
mkdir ~/projects
mkdir ~/virtualenv

# Create a Python2.5 virtual environment for the edthedev project
virtualenv --python=/usr/bin/python2.5 ~/virtualenv/edthedev

# Check out the project:
cd ~/projects
svn co http://svn.edthedev.com/projects/edthedev

# Activate the virutalenv that we created for this project.
source ~/virtualenv/edthedev/bin/activate

# Try running the project
~/edthedev/bin/addJiraTicket --debug

# Look at that, we're missing some required packages... 
# so use pip to install it into our virtual environment.
pip install fpconst

# But one of our packages isn't available on pypi, 
# so we will install it directly from the online source tarball
pip install http://sourceforge.net/projects/pywebsvcs/files/SOAP.py/SOAPpy%200.11.0/SOAPpy-0.11.0.tar.gz/download

# Repeat the previous few steps until your program runs properly inside the virtual environment.
~/edthedev/bin/addJiraTicket --debug

# We will save a REQUIREMENTS.txt file to include with our project to make things easier for the next user.
pip freeze > REQUIREMENTS.txt


Check out the contents of REQUIREMENTS.txt, and notice that it includes every package we've installed to get our virtual environment working, inluding the exact package version. You may want to change the '==' signs to '>=' if you trust your dependencies to maintain backward compatibility.