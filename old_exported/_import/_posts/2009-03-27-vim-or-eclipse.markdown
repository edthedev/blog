---
layout: post
title: "Vim or Eclipse"
date: 2009-03-27 16:10:29
link: http://www.edthedev.com/2009/vim-or-eclipse/
categories:
- Random Geekiness
published: true
comments: true
---
If you love Vim, but know you need Eclipse, it's okay. Just get the [Vrapper Eclipse Add-on](http://vrapper.sourceforge.net/documentation/?page=2). Like everything else I feature on EdTheDev.com, it's free and pretty awesome.

As a hard-core Vim user, this add-on finally let me embrace Eclipse.

Enjoy!