---
layout: post
title: "Add an image to your webpage."
date: 2007-11-21 17:17:01
link: http://www.edthedev.com/2007/OLD6/
published: true
comments: true
---
A picture is worth a thousand words, so let's use them.

To add a picture to your webpage, use the 'Image' tag, like in the example below.
<code>
{% img http://docs.google.com/File?id=dhn7q9bp_165hmmx4v %}
</code>
{% img http://docs.google.com/File?id=dhn7q9bp_165hmmx4v %}

Change the 'src' attribute to point to the location of the picture you want to display. <!-- break -->The 'source' attribute, <code>src="...some web location..."</code>, is what tells the web browser where to find the picture. A person's web browser will go look for the image at that web location. 

This is important to remember - the picture needs to be stored somewhere that <i>every person</i> who ever views the web-page will have access to it. That means that the picture needs to be uploaded to a web server before you use it in your web pages. My favorite web server to upload pictures to is [Google's Picasa](http://www.picasaweb.google.com).

A lesson in ethics: It is considered rude to point an image source attribute to a picture on someone else's web server. If you have permission to re-use an picture you find online; you should download the picture to your local computer, then upload the picture to a picture posting server such as [Google's Picasa](http://www.picasaweb.google.com).

That said, consider yourself granted permission to point your image source attribute directly to any pictures that I post to this blog. You can rest assured that I have already done the work of making sure that they are on a server that can handle the extra load of being linked to from various web pages.