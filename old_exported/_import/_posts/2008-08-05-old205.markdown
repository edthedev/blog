---
layout: post
title: "Advice that goes beyond Administration"
date: 2008-08-05 07:12:16
link: http://www.edthedev.com/2008/old205/
categories:
- Programming
- Solutions
published: true
comments: true
---
CyberCiti has a good article about [tips for system administrators](http://www.cyberciti.biz/tips/10-ultimate-rules-for-effective-system-administration.html); many of those tips can extend into the life of any tech-sector worker.