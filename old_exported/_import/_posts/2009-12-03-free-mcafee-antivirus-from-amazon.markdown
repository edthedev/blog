---
layout: post
title: "Free McAfee Antivirus from Amazon"
date: 2009-12-03 21:35:18
link: http://www.edthedev.com/2009/free-mcafee-antivirus-from-amazon/
categories:
- Security
published: true
comments: true
---
My sister, Illinois' expert at finding great things for free, learned of a deal that warms my security-focused heart.

Amazon.com is selling McAfee Antivirus for free ($70 with a $70 mail in rebate).

Clicking this TinyUrl will take you to the product page, appropriately crediting my sister for referring you: [http://tinyurl.com/FreeMcAfeeRebate](http://tinyurl.com/FreeMcAfeeRebate)