---
layout: post
title: "C++ is dead to me."
date: 2008-03-12 12:38:10
link: http://www.edthedev.com/2008/OLD123/
published: true
comments: true
---
I just read this [article which articulates the waning interest][1] in C++ that I have felt lately.
[1]: http://prophipsi.blogspot.com/2008/03/why-i-no-longer-like-or-use-c.html

If you're too lazy to click the link to read it, let me summarize:
C++ focuses on machine efficiency rather than programming efficiency. Since programmer hours are now quite expensive, it makes more sense to write most of the code in a faster language to write, such as Python. Then, when some part of the code really needs to be sped up (typically only 10% or less), I'll gladly write that portion in C. The author of the article also left out the issue of maintainability - one of the biggest costs in programming.