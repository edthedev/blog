---
layout: post
title: "NUnit Testing in Vb.Net"
date: 2007-12-20 09:44:06
link: http://www.edthedev.com/2007/OLD86/
published: true
comments: true
---
I spent my evening [creating NUnit tests][1] for [Firefly][3].

Overall, I'm a big fan of [NUnit][4].

So what is NUnit? It's a .Net friendly framework for running [unit tests][6] - a way to automate some quality assurance steps. 

![A screenshot of NUnit testing Firefly](http://www.edthedev.com/files/NUnitTestInterface.png)

I'm looking forward to having a strong set of NUnit tests to let me do some [code refactoring][5] of Firefly, confident that NUnit will let me know if I break anything in the process.

Now that I have your interest, read [this article][1] and [this one][2] to get started with your own NUnit tests.

[1]: http://www.byte-vision.com/NUnitAndVBArticle.aspx "NUnit Testing in Vb.Net"
[2]: http://www.15seconds.com/issue/040922.htm "NUnit Testing Walkthrough"
[3]: http://www.cites.uiuc.edu/ssnprogram/firefly/ "Firefly SSN Finder"
[4]: http://www.nunit.org/ "NUnit Home"
[5]: http://en.wikipedia.org/wiki/Refactoring "Wikipedia Entry on Code Refactoring"
[6]: http://en.wikipedia.org/wiki/Unit_test "Wikipedia Entry on Unit Testing"