---
layout: post
title: "Favorite Vim commands"
date: 2010-05-29 13:44:39
link: http://www.edthedev.com/2010/favorite-vim-commands/
categories:
- Programming
published: true
comments: true
---
<dl>
<dt> :!python % </dt>
<dd> Run the current file in Python </dd>
<dt> :% s/regex/replacement/g </dt>
<dd> Replace every instance of the regex with the given expression. </dd>
<dt> :! svn ci %</dt>
<dd> Check the current file into SVN version control.</dd>
<dt> ESC 77 G i</dt>
<dd> Jump to line 77 in the file, then return to edit mode. <dd>
<dt> ESC $ i</dt>
<dd> Jump to end of line, then return to edit mode.</dd>
<dt> ESC A</dt>
<dd>Start editing at the end of the current line.</dd>
</dl>


[More great vim commands.](http://www.worldtimzone.com/res/vi.html)