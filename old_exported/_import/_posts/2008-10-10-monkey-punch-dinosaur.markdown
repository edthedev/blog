---
layout: post
title: "Monkey Punch Dinosaur!"
date: 2008-10-10 17:55:08
link: http://www.edthedev.com/?p=10320
categories:
- Random Geekiness
published: false
comments: true
---
I just discovered [Monkey Punch Dinosaur](http://monkeypunchdinosaur.blogspot.com/), and thought it was a delightful concept for a website! It's like a refrigerator that we all share...that is...all of us who wonder who would win in a fight between Godzilla and King Kong.

Anyhow, I sent in my humble contribution:
<a href="http://www.edthedev.com/wp-content/uploads/2008/10/monkey_punch_dino.jpg">{% img http://www.edthedev.com/wp-content/uploads/2008/10/monkey_punch_dino-300x194.jpg %}</a>

I suppose I must have a little bit of pacifist in me; or maybe I just think those two deserved a break for some refreshing punch.