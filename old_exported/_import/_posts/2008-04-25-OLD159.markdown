---
layout: post
title: "Windows Powertoys for Productivity"
date: 2008-04-25 12:04:10
link: http://www.edthedev.com/2008/OLD159/
published: true
comments: true
---
I'm a big fan of Microsoft [Windows Powertoys](http://www.microsoft.com/windowsxp/downloads/powertoys/xppowertoys.mspx). In particular, the Alt-Tab replacer, Open Command Window Here, and Virtual Desktop manager.