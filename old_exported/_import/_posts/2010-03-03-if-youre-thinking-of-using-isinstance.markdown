---
layout: post
title: "If you&#039;re thinking of using isInstance"
date: 2010-03-03 10:35:51
link: http://www.edthedev.com/2010/if-youre-thinking-of-using-isinstance/
categories:
- Example Code
- Programming
published: true
comments: true
---
Think again: http://www.canonical.org/~kragen/isinstance/

I was tempted to use isInstance, when what I really needed was has_attr.