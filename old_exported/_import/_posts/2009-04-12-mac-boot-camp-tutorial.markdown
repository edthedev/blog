---
layout: post
title: "Mac Boot Camp Tutorial"
date: 2009-04-12 06:12:48
link: http://www.edthedev.com/?p=10536
categories:
- Random Geekiness
published: false
comments: true
---
1. How to find boot camp. (And which Macs have it.)
2. You need your XP disk...and key.
3. Activate Windows.
4. In Windows, run Bootcamp for Windows from the Mac OSX install disk that came with your computer. This installs the wireless card driver, the camera driver, and the nVidia graphics card driver, and a bunch of less important stuff.
4. Hold Option while booting to decide which OS to boot to.
5. Be sure to apply all Windows updates - your whole computer is only as secure as it's least secure OS.