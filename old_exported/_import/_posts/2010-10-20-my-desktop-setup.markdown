---
layout: post
title: "My Desktop setup"
date: 2010-10-20 14:51:50
link: http://www.edthedev.com/?p=11149
categories:
- Random Geekiness
published: false
comments: false
---
xmonad (launched from .xsession)
dmenu (adds alt-p application menu)

Other handy stuff:
Put pidgin and thunderbird and xclock into your .xsession

Gettings started:
alt-shift-enter for a new terminal
alt-p for anything else

This will override Ubuntu's session management, but that never worked for me anyhow.