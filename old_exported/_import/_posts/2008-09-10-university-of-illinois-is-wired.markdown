---
layout: post
title: "University of Illinois is Wired"
date: 2008-09-10 13:23:53
link: http://www.edthedev.com/2008/university-of-illinois-is-wired/
categories:
- Random Geekiness
published: true
comments: true
---
My alma-mater, and current employer, the [University of Illinois](http://www.illinois.edu) was named [America's most wired campus](http://tinyurl.com/5657at) by [PC Magazine](http://www.pcmag.com).

Being 'first place' isn't part of the University's mission; but the article does highlight many things that this campus does well, and I'm proud of the colleagues who make those things work.