---
layout: post
title: "Docking Windows in Microsoft Windows"
date: 2009-01-30 06:55:54
link: http://www.edthedev.com/2009/docking-windows-in-microsoft-windows/
categories:
- Example Code
- Solutions
published: true
comments: true
---
Windows 7 finally introduced the ability to dock your separate applications into different portions of the screen. Since most of us haven't adopted Windows 7 yet, [AutoHotKey for Windows](http://www.autohotkey.com/) provides the same functionality now. As a web developer, I rarely work in an [IDE](http://en.wikipedia.org/wiki/Integrated_development_environment), so I am constantly using AutoHotKey to control my windows, instead.

I took a script featured at LifeHacker, and modified it to suit my own needs. Specifically, I bound Windows+Down to resize the active window to be short and wide at the bottom of the screen.

So my 'docks' are:

Windows+Up: Full Screen

Windows+Left: The left half of the screen

Windows+Right: The right half of the screen

Windows+Down: A wide window in the bottom third of the screen.
My script also makes Windows+N open GVim, and makes Windows+G launch a Google search, because it's just that awesome.

And here is my script:<code> </code>[autohotkey ahk](http://www.edthedev.com/wp-content/uploads/2009/01/autohotkeyahk.txt)