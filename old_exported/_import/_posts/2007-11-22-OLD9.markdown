---
layout: post
title: "IT Professionals like Excel"
date: 2007-11-22 09:18:16
link: http://www.edthedev.com/2007/OLD9/
published: true
comments: true
---
I've discovered a new tautology - a new revelation into the deepest mysteries of life:
"IT professionals like Excel."

Let's all meditate on that. I think it's true. Sure, it may be more accurate to state that IT professionals find the grid format used by Excel useful, even if they prefer a different application. But that's a lot harder to say.

And CSV is awsome, because any program that can read an Excel file can read CSV.
I may create a tutorial about creating CSV files*. Stay tuned.

* Yes, this blog is dedicated to teaching things that are obvious after you know them. CSV definitely falls into this category.