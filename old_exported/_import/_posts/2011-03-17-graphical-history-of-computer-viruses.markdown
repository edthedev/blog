---
layout: post
title: "Graphical history of computer viruses"
date: 2011-03-17 19:37:14
link: http://www.edthedev.com/2011/graphical-history-of-computer-viruses/
categories:
- Security
published: true
comments: false
---
F-Secure has created a graphic with descriptions of [history's most memorable computer viruses](http://mashable.com/2011/03/16/history-of-computer-viruses/). I was pleased to see that Sony's ill planned Digital Right Management root-kit is included in the list.