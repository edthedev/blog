---
layout: post
title: "Securing Our Minds"
date: 2008-12-23 12:25:48
link: http://www.edthedev.com/?p=10389
categories:
- Random Geekiness
published: false
comments: true
---
For the past six months I have written a column the University's Network Security weekly email. The section is called the "Secure Development Tip of the Week", and I'm loving writing it. We have so many brilliant people on staff; it is very satisfying to be one of the resources that helps them stay at the cutting edge.