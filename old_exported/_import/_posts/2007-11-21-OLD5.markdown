---
layout: post
title: "Modify every line of a file using PowerShell"
date: 2007-11-21 17:16:10
link: http://www.edthedev.com/2007/OLD5/
published: true
comments: true
---
get-content text1.txt | % { $_ -replace "\|", "" } | % { $_ -replace " ", "" } > text2.txt