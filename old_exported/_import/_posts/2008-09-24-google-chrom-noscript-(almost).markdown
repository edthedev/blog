---
layout: post
title: "Google Chrom NoScript (Almost)"
date: 2008-09-24 11:18:27
link: http://www.edthedev.com/?p=10315
categories:
- Random Geekiness
published: false
comments: true
---
The ability to disable all scripts by default is minimum for a functional web browser. The ability to conveniently re-enable scripts for trusted websites will be the de-facto standard in the future.

Security is about trust. People don't mind seeing advertisements, if they're from advertisers they trust. But no website should be allowed to run code on my computer until I proclaim that it has earned my trust.

Anyhow, without further ado: You can disable Google Chrome from the command line using the -disable-javascript option. I recommend the -disable-java option as well.

I will be trying it with these two options until the Chrome Plugins have a NoScript plugin available.

- Edward