---
layout: post
title: "diff a directory with Python"
date: 2009-11-18 20:02:21
link: http://www.edthedev.com/2009/diff-a-directory-with-python/
categories:
- Example Code
- Programming
- Solutions
published: true
comments: true
---
I needed to diff the entire contents of two code directories today (a lot of old code files and new code files before and after for a security audit). There is probably a simple *Nix pipe-style command to do that; but I have more programmer in my blood than *Nix admin. 

Without further ado, here's the script:
<code>

{% blockquote %}

import subprocess
import os
import traceback

report_file = "diff_report.txt"
home = "/home/edward/audit"
old = "prod"
new = "testing"
file_type = ".pm"

# diff = "/usr/bin/tkdiff"
diff = "/usr/bin/diff"

# Report header...
report = open(report_file, 'w')
report.write('Diffs and files that are new:\n') 
report.close() 

# Processing...
report = open(report_file, 'a')
new_files = os.listdir('./%s' % new)

for file_name in new_files:
  old_file = "%s/%s/%s" % (home, old, file_name)
  new_file = "%s/%s/%s" % (home, new, file_name)
  args = "%s %s" % (new_file, old_file)
  command = "%s %s" % (diff, args)
  command_input = [diff, new_file, old_file]
  #  call(command_input)
  if file_type in file_name:
  if os.path.exists(old_file):
  print "Running command: %s" % command
  try:
  diff_job = subprocess.Popen(command_input, stdout=subprocess.PIPE)
  report.write("--- Diff of %s:\n" % file_name)
  report.write(diff_job.communicate()[0])
  except Exception, e:
  print "Command failed: %s" % command
  raise e
  print traceback.print_stack()
  else:
  report.write("--- New file %s:\n" % file_name)
  new_handle = open(new_file, 'r')
  report.write(new_handle.read())
  new_handle.close()

report.close()
{% endblockquote %}
</code>