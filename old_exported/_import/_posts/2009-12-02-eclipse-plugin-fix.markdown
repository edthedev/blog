---
layout: post
title: "Eclipse Plugin Fix"
date: 2009-12-02 17:22:20
link: http://www.edthedev.com/2009/eclipse-plugin-fix/
categories:
- Solutions
published: true
comments: true
---
The problem: Your fresh install of Eclipse won't succeed in installing any of the plugins that make it wonderful; so Eclipse is useless to you. The specific error you're seeing is:

The artifact file for osgi.bundle,
org.eclipse.ant.ui,3.4.1.v20090901_r351 was not found.

Rather than analyse, research, and work to fix the problem, you could just trust me that the following Debian-Fu will solve it:

sudo aptitude install eclipse-jdt eclipse-pde

Don't thank me, thank the nice folks who commented on the bug over at:

https://bugs.launchpad.net/ubuntu/+source/eclipse/+bug/477944