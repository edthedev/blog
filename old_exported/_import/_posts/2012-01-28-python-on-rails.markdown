---
layout: post
title: "Python on Rails"
date: 2012-01-28 15:44:01
link: http://www.edthedev.com/2012/python-on-rails/
categories:
- Random Geekiness
published: true
comments: false
---
Ever since I saw that first [Ruby on Rails tutorial][1], I've been looking for a way to replicate the experience with [Python][2].

My personal opinion is that Python has a lot more "power under the hood" than Ruby - a more active developer community, better standard libraries, and more maturity in the third-party libraries that really count toward the bottom-line.

But what it hasn't had until now was a really fast way to prototype applications. Some people might say [Django][3] fill this need, but I disagree. Django is built for marathons, and makes some sacrifices to meet that goal.

I've finally found a good 'sprint' framework for Python in [Bottle.py][4]. Like anyone doing a first project, I decided to [create a dice roller][5].

[1]: http://www.youtube.com/watch?v=Gzj723LkRJY "Build a Rails App in 15 Minutes"
[2]: http://python.org "Python Programming Language"
[3]: https://www.djangoproject.com/ "Django Web Framework for Python"
[4]: http://bottlepy.org/docs/dev/tutorial_app.html
"Bottle.py Tutorial"
[5]: http://minecraft.edthedev.com/scripts "Bottle.py Dice Roller"