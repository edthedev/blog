---
layout: post
title: "Android on iPhone"
date: 2010-04-25 20:13:00
link: http://www.edthedev.com/2010/android-on-iphone/
categories:
- Solutions
published: true
comments: true
---
It's now possible to dual-boot your iPhone with Andoid. 
http://www.taranfx.com/android-on-iphone

I'm very excited about this because I like the Google Android developer kit, and I find that working with Apple's XCode is like having my pinky knuckle vigorously rubbed against a piece of sandpaper*. 

* I get creative with analogies when development environments annoy me.