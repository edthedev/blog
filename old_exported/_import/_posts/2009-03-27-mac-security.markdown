---
layout: post
title: "Mac Security"
date: 2009-03-27 16:15:55
link: http://www.edthedev.com/2009/mac-security/
categories:
- Security
published: true
comments: true
---
Apple Insider has a nice [article about Mac security](http://www.appleinsider.com/articles/09/03/26/pwn2own_contest_winner_macs_are_safer_than_windows.html). AppleInsider interviewed hackers and security experts about why Mac's are considered safer than Windows. The conclusion is that Macs are much easier to compromise than PCs, but just aren't worth the trouble due to their lack of market position.

My favorite article highlights follow. Disclaimer: I run both Windows and Linux, and look forward to purchasing a Mac soon. I respect the security teams behind all of these products. There are good reasons why the products work they ways they do. Until the day when all are perfectly secure, anything you can learn about their security models helps.

At Pwn2Own, the $5,000 prize for hacking the Mac was collected within seconds of the contest starting. The winner, who also won last year, hinted that he had a large array of working hacks ready, and the first hack just happened to work on the first try.

The $10,000 prize for hacking the iPhone was not collected; but the rumor mill says that someone had a working hack, but was unwilling to share it for only $10,000.