---
layout: post
title: "Choosing a Windows Diff Utility"
date: 2008-01-24 08:50:15
link: http://www.edthedev.com/2008/OLD103/
published: true
comments: true
---
'Diff' is a Linux tool for comparing text files. Windows Diff programs typically display the selected files side by side, with the differences highlighted. In it's simplest form Linux Diff just displays the list of differences.

Any Diff program is a great way to figure out which of two similar files is better, or to pick out the best parts of each file. I love Diff; but as a Windows user choosing the right Diff program can be tricky.

There are many nice free Diff tools for Windows; but I have yet to find an authoritative guide to help me choose one. So I'm creating my contribution to that guide here. Please feel free to add your suggestions as comments and I will update this list accordingly.
Also, feel free to use what I have here as part of a larger guide - just mention me in the credits with my name and a link.


Windows Native Diff
-------------------
Windows does indeed have a built in tool. It is called 'comp'. As a Linux fan-boy, I can only describe it's quality as 'tragic'. Try it on the command line, or just enjoy the simulated experience below:
<code>

comp File12.pl File1010.pl
Comparing File12.pl and File1010.pl...
Files are different sizes.
Compare more files (Y/N) ? n

</code>
Indeed. The files are, in fact, _different sizes_. And will I compare more files? No, the above experience is all I care to have with this tool.


DiffMerge
---------
I'm trying out [DiffMerge][1].
[1]: http://www.sourcegear.com/diffmerge/downloads.html

-  I'm disappointed that I can't just drag-and-drop a couple of files into the window, the way I prefer to work with my favorite editor, [Crimson][2].
-  [DiffMerge][1] isn't a champion at handling large files. I asked it to compare two alphabetized files full of hostnames with about 6,000 lines each. I gave up and closed it after ten minutes. I don't necessarily disagree with DiffMerge's choice to focus on good features for small jobs; but it's worth mentioning here. You'll need a different tool for large files.

[2]: http://www.crimsoneditor.com/

[Diffmerge][1] does feel _shiny_, though. It gets several things very right:
-  As the name implies, it's not just a 'diff' tool, it's a 'merge' tool. Right click a diff block and enjoy the rich context sensitive options.
-  DiffMerge can actually compare three files at once. To most people, this is superfluous, but to some it might be important.
-  The line numbers are displayed by default - I hate having to go figure out where to turn them on. 
-  The default colors are gentle and attractive. 
-  And even when comparing three separate files, the highlighting works - I can see the all important 'diff' between the files.


Tortoise SVN
------------
If you're writing code, you should be using [Subversion][10], and if you're using [Subversion][10] on Windows, you should be using [Tortoise SVN][20].
And if you are using [Tortoise SVN][20], you have a very nice pair of Diff and Merge tools already integrated into [Tortoise SVN][20].

[10]: http://subversion.tigris.org/
[20]: http://tortoisesvn.tigris.org/

-  The diff tool is available through the Windows right-click menu from any file that is registered in your [SVN][10] repository through [Tortoise SVN][20]. Since you should be using both of these tools anyways, this is very handy, and it tends to be my default tool for a quick diff.
-  The merge tool has three panels (old, new, and merged versions), and each block of text can be right clicked to select obvious options like 'use this in the merged version'.
-  Add to this that it takes its cues from the [SVN][10] state of the file, and it's a very nice workflow (if you were using [Tortoise SVN][20] anyways).


CSDiff
------
[CSDiff][3] is an entirely acceptable diff tool. But having it installed next to [Diffmerge][1], I just don't use it anymore.

-  I tried [CSDiff][3] against the two large files that [Diffmerge][1] was unable to process. [CSDiff][3] finished instantly, but gave up and decided that there were no similarities between the files at all. I may have to just write a shell script to do the comparison for me.

[3]: http://www.componentsoftware.com/products/csdiff/download.htm