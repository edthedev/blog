---
layout: post
title: "Choice Impairs programmers too"
date: 2010-05-15 00:01:50
link: http://www.edthedev.com/2010/choice-impairs-programmers/
categories:
- Random Geekiness
published: true
comments: true
---
"there's an entire school of thought based around the idea that simple, focused programs are preferable to monstrosities that do it all." - Sammy Larbi from [My Secret Life as a Spaghetti Coder
](
http://www.codeodor.com/index.cfm/2010/5/10/How-Choice-Impairs-Programmers/3191)