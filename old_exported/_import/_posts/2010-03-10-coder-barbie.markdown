---
layout: post
title: "Coder Barbie"
date: 2010-03-10 09:20:28
link: http://www.edthedev.com/?p=10715
categories:
- Random Geekiness
published: false
comments: true
---
As a programmer who has worked with many female programmers, I can confirm one of the points this article makes.

http://mashable.com/2010/03/09/computer-engineer-barbie/

Programmers with style do exist; and it would be inappropriate for Barbie to lose her sense of fashion just because she became a programmer.