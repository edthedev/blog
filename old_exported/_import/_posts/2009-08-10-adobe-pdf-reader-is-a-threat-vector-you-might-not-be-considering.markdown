---
layout: post
title: "Adobe PDF Reader is a threat vector you might not be considering"
date: 2009-08-10 10:32:48
link: http://www.edthedev.com/?p=10561
categories:
- Random Geekiness
published: false
comments: true
---
This is old news in the Security world, but might be interesting to some: Adobe PDF Reader has ousted Microsoft Word as the application most targeted by malicious downloads:
http://news.cnet.com/8301-27080_3-10304455-245.html