---
layout: post
title: "Display All Environment Variables in Python"
date: 2008-04-04 13:35:05
link: http://www.edthedev.com/2008/OLD143/
published: true
comments: true
---
This is a handy Python CGI script that displays all available environment variables.
It's nice for having a look around; but don't leave this lying around on your sever.



#!/usr/bin/python

import os

print "Content-Type: text/plain\n\n"
for key in os.environ.keys():
  print "%30s %s \n" % (key,os.environ[key])

