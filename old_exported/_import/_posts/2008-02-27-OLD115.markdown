---
layout: post
title: "Set Your Screen Saver to your World of Warcraft Screenshots"
date: 2008-02-27 04:34:12
link: http://www.edthedev.com/2008/OLD115/
published: true
comments: true
---
Pressing the 'Print Screen' key while playing World of Warcraft saves a picture of whatever is on screen. This is a great way to memorialize great moments of game-play. 

Of course, if you're like me, you're never really going to see these pictures again. So set your Windows screen-saver to display them using the following instructions.

1. Right-click on a blank space on your Desktop
2. Select 'Properties' from the little context menu that appears.
You will see a window like this one:
![Display Properties](http://edthedev.com/files/DisplayProperties.png)
3. Select the 'Screen Saver' tab at the top. The window will change to look more like this one:
![Screen Saver Tab](http://edthedev.com/files/DisplayProperties_ScrSvr.png)
4. In the drop-down box under 'Screen saver', select 'My Pictures Slideshow'.
5. Press the 'Settings' button just to the right of that drop-down. A window will pop up that looks like this:
![My Pictures Screen Saver Options Window](http://edthedev.com/files/MyPicturesScreenSaverOptions.png)

4. In the 'My Pictures Screen Saver Options' window, 
Set 'User Pictures in this folder' to C:\Program Files\World of Warcraft\Screenshots

5. If you want to be able to read your conversations, be sure to set the following:
* 'How big should pictures be?' to Largest
* Check the box next to 'Stretch small pictures'
* Check the box next to 'Allow scrolling through pictures with the keyboard'

Enjoy!
- Edward