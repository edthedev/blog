---
layout: post
title: "Live long and code well"
date: 2010-08-10 07:53:03
link: http://www.edthedev.com/2010/live-long-and-code-well/
categories:
- Programming
- Solutions
published: true
comments: false
---
Zed Shaw gives advice on [staying healthy as a programmer](http://sheddingbikes.com/posts/1281257293.html).