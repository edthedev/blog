---
layout: post
title: "Need to save a picture of what is on your screen?"
date: 2008-08-15 08:00:10
link: http://www.edthedev.com/2008/old210/
categories:
- Solutions
published: true
comments: true
---
Most of my readers (myself) already know this; but I'm linking it anyhow because it's a great set of instructions, and really good instructions are incredibly rare. [How to take a screenshot](http://take-a-screenshot.org/).

Follow the link. Admire the instructions.

- Edward