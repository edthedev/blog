---
layout: post
title: "Get a password through Python command line"
date: 2008-04-23 13:48:25
link: http://www.edthedev.com/2008/OLD156/
published: true
comments: true
---
Here's the code:


<code>
import getpass
password = getpass.getpass()
</code>


Doing it this way will hide the password as the user types it.