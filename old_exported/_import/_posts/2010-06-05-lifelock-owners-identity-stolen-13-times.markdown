---
layout: post
title: "LifeLock owner&#039;s identity stolen 13 times"
date: 2010-06-05 17:00:40
link: http://www.edthedev.com/2010/lifelock-owners-identity-stolen-13-times/
categories:
- Security
published: true
comments: true
---
LifeLock is an interesting service, but it [actually doesn't protect that well](http://www.cringely.com/2010/05/lifeblocked/). I've seen a lot of articles on the subject, but the one I just linked has the best over-all summary of the trade-offs involved with trying to offer the kind of protection that LifeLock sells.