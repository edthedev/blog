---
layout: post
title: "Recent Commits February 18, 2012"
date: 2012-02-18 17:38:09
link: http://www.edthedev.com/2012/recent-commits-february-18-2012/
categories:
- Example Code
published: true
comments: false
---
<ul class="scrd_digest">
- [Fixed nook scripts.](https://bitbucket.org/edthedev/edthedev/changeset/560968bb2b63)
<div>Fixed nook scripts.
  
  
  
  
  
  
  - [
  config/cleanup-all-the-downloads
  ](https://bitbucket.org/edthedev/edthedev/src/560968bb2b63/config/cleanup-all-the-downloads)(2 lines added, 0 lines removed)
  
  
  - [
  config/nook-build-books
  ](https://bitbucket.org/edthedev/edthedev/src/560968bb2b63/config/nook-build-books)(5 lines added, 8 lines removed)
  
  
  - [
  config/nook-resize-images
  ](https://bitbucket.org/edthedev/edthedev/src/560968bb2b63/config/nook-resize-images)(1 lines added, 0 lines removed)
  
  
  - [
  config/nook-sync
  ](https://bitbucket.org/edthedev/edthedev/src/560968bb2b63/config/nook-sync)(4 lines added, 21 lines removed)
  
  
  - [
  config/to-nook
  ](https://bitbucket.org/edthedev/edthedev/src/560968bb2b63/config/to-nook)(8 lines added, 0 lines removed)
  
  </div>

- [More nook helpers](https://bitbucket.org/edthedev/edthedev/changeset/750453bb8c56)
<div>More nook helpers
  
  
  
  
  
  
  - [
  config/nook-build-books
  ](https://bitbucket.org/edthedev/edthedev/src/750453bb8c56/config/nook-build-books)(13 lines added, 0 lines removed)
  
  
  - [
  config/nook-resize-images
  ](https://bitbucket.org/edthedev/edthedev/src/750453bb8c56/config/nook-resize-images)(8 lines added, 0 lines removed)
  
  
  - [
  config/nook-sync
  ](https://bitbucket.org/edthedev/edthedev/src/750453bb8c56/config/nook-sync)(22 lines added, 0 lines removed)
  
  
  - [
  config/sync-nook
  ](https://bitbucket.org/edthedev/edthedev/src/750453bb8c56/config/sync-nook)(0 lines added, 4 lines removed)
  
  
  - [
  vimrc_edthedev
  ](https://bitbucket.org/edthedev/edthedev/src/750453bb8c56/vimrc_edthedev)(1 lines added, 0 lines removed)
  
  </div>

- [Perms](https://bitbucket.org/edthedev/edthedev/changeset/346887be102d)
<div>Perms
  
  
  
  
  
  
  - [
  config/sync-nook
  ](https://bitbucket.org/edthedev/edthedev/src/346887be102d/config/sync-nook)(0 lines added, 0 lines removed)
  
  </div>

- [Moved](https://bitbucket.org/edthedev/edthedev/changeset/76d0d3b0ff3c)
<div>Moved
  
  
  
  
  
  
  - [
  config/sync-nook
  ](https://bitbucket.org/edthedev/edthedev/src/76d0d3b0ff3c/config/sync-nook)(4 lines added, 0 lines removed)
  
  
  - [
  nook/sync-nook
  ](https://bitbucket.org/edthedev/edthedev/src/76d0d3b0ff3c/nook/sync-nook)(0 lines added, 4 lines removed)
  
  </div>

- [Added rsync script to sync nook](https://bitbucket.org/edthedev/edthedev/changeset/6aeb53f82ca7)
<div>Added rsync script to sync nook
  
  
  
  
  
  
  - [
  nook/sync-nook
  ](https://bitbucket.org/edthedev/edthedev/src/6aeb53f82ca7/nook/sync-nook)(4 lines added, 0 lines removed)
  
  </div>

- [automerge](https://bitbucket.org/edthedev/edthedev/changeset/395443f588bf)
<div>automerge
  
  
  
  
  
  
  - [
  bottle/605px-Octahedron_svg.png
  ](https://bitbucket.org/edthedev/edthedev/src/395443f588bf/bottle/605px-Octahedron_svg.png)(0 lines added, 0 lines removed)
  
  
  - [
  bottle/bottle
  ](https://bitbucket.org/edthedev/edthedev/src/395443f588bf/bottle/bottle)(1 lines added, 0 lines removed)
  
  
  - [
  bottle/bottle.wsgi
  ](https://bitbucket.org/edthedev/edthedev/src/395443f588bf/bottle/bottle.wsgi)(11 lines added, 0 lines removed)
  
  
  - [
  bottle/cardgame.py
  ](https://bitbucket.org/edthedev/edthedev/src/395443f588bf/bottle/cardgame.py)(122 lines added, 0 lines removed)
  
  
  - [
  bottle/d10.png
  ](https://bitbucket.org/edthedev/edthedev/src/395443f588bf/bottle/d10.png)(0 lines added, 0 lines removed)
  
  
  - [
  bottle/d10.svg
  ](https://bitbucket.org/edthedev/edthedev/src/395443f588bf/bottle/d10.svg)(202 lines added, 0 lines removed)
  
  
  - [
  bottle/d12.png
  ](https://bitbucket.org/edthedev/edthedev/src/395443f588bf/bottle/d12.png)(0 lines added, 0 lines removed)
  
  
  - [
  bottle/d12.svg
  ](https://bitbucket.org/edthedev/edthedev/src/395443f588bf/bottle/d12.svg)(899 lines added, 0 lines removed)
  
  
  - [
  bottle/d2.png
  ](https://bitbucket.org/edthedev/edthedev/src/395443f588bf/bottle/d2.png)(0 lines added, 0 lines removed)
  
  
  - [
  bottle/d2.svg
  ](https://bitbucket.org/edthedev/edthedev/src/395443f588bf/bottle/d2.svg)(177 lines added, 0 lines removed)
  
  
  - [
  bottle/d20.png
  ](https://bitbucket.org/edthedev/edthedev/src/395443f588bf/bottle/d20.png)(0 lines added, 0 lines removed)
  
  
  - [
  bottle/d20.svg
  ](https://bitbucket.org/edthedev/edthedev/src/395443f588bf/bottle/d20.svg)(90 lines added, 0 lines removed)
  
  
  - [
  bottle/d4.png
  ](https://bitbucket.org/edthedev/edthedev/src/395443f588bf/bottle/d4.png)(0 lines added, 0 lines removed)
  
  
  - [
  bottle/d4.svg
  ](https://bitbucket.org/edthedev/edthedev/src/395443f588bf/bottle/d4.svg)(138 lines added, 0 lines removed)
  
  
  - [
  bottle/d6.png
  ](https://bitbucket.org/edthedev/edthedev/src/395443f588bf/bottle/d6.png)(0 lines added, 0 lines removed)
  
  
  - [
  bottle/d6.svg
  ](https://bitbucket.org/edthedev/edthedev/src/395443f588bf/bottle/d6.svg)(115 lines added, 0 lines removed)
  
  
  - [
  bottle/d8.png
  ](https://bitbucket.org/edthedev/edthedev/src/395443f588bf/bottle/d8.png)(0 lines added, 0 lines removed)
  
  
  - [
  bottle/d8.svg
  ](https://bitbucket.org/edthedev/edthedev/src/395443f588bf/bottle/d8.svg)(123 lines added, 0 lines removed)
  
  
  - [
  bottle/index.py
  ](https://bitbucket.org/edthedev/edthedev/src/395443f588bf/bottle/index.py)(122 lines added, 0 lines removed)
  
  
  - [
  bottle/jquery.idle-timer.js
  ](https://bitbucket.org/edthedev/edthedev/src/395443f588bf/bottle/jquery.idle-timer.js)(246 lines added, 0 lines removed)
  
  
  - [
  bottle/roller.css
  ](https://bitbucket.org/edthedev/edthedev/src/395443f588bf/bottle/roller.css)(89 lines added, 0 lines removed)
  
  
  - [
  bottle/roller.html
  ](https://bitbucket.org/edthedev/edthedev/src/395443f588bf/bottle/roller.html)(33 lines added, 0 lines removed)
  
  
  - [
  bottle/roller.py
  ](https://bitbucket.org/edthedev/edthedev/src/395443f588bf/bottle/roller.py)(299 lines added, 0 lines removed)
  
  
  - [
  bottle/setupbottle
  ](https://bitbucket.org/edthedev/edthedev/src/395443f588bf/bottle/setupbottle)(8 lines added, 0 lines removed)
  
  
  - [
  bottle/starcraftii
  ](https://bitbucket.org/edthedev/edthedev/src/395443f588bf/bottle/starcraftii)(1 lines added, 0 lines removed)
  
  
  - [
  bottle/targetgame
  ](https://bitbucket.org/edthedev/edthedev/src/395443f588bf/bottle/targetgame)(77 lines added, 0 lines removed)
  
  
  - [
  config/apache_public_html
  ](https://bitbucket.org/edthedev/edthedev/src/395443f588bf/config/apache_public_html)(5 lines added, 0 lines removed)
  
  
  - [
  config/install-dokuwiki
  ](https://bitbucket.org/edthedev/edthedev/src/395443f588bf/config/install-dokuwiki)(27 lines added, 0 lines removed)
  
  
  - [
  games/cardgame.py
  ](https://bitbucket.org/edthedev/edthedev/src/395443f588bf/games/cardgame.py)(122 lines added, 0 lines removed)
  
  
  - [
  kindle/books.txt
  ](https://bitbucket.org/edthedev/edthedev/src/395443f588bf/kindle/books.txt)(1 lines added, 0 lines removed)
  
  
  - [
  kindle/make-books.ini.example
  ](https://bitbucket.org/edthedev/edthedev/src/395443f588bf/kindle/make-books.ini.example)(8 lines added, 0 lines removed)
  
  
  - [
  kindle/make-books.py
  ](https://bitbucket.org/edthedev/edthedev/src/395443f588bf/kindle/make-books.py)(84 lines added, 0 lines removed)
  
  
  - [
  minecraft/backup_worlds
  ](https://bitbucket.org/edthedev/edthedev/src/395443f588bf/minecraft/backup_worlds)(0 lines added, 0 lines removed)
  
  
  - [
  org/aliases
  ](https://bitbucket.org/edthedev/edthedev/src/395443f588bf/org/aliases)(15 lines added, 27 lines removed)
  
  
  - [
  org/logit
  ](https://bitbucket.org/edthedev/edthedev/src/395443f588bf/org/logit)(8 lines added, 12 lines removed)
  
  
  - [
  vimrc_edthedev
  ](https://bitbucket.org/edthedev/edthedev/src/395443f588bf/vimrc_edthedev)(9 lines added, 1 lines removed)
  
  </div>

- [Better name](https://bitbucket.org/edthedev/edthedev/changeset/78be65c8cb9a)
<div>Better name
  
  
  
  
  
  
  - [
  config/random-wallpaper
  ](https://bitbucket.org/edthedev/edthedev/src/78be65c8cb9a/config/random-wallpaper)(21 lines added, 0 lines removed)
  
  
  - [
  config/random_wallpaper
  ](https://bitbucket.org/edthedev/edthedev/src/78be65c8cb9a/config/random_wallpaper)(0 lines added, 21 lines removed)
  
  </div>

- [A script for converting a text graph file into a GraphViz graph image.](https://bitbucket.org/edthedev/edthedev/changeset/00a6d6275edc)
<div>A script for converting a text graph file into a GraphViz graph image.
  
  
  
  
  
  
  - [
  config/twoScreens
  ](https://bitbucket.org/edthedev/edthedev/src/00a6d6275edc/config/twoScreens)(2 lines added, 1 lines removed)
  
  
  - [
  edthedev/text.py
  ](https://bitbucket.org/edthedev/edthedev/src/00a6d6275edc/edthedev/text.py)(2 lines added, 2 lines removed)
  
  
  - [
  text/list-headers
  ](https://bitbucket.org/edthedev/edthedev/src/00a6d6275edc/text/list-headers)(2 lines added, 1 lines removed)
  
  
  - [
  text/make-graph
  ](https://bitbucket.org/edthedev/edthedev/src/00a6d6275edc/text/make-graph)(8 lines added, 0 lines removed)
  
  
  - [
  vimrc_edthedev
  ](https://bitbucket.org/edthedev/edthedev/src/00a6d6275edc/vimrc_edthedev)(10 lines added, 4 lines removed)
  
  </div>


Digest powered by [RSS Digest](http://www.rssdigestpro.com)
