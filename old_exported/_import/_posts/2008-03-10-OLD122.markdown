---
layout: post
title: "Python DocStrings"
date: 2008-03-10 14:18:33
link: http://www.edthedev.com/2008/OLD122/
published: true
comments: true
---
Python modules have a great internal documentation system called [DocStrings][1]. You can use these to explore installed modules through the interactive Python interpreter.
[1]: http://epydoc.sourceforge.net/docstrings.html

Launch the interactive Python interpreter by typing 'Python' on the command line. Then enter the following commands (replacing ModuleName with the name of the Module you wish to explore).
<code>
>>>import ModuleName
>>>help(ModuleName)
</code>

Use the spacebar to see the next page of the help provided. Use the 'Q' key to exit the help pages. Often you will encounter additional interesting terms, which can be accessed through the command:
<code>
>>>help(ModuleName.Term)
</code>

Enjoy!
- Edward