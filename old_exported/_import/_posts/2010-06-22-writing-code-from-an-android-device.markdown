---
layout: post
title: "Writing code from an Android device"
date: 2010-06-22 19:19:16
link: http://www.edthedev.com/2010/writing-code-from-an-android-device/
categories:
- Random Geekiness
published: true
comments: true
---
James Graves has done what I've been dreaming of - he has configured an [Android phone as a development environment](http://partiallyappliedlife.blogspot.com/2010/06/writing-code-on-handhelds.html). James, you are my hero for the week!