---
layout: post
title: "Background noise for programming"
date: 2010-05-15 00:01:33
link: http://www.edthedev.com/2010/background-noise-for-programming/
categories:
- Programming
- Solutions
published: true
comments: true
---
It's a lot easier to write software in a noise-free environment, or predictable consistent background noise. A good set of headphones is a must, and an iPod application or two can be really handy.

[White Noise](http://www.tmsoft.com/iphone-whitenoise.html) by TMSoft is currently my favorite. Their boat rocking, railroad tracks bumps and various types of rain are great for relaxing just the right amount.

I also highly recommend the 'Pirates of the Caribbean' movie sound track, and just about anything composed by John Williams.

Update: My friend Rob pointed me to the incredibly awesome [Trance Around the World Podcast](http://www.trancearoundtheworld.com/). Highly recommended.