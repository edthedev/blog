---
layout: post
title: "PowerShell script to list the free space on your drives"
date: 2008-03-29 04:56:34
link: http://www.edthedev.com/2008/OLD134/
published: true
comments: true
---
Here is the command:

<code>Get-WMIObject Win32_LogicalDisk | ForEach-Object {[math]::truncate($_.freespace / 1GB)}</code>
  
But you could shorten it with some abbreviations:

<code>gwmi Win32_LogicalDisk | % {[math]::truncate($_.freespace / 1GB)}</code>

And if you want precision rather than a clean answer:

<code>gwmi Win32_LogicalDisk | % {$_.freespace / 1GB}</code>

The commands above will list the free space on each of your computers logical disks, so expect to see a list. Naturally, CD and DVD drives will be listed as having 0 free space.