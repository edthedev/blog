---
layout: post
title: "Deployment first development"
date: 2010-08-18 09:52:13
link: http://www.edthedev.com/2010/deployment-first-development/
categories:
- Programming
- Solutions
published: true
comments: false
---
This is [a very cool article that encourages us to face our worst fears](http://jaybill.com/2010/08/17/deployment-first-development/): packaging and deploying