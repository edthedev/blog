---
layout: post
title: ""
date: 2009-02-16 13:28:57
link: http://www.edthedev.com/2009/10428/
categories:
- Programming
published: true
comments: true
---
Search functions typically bring back more results than your user really wants to see.

Luckily, [Django does Pagination](http://docs.djangoproject.com/en/dev/topics/pagination/). I cannot express how awesome it is to have a typical feature like that done once, done right. Batteries are, indeed, included.