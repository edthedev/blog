---
layout: post
title: "Such an Incredilbly Bad Idea"
date: 2008-07-16 12:10:24
link: http://www.edthedev.com/2008/old200/
categories:
- Security
published: true
comments: true
---
I love providing you with links to great code examples; but it's time to shake things up.

I have found, for you, a link to the worst piece code example I have seen in some time. The code itself is great, but what it actually does is so bad that I can't get into describing it here.

Without further delay, check out [letting complete strangers run shell commands on your server](http://www.sitepoint.com/article/take-command-ajax/2).

In fairness to the author, he does put a disclaimer that basically says [you should never ever use this code](http://www.sitepoint.com/article/take-command-ajax/3). But it's too good of an opportunity for me to pass up: a chance to remind everyone that code can be incredibly dangerous.