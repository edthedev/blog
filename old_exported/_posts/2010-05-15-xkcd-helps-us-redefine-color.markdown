---
layout: post
title: "XKCD helps us redefine color"
date: 2010-05-15 00:08:23
link: http://www.edthedev.com/2010/xkcd-helps-us-redefine-color/
categories:
- Random Geekiness
published: true
comments: true
---
The brilliant mind behind XKCD.com ran a truly [fascinating survey about how people name colors](http://blog.xkcd.com/2010/05/03/color-survey-results/). The results are surprising and in some cases hilarious.