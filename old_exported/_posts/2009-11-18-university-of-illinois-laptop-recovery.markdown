---
layout: post
title: "University of Illinois Laptop Recovery"
date: 2009-11-18 20:23:28
link: http://www.edthedev.com/2009/university-of-illinois-laptop-recovery/
categories:
- Security
- Programming
published: true
comments: true
---
The University of Illinois Network Security team offers a computer registration service for campus networked devices. Registering a device used on campus increases the chances that Campus Police (with help from Network Security) can recover the device if it is stolen.
I'm a big fan of this service because I wrote it for them.

[http://www.cites.illinois.edu/security/register/](http://www.cites.illinois.edu/security/register/)