---
layout: post
title: "Speed up Windows"
date: 2008-06-26 07:39:27
link: http://www.edthedev.com/2008/OLD190/
published: true
comments: true
---
[Web Worker Daily](http://webworkerdaily.com/) has a nice article about [how to speed up your old Windows computer](http://webworkerdaily.com/2008/06/25/7-ways-to-add-oomph-to-an-aging-windows-system/). 

It amuses me greatly that the comments on the article all recommend [installing Ubuntu](https://help.ubuntu.com/community/Installation#head-59d90d26191eb3f0e6fd17b48605bbedfb514f86) instead of Windows.

Enjoy!

- Edward