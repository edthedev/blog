---
layout: post
title: "bytes to string and back"
date: 2010-04-23 13:36:17
link: http://www.edthedev.com/?p=10902
categories:
- Random Geekiness
published: false
comments: true
---
## Java


<code>
byte[] myBytes = inputString.getBytes("UTF8");
String myString = new String(myBytes, "UTF8");
</code>

## And in C#


<code>
System.Text.Encoding encoder = new System.Text.Encoding.UTF8;
byte[] myBytes = System.Text.Encoding.UTF8.GetBytes(inputString);
string myString = System.Text.Encoding.UTF8.GetString(myBytes);
</code>
