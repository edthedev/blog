---
layout: post
title: "Take your thumb drive to the next level of productivity"
date: 2008-12-05 17:34:50
link: http://www.edthedev.com/2008/pstart-and-some-handy-batch-files/
categories:
- Example Code
- Solutions
published: true
comments: true
---
I hope you're enjoying the thumb drive environment that I showed you how to create. Now I will show you how to make it even better.

I found a better thumb drive application launcher called [PStart](http://www.pegtop.net/start/). I greatly prefer it over PortableAppsLauncher. Both have weaknesses - neither will launch a web link or a Windows Folder from the menu. But PStart _will_ launch a batch file; and that's enough for me to work around these two problems.

First, you'll want to use a [TiddlyWiki](http://www.tiddlywiki.com/) to manage your bookmarks. It's free. You can launch it from PStart.  You can drag new bookmarks into entries in it; you can organize how your favorites are organized, tagged and presented; and it's fully searchable. Plus, you can choose from a bunch of cool looking [TiddlyWiki Themes](http://tiddlythemes.com/).

Next, you'll want [EjectUSB](http://www.pocketappreview.com/main/item/17) - you can launch it from PStart, and it's far fewer clicks than the standard windows method for safely ejecting your USB drive. Like everything I recommend here, it works on Windows and it's free.

I also created some batch files to support PStart.

Since I pretty much always need a command prompt and a GUI interface to any folder I'm working with, I created prompt_and_folder.bat. It opens both a CommandPromptPortable prompt and a Windows Explorer window at the same folder. 
Save this code into a file named 'prompt_and_folder.bat' in your 'bin' directory:
<code>

explorer.exe %1
start \PortableApps\CommandPromptPortable\CommandPromptPortable.exe '%1'
REM start powershell.exe -NoExit -Command "cd '%1'"

</code>
Remove the 'REM' to also start a PowerShell prompt on any computer that has PowerShell available from the path.

If you're using the Thumb Drive setup that I recommended; put these batch files in you /bin directory; and they will be available from within PortableCommandPrompt in case you want to use them there as well.

When you add these to your PStart menu, put the path you want to launch into the 'Arguments' text box.

I created web.bat to hack PStart into letting me add web URLs to my start menu. Just put the URL desired into the 'Arguments' text box, and be sure to give each link a meaningful name so that they don't all list as 'web.bat' in the menu.
Save this code into a file named 'web.bat' in your 'bin' directory:
<code>

start \PortableApps\FirefoxPortable\FirefoxPortable.exe %1

</code>

Enjoy!
- Edward