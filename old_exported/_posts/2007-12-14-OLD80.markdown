---
layout: post
title: "New MMO from Blizzard"
date: 2007-12-14 09:05:54
link: http://www.edthedev.com/2007/OLD80/
published: true
comments: true
---
Blizzard has announced that they are working on a [new online game][2], but they have not committed to a name. I think it's fairly obvious that this game will be called ["World of Justice League RPM Racing Lost Vikings Diablo II"][1]

[1]: http://en.wikipedia.org/wiki/Blizzard_Entertainment#Titles "Games by Blizzard"

[2]: http://gigaom.com/2007/05/05/world-of-starcraft-new-blizzard-mmo-to-be-announced-may-19/ "Blizzard Announcement"