---
layout: post
title: "Message Boxes"
date: 2007-11-30 08:37:34
link: http://www.edthedev.com/2007/old69/
categories:
- Programming
published: true
comments: true
---
A word of advice: Message boxes are bludgeons, and your users are people who don't enjoy being bludgeoned.