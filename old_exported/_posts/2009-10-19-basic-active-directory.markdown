---
layout: post
title: "Basic Active Directory"
date: 2009-10-19 14:55:51
link: http://www.edthedev.com/?p=10573
categories:
- Random Geekiness
published: false
comments: true
---
LearnThat.com has a very nice tutorial on the core concepts of Active Directory. Highly recommended.

http://www.learnthat.com/Software/learn/1295/Introduction_to_Active_Directory/

If you didn't already click through, here are some questions that the 12 page article answers well:

! What's an Organization Unit and why create them?

"There are three primary reasons for creating OUs:

Organizational Structure: First, creating OUs allows a company to build a structure in Active Directory which matches their firm’s geographic or organizational structure. This permits ease of administration and a clean structure.

Security Rights: The second reason to create an OU structure is to assign security rights to certain OUs. This, for example, would allow you to apply Active Directory Policies to one OU which are different than another. You could setup policies which install an accounting software application on computers in the Accounting OU.

Delegated Administration: The third reason to create OUs is to delegate administrative responsibility. AD Architects can design the structure to allow local administrators certain administrative responsibility for their OU and no other. This allows for a delegated administration not available in Windows NT networks."

! If OU's are so powerful, why create sub-domains rather than just OUs?

"Domain-wide policies can be changed per domain in a domain tree which is not possible with only an OU structure. Policies such as minimum and maximum password age, minimum password length, and account lockout are domain-wide policies and cannot be changed on a per-OU basis. By creating multiple domains, administrators can set these policies for each domain."