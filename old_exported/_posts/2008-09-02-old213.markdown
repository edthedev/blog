---
layout: post
title: "Google Chrome! Hooray!"
date: 2008-09-02 12:01:41
link: http://www.edthedev.com/2008/old213/
categories:
- Security
published: true
comments: true
---
{% img http://edthedev.com/files/chrome-205_noshadow.png Google's Chrome Icon %}As a developer focused on security issues, I like to be an early adopter of new applications, especially ones that push the envelope (and potentially introduce new insecurities). So I downloaded Google's new web browser - [Google Chrome](http://www.google.com/chrome) - right away.

The Google guys are good at programming, but time has taught us that browser security is hard. Grab the newly released [Chrome Security Update](http://security.bkis.vn/?p=119) if you don't already have it. Chrome is a new product, so it's important to keep an eye out for updates until they've had a chance to work the bugs out.

I'm a bit disappointed to discover that [Chrome does not support RSS](http://www.rss4lib.com/2008/09/google_chrome_and_rss.html) yet. I realize it's first and foremost a browser; but it would have been nice to at least have a simple tie into [Google Reader](http://www.google.com/reader/).

I would love to share more of my thoughts with you, but my [Chrome](http://www.google.com/chrome) use is on hold until Google fixes some incompatibilities with [Atlassian Confluence](http://www.atlassian.com/software/confluence/).

If you just can't wait to learn more, check out [Scott Berkun's Review of Google Chrome](http://www.scottberkun.com/blog/2008/googles-web-browser-chrome-early-review/).

And, be sure to enjoy this [informative Google Chrome comic](http://www.google.com/googlebooks/chrome) by one of my heroes, [Scott McCloud](http://www.scottmccloud.com/), the author of the excellent guide, [Making Comics](http://www.amazon.com/Making-Comics-Storytelling-Secrets-Graphic/dp/0060780940/ref=pd_bbs_sr_1?ie=UTF8&s=books&qid=1220382040&sr=8-1).

- Edward