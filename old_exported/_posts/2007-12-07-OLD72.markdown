---
layout: post
title: "File Access in .Net"
date: 2007-12-07 13:16:12
link: http://www.edthedev.com/2007/OLD72/
published: true
comments: true
---
* [This][1] article has some decent example code, and attempts to demystify file access in .Net.
* [This one][2] goes a bit deeper and has an interesting bit about network streams.




[1]: http://www.ondotnet.com/pub/a/dotnet/2003/04/14/streams.html "ONDotnet.com .Net File Access"

[2]: http://www.codeguru.com/Csharp/Csharp/cs_data/streaming/article.php/c4223 "Code Guru"