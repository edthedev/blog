---
layout: post
title: "Google Chrome: Round Two"
date: 2008-09-17 20:59:29
link: http://www.edthedev.com/2008/google-chrome-round-two/
published: true
comments: true
---
[caption id="attachment_10303" align="alignnone" width="205" caption="Google Chrome Logo"]{% img http://www.edthedev.com/wp-content/uploads/2008/09/chrome-205_noshadow.png Google Chrome Logo %}[/caption]

My [previous experimentation](http://www.edthedev.com/2008/old213/) with [Google Chrome](http://www.google.com/googlebooks/chrome/) was cut short by Chrome's incompatibility with Confluence.

Luckily, we upgraded to [Confluence 2.8](http://confluence.atlassian.com/display/CONF28/Confluence+Documentation+Home), which seems to play quite nicely with Chrome.

Stay tuned for more Chrome news.

- Edward