---
layout: post
title: "Google Result Hijacking"
date: 2009-05-31 15:56:46
link: http://www.edthedev.com/2009/google-result-hijacking/
categories:
- Security
published: true
comments: true
---
Yahoo Tech News reports that a virus called 'Gumblar' is usurping control of vulnerable websites and then vulnerable home computers that visit those websites. Once installed, it redirects your Internet Explorer Google search results to spam pages or more virus delivery pages.

This is a classic technique, not a new one; but the article is a nice way to learn about it. 

The most important thing to note is that 'Gumblar' is targeting vulnerabilities that are almost a year old now. In a sane world, that wouldn't work because everyone would apply security patches. In the real world it works just fine. Applying software updates is the digital equivalent of washing your hands. Knowing that some of your neighbors don't even do it once a year is incentive to take extra care with your own computers.

Here's the article:
http://tech.yahoo.com/news/pcworld/20090514/tc_pcworld/newwaveofgumblarhackedsitesinstallsgoogletargetingmalware