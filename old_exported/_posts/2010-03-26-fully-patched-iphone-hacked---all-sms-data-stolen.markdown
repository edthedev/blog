---
layout: post
title: "Fully patched iPhone hacked - all SMS data stolen"
date: 2010-03-26 08:55:48
link: http://www.edthedev.com/?p=10762
categories:
- Random Geekiness
published: false
comments: true
---
This isn't really news to security geeks, but for everyone else:

Your fully patched iPhone is vulnerable to an attack that steals all of your SMS text messages, including some that you have actually deleted.
http://blogs.zdnet.com/security/?p=5836

That said, it's the good guys who figured this out, and the vulnerability isn't particularly fast acting. Hopefully Apple will have a mitigation ready soon.