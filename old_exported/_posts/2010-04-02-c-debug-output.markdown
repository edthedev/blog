---
layout: post
title: "C# Debug output"
date: 2010-04-02 08:33:18
link: http://www.edthedev.com/2010/c-debug-output/
categories:
- Example Code
- Solutions
published: true
comments: true
---
Here's a snippet that I use to push debug output to the console, only when compiled in Debug mode.


private static void DebugOutput(String message)
{
  DebugOutput(message, null);
}

private static void DebugOutput(String message, object details)
{
# if DEBUG
  System.Console.WriteLine(message, details);
# endif
}

