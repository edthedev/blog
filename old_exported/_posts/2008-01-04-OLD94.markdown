---
layout: post
title: "Editor-Foo"
date: 2008-01-04 14:21:22
link: http://www.edthedev.com/2008/OLD94/
published: true
comments: true
---
Here's a [link][1] to some great Editor-Foo*. 
What is Editor-Foo, you ask? Well Kung-Foo is mastery of the body; Script-Foo is mastery of scripting. Editor-Foo is arguably as critical as either one - the mastery of your editor of choice.

This [page][1] demonstrates some cool techniques that will work in the featured Notepad++, and will also work in Crimson, and should work in many other Editor-Foo ready text editors.

[1]: http://thinkersroom.com/bytes/2006/11/20/grey-matter-code-generation-on-a-budget/