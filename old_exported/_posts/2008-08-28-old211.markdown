---
layout: post
title: "Front Door, NoScript, Web of Trust, Common Sense"
date: 2008-08-28 05:33:57
link: http://www.edthedev.com/2008/old211/
categories:
- Security
published: true
comments: true
---
At the top of your Firefox window, click on the 'Tools' menu.
Now select 'Add-ons'.

Your window should look a bit like mine, in the screen-shot below.

This is really important - it is common sense.
If you live without a front door, or if you announce your credit card number and security code in movie theaters, then you may ignore the rest of this post. The internet is great, but it does have users and websites who want to hijack your computer and steal your credit card numbers. It happens all the time to people just like you and me.

So look at the window we just brought up, I want you to make sure you have the two items displayed in my screenshot.

{% img http://edthedev.com/files/ff_ext.png %}

[caption id="attachment_10240" align="alignnone" width="300" caption="My Firefox Plugins"]{% img http://www.edthedev.com/wp-content/uploads/2008/09/ff_ext-300x236.jpg My Firefox Plugins %}[/caption]

NoScript is more important than antivirus. Antivirus is like mousetraps. NoScript is like having a front door rather than a gaping hole.

Web of Trust (WOT) is Neighborhood Watch for the internet. How do you know who you can trust on the internet? Web of Trust tells you. In real life, there's no way to have a red warning pop up when you're about to buy a 'car' from a con-man who has spent his entire life in and out of jail. On the internet, Web of Trust can warn you before you give him your credit card number.