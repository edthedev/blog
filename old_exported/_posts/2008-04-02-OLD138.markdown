---
layout: post
title: "Firefox 2.0.0.13 Acid3 Test - Epic Fail!"
date: 2008-04-02 10:07:00
link: http://www.edthedev.com/2008/OLD138/
published: true
comments: true
---
[Acid3][1] is a new effort to make web standards more consistent across browsers, increase the accessibility of the internet, and make web developers sound like drug addicts.

[1]: http://acid3.acidtests.org/

Both [Opera][2] and [Safari][3] just announced that their latest versions score 100/100 on the test, although they still don't perfectly match the correct image.
[2]: http://www.opera.com/ "Opera Web Browser"
[3]: http://www.apple.com/safari/ "Safari Web Browser"

[Firefox][4], on the other hand, managed this epic failure:
[4]: http://www.mozilla.com/en-US/firefox/ "Firefox Web Browser"

![5](http://edthedev.com/files/firefox_acid3_fail.jpg)

Now here is what it is supposed to look like:

![7](http://edthedev.com/files/acid3_correct.jpg)

Firefox's current rendering is not even close.

This doesn't in any way decrease my love and devotion to Firefox. But I will be watching to see if Firefox improves it's score with the next release.

But, Firefox deserves some love. Microsoft Internet Explorer 7.053 managed this disaster rendering:

![6](http://edthedev.com/files/ie_acid3_fail.jpg)

I can't even see the score in there.

[Acid3][1] isn't exactly a fair test right now - it was designed to highlight the deficiencies in consistency between browsers, and it does that wonderfully. At the very least, I hope it will generate some sympathy for those of us who have to deal with these inconsistencies.

Update: 

Firefox 3 Beta 5 does much better! All that seems to be missing is the colors.
![7](http://edthedev.com/files/acid3_firefox3.jpg)

- Edward