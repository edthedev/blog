---
layout: post
title: "How to identify a programmer with minimal effort"
date: 2010-04-06 20:09:59
link: http://www.edthedev.com/2010/how-to-identify-a-programmer-with-minimal-effort/
categories:
- Programming
published: true
comments: true
---
Stack Overflow has a very cute and clever discussion about how to spot a programmer without looking for source code:
http://stackoverflow.com/questions/895296/how-can-you-tell-if-a-person-is-a-programmer

I would add to the list, if he or she actually is aware of (and has tried to optimize) their response algorithm for when people interrupt them from deep thought.