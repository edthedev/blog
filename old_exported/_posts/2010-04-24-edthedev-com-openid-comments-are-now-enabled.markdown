---
layout: post
title: "EdTheDev.com OpenID comments are now enabled."
date: 2010-04-24 15:12:32
link: http://www.edthedev.com/2010/edthedev-com-openid-comments-are-now-enabled/
categories:
- Solutions
published: true
comments: true
---
Thank you to my buddy Rob for guilting me into finally getting OpenID support installed. 

You can now comment on any article on EdTheDev.com using your existing Yahoo.com, Google.com, or Facebook.com account (and about a million others, actually).