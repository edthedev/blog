---
layout: post
title: "Disable the Capslock Key"
date: 2008-05-21 12:17:12
link: http://www.edthedev.com/2008/OLD177/
published: true
comments: true
---
I'm a fan of this [technique for disabling the capslock key](http://johnhaller.com/jh/useful_stuff/disable_caps_lock/), but don't recommend it if you want to be able to use a screen reader like [Thunder](http://www.screenreader.net/index.php?section=Thunder%20and%20Manuals), which claims the capslock key as it's own.

Ironically, everyone seems to agree that capslock, itself, is useless.

- Edward