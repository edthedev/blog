---
layout: post
title: "Is some smoke okay?"
date: 2009-11-22 14:58:47
link: http://www.edthedev.com/2009/is-some-smoke-okay/
categories:
- Random Geekiness
published: true
comments: true
---
My brother and I were discussing how sometimes someone asks for your help diagnosing their computer / iPhone / digital clock problem, and you ask them if anything strange happened lately, and they say no. You take it apart and have a look inside, and see burned out capacitors. So you ask "Did this let off smoke recently?" and they say "Yes."

There are people who do not know which devices they own should and should not give off smoke. So as a public service, I'm writing a guide: Is some smoke okay?
<table style="width: 718px; height: 189px;" border="0"><caption>Is giving off some smoke okay?</caption>
<tbody>
<tr>
<td>**Device Name**</td>
<td>**Is some smoke okay?**</td>
</tr>
<tr>
<td>Desktop computer</td>
<td>Not okay.</td>
</tr>
<tr>
<td>Laptop computer</td>
<td>Not okay.</td>
</tr>
<tr>
<td>Lamp</td>
<td>Not okay.</td>
</tr>
<tr>
<td>Power cord</td>
<td>Not okay.</td>
</tr>
<tr>
<td>Charcoal Grill</td>
<td>Okay.</td>
</tr>
<tr>
<td>Cellular phone</td>
<td>Not okay.</td>
</tr>
<tr>
<td>Candle</td>
<td>Okay.</td>
</tr>
<tr>
<td>VHS Tape</td>
<td>Not okay.</td>
</tr>
<tr>
<td>Television set</td>
<td>Not okay.</td>
</tr>
<tr>
<td>Cooking pot or pan</td>
<td>Okay.</td>
</tr>
<tr>
<td>Booklight</td>
<td>Not okay.</td>
</tr>
<tr>
<td>Printer</td>
<td>Not okay.</td>
</tr>
<tr>
<td>Microwave popcorn</td>
<td>Usually okay.</td>
</tr>
<tr>
<td>Cigarette</td>
<td>Okay.</td>
</tr>
<tr>
<td>Toaster</td>
<td>Okay.</td>
</tr>
<tr>
<td>Well done steak</td>
<td>Okay.</td>
</tr>
<tr>
<td>Electric blanket</td>
<td>Not okay.</td>
</tr>
<tr>
<td>Camp fire</td>
<td>Okay.</td>
</tr>
<tr>
<td>Gas or electric grill</td>
<td>Okay.</td>
</tr>
<tr>
<td>Pen light</td>
<td>Not okay.</td>
</tr>
<tr>
<td>Automobile</td>
<td>Okay.</td>
</tr>
<tr>
<td>Speakers</td>
<td>Not okay.</td>
</tr>
</tbody></table>
Tune in next week for ongoing segments, including "Is it okay if it's really really hot?" and "Can I safely submerge it in water?"