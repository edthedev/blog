---
layout: post
title: "Not sure how your website looks?"
date: 2008-08-15 07:57:14
link: http://www.edthedev.com/2008/old209/
published: true
comments: true
---
[BrowserShots](http://browsershots.org/) is an awesome tool that loads your website in many different web browsers and takes a screenshot so that you can see what your site looks like to users of other platforms.

Enjoy!

- Edward