---
layout: post
title: "Cory Doctrow on Phishing"
date: 2010-05-12 07:59:52
link: http://www.edthedev.com/2010/cory-doctrow-on-phishing/
categories:
- Security
published: true
comments: true
---
Cory Doctrow, author of [Little Brothe](http://craphound.com/littlebrother/download/)r, wrote an excellent [article about being taken by a phishing attack](http://www.locusmag.com/Perspectives/2010/05/cory-doctorow-persistence-pays-parasites/). Cory has a unique talent for making security concepts accessible.