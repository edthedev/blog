---
layout: post
title: "Excellent article on the merits of Agile AND Waterfall software development methodologies"
date: 2011-10-26 07:23:56
link: http://www.edthedev.com/?p=11226
categories:
- Random Geekiness
published: false
comments: false
---
"Usually, it takes a strong business analyst from IT to untangle threads [of complex business logic]. But agile takes business analysts out of the loop, preferring direct interaction between developers and users. Complex business logic inevitably pushes these types of projects in a waterfall-ish direction." And that is a good thing.


http://www.infoworld.com/t/agile-development/end-the-holy-war-over-agile-development-177022