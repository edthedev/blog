---
layout: post
title: "JavaDoc checklist"
date: 2010-02-18 14:16:15
link: http://www.edthedev.com/2010/javadoc-checklist/
categories:
- Example Code
- Programming
published: true
comments: true
---
Things to remember when writing JavaDoc:
### Class declaration

{% blockquote %}
<div id="_mcePaste">/**</div>
<div id="_mcePaste">* (Class name)</div>
<div id="_mcePaste">*</div>
<div id="_mcePaste">* (Purpose of the class...)</div>
<div id="_mcePaste">* <p></div>
<div>* (Copyright)</div>
<div>* <p></div>
<div id="_mcePaste">* (License)</div>
<div id="_mcePaste">*</div>
<div id="_mcePaste">* @author Edward Delaporte, Research Programmer, University of Illinois</div>
<div id="_mcePaste">* @author (anyone else), Research Programmer, University of Illinois</div>
<div id="_mcePaste">* <p></div>
<div id="_mcePaste">* Last updated by $Author$</div>
<div id="_mcePaste">* @version $Revision$</div>
<div id="_mcePaste">* <p></div>
<div id="_mcePaste">* Subversion URL: $URL$</div>
<div id="_mcePaste">*/</div>{% endblockquote %}
### Method block


	- What it does.
	- Parameters with @param variableName description
	- Return type with @return description
	- Possible exceptions with @throws, including known causes.

* Comment every control structure.

Reference: http://www.ajkuiper.com/javadoc%20and%20the%20art%20of%20speccing.htm

http://www.otm.illinois.edu/faculty/forms/opensource.asp