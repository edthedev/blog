---
layout: post
title: "Ben Writes on Speeding up Vista"
date: 2009-03-23 17:54:51
link: http://www.edthedev.com/2009/ben-writes-on-speeding-up-vista/
categories:
- Solutions
published: true
comments: true
---
Windows Vista isn't known for making good use of your hardware resources. Many of the applications you install make the problem worse by running all the time, whether you're using them or not.

If your Vista computer is running slow, you should check out Ben's illustrated walk-through on [Disabling start-up processes in Windows Vista](http://ben-writes.com/?p=35).