---
layout: post
title: "Image Cropping in Linux"
date: 2008-04-03 13:51:13
link: http://www.edthedev.com/2008/OLD140/
published: true
comments: true
---
A [friend of a friend][1] has solved my conundrum of what to use for cropping and resizing images when running Linux. He suggests [Mirage][2] as an alternative to the incredibly slow loading [Gimp][3]. Thanks Jake.

[1]: http://listento.jaketolbert.com/?p=1350
[2]: http://linux.softpedia.com/get/Multimedia/Graphics/Mirage-10810.shtml
[3]: http://www.gimp.org/ "GNU Image Manipulation Program"

If anyone found this post looking for a similar solution for Windows, I recommend [Paint.Net][4]; a rarity from Microsoft: a tool that is free, excellent, and not installed by default.

[4]: http://www.getpaint.net/

- Edward