---
layout: post
title: "My favorite hacks"
date: 2009-12-30 08:52:25
link: http://www.edthedev.com/2009/my-favorite-hacks/
categories:
- Random Geekiness
published: true
comments: true
---
To the tune of 'My Favorite Things'.

Pointers to pointers,
And references dereferenced,
C strings escaped,
and a registry preferenced.
Serialized instances sorted in stacks,
These are a few of my favorite hacks.

(chorus)
When the thread locks, 
and MEM hits max.
when I'm out of luck...
I simply remember my favorite hacks,
and then I don't feel so stuck.

Wix MSI
and some installs by OneClick.
Registry rollback 
when PC is a brick.

Backgrounded install for all service packs,
These are a few of my favorite hacks.

(repeat chorus)

127.zero.oh-one
Localhost loopback
and self-serving test runs.

Localized SqlLite storing some MACs. 
These are a few of my favorite hacks.

(repeat chorus)

Shared folder data,
import through a spreadsheet,
WordMerge then parse back,
and upload and repeat.
Specialized servers turn emails to fax.
These are a few of my favorite hacks.

(repeat chorus)

Php Escape string,
and regex in taint mode,
use strict or warning,
but never use globals.

Update your language or suffer attacks,
these are a few of my favorite hacks.