---
layout: post
title: "The Fairy Tale Beauty of Unit Testing"
date: 2009-02-28 08:51:22
link: http://www.edthedev.com/2009/the-fairy-tale-beauty-of-unit-testing/
categories:
- Random Geekiness
published: true
comments: true
---
To demonstrate the incredible beauty of [unit testing](http://en.wikipedia.org/wiki/Unit_test), I have composed this short script. Imagine how much better your favorite fairy-tales would be if they conformed to modern programming best practices!


ok(here.contains('shoemaker'))
ok(now() >= midnight)
ok(now() <= sunrise)
elf.enter()
while not shelf.is_full():
  elf.make_shoe()
ok(shelf.is_full())



Yea, I'm weird. I know.