---
layout: post
title: "This is a Topical Blog"
date: 2008-01-23 18:38:46
link: http://www.edthedev.com/2008/OLD101/
published: true
comments: true
---
I referred to this blog as 'topical' in a conversation with my wife.

She suggested that I add the appropriate instructions:


<code>
Apply small amount to palm.
Lather well.
Rinse.
Repeat as needed.
</code>
