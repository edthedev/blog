---
layout: post
title: "Shattering sounds"
date: 2010-07-16 14:49:37
link: http://www.edthedev.com/?p=11081
categories:
- Random Geekiness
published: false
comments: true
---
This week's geek hero award goes to Changxi Zheng and Doug L. James, for their work with computer simulating the sounds that things make when they break.

<object width="640" height="385">



<embed src="http://www.youtube.com/v/nHH8N_lNZzI&rel=0&color1=0xb1b1b1&color2=0xd0d0d0&hl=en_US&feature=player_embedded&fs=1" type="application/x-shockwave-flash" allowfullscreen="true" allowScriptAccess="always" width="640" height="385"></embed></object>

Read more about what you are seeing, over at:
http://www.cs.cornell.edu/projects/FractureSound/