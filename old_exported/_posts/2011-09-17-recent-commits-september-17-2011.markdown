---
layout: post
title: "Recent Commits September 17, 2011"
date: 2011-09-17 17:32:18
link: http://www.edthedev.com/2011/recent-commits-september-17-2011/
categories:
- Example Code
published: true
comments: false
---
<ul class="scrd_digest">
- [Improved project handling in next.](https://bitbucket.org/edthedev/edthedev/changeset/1a261bebb865)
<div>Improved project handling in next.</div>

- [Bugfix](https://bitbucket.org/edthedev/edthedev/changeset/7e32e3f44ca2)
<div>Bugfix</div>

- [Actually fixed bug...](https://bitbucket.org/edthedev/edthedev/changeset/7566561d307e)
<div>Actually fixed bug...</div>

- [Bugfixes](https://bitbucket.org/edthedev/edthedev/changeset/027f9c62b31a)
<div>Bugfixes</div>

- [Various tweaks.
check_script is nice. Makes sure the script compiles, and walks you through the help(class) and class.py --help outputs.](https://bitbucket.org/edthedev/edthedev/changeset/74757874aaad)
<div>Various tweaks.
check_script is nice. Makes sure the script compiles, and walks you through the help(class) and class.py --help outputs.</div>

- [Bugfixes and better instructions.
- Handles missing NOTES_HOME without freaking out.
- Better instructions for setting NOTES_HOME
- Those two are totally unrelated. Yea.](https://bitbucket.org/edthedev/edthedev/changeset/b6bab1d00537)
<div>Bugfixes and better instructions.
- Handles missing NOTES_HOME without freaking out.
- Better instructions for setting NOTES_HOME
- Those two are totally unrelated. Yea.</div>

- [In the middle of bugfixes.](https://bitbucket.org/edthedev/edthedev/changeset/5a6fce9725ba)
<div>In the middle of bugfixes.</div>

- [Org now allows arbitrary boxes.
Move items to a box using >box_name.](https://bitbucket.org/edthedev/edthedev/changeset/86aef9a5aa1e)
<div>Org now allows arbitrary boxes.
Move items to a box using >box_name.</div>

- [Better names for package list management scripts.](https://bitbucket.org/edthedev/edthedev/changeset/ca2b4c688eda)
<div>Better names for package list management scripts.</div>


Digest powered by [RSS Digest](http://www.rssdigestpro.com)
