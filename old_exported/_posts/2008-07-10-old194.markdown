---
layout: post
title: "Just Barely Good Enough"
date: 2008-07-10 11:22:18
link: http://www.edthedev.com/2008/old194/
categories:
- Programming
published: true
comments: true
---
Here's a new perspective: try to make your systems [Just Barely Good Enough](http://www.agilemodeling.com/essays/barelyGoodEnough.html).