---
layout: post
title: "EdTheDev.com - Now nicer for iPod and iPhone readers."
date: 2009-05-31 15:54:52
link: http://www.edthedev.com/2009/edthedev-nicer-for-ipod-and-iphone-readers/
categories:
- Random Geekiness
published: true
comments: true
---
I just installed [Brave New Code's](http://bravenewcode.com) [WPTouch wordpress add-on](http://www.bravenewcode.com/wptouch/) WordPress add-on. If you read this blog from your iPhone or iPod, you'll see an amazing improvement in usability.