---
layout: post
title: "Index Card Art"
date: 2007-12-09 05:35:27
link: http://www.edthedev.com/2007/OLD75/
published: true
comments: true
---
![A black and white abstract.][2]

I drew this on the back of a 3 by 5 index card.
I tried to imply a three dimensional texture; while leaving enough two dimensional lines to make the onlooker uncertain that their chosen perspective is accurate.

<!-- break -->

![A colorful abstract.][1]

This was also drawn on the back of a 3 by 5 index card; but first I cut it to create a different shape of canvas. I colored it with pens and pencils.

[1]: http://www.edthedev.com/files/future.jpg
[2]: http://www.edthedev.com/files/patternRecognition.jpg