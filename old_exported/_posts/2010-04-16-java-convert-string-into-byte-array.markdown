---
layout: post
title: "Java convert string into byte array"
date: 2010-04-16 20:17:00
link: http://www.edthedev.com/2010/java-convert-string-into-byte-array/
categories:
- Random Geekiness
published: true
comments: true
---
Have a string and need to turn it into bytes?

<code>
String myString = "This is some input.";
byte[] myBytes = myString.getBytes("US-ASCII");
</code>
