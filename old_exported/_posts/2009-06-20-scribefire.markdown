---
layout: post
title: "ScribeFire for Firefox!"
date: 2009-06-20 07:09:48
link: http://www.edthedev.com/2009/scribefire/
categories:
- Random Geekiness
published: true
comments: true
---
I'm trying out a new way to update my blog. Hopefully it will lead to me updating more often.  In the meantime, check out the editor I'm using:  [Getting Started With ScribeFire - Scribefire: Fire up your blogging](http://blog.scribefire.com/help/firstrun/)  