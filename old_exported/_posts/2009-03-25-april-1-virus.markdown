---
layout: post
title: "April 1 Virus"
date: 2009-03-25 19:11:30
link: http://www.edthedev.com/2009/april-1-virus/
categories:
- Security
- Solutions
published: true
comments: true
---
Are you wondering whether you should be worried about the Conficker (also known as Downandup) worm? Are you not even sure what it is?

[John Markoff](http://bits.blogs.nytimes.com/author/john-markoff/) wrote an article for the New York Times explaining what Conficker is, and why people are worried about what it's going to do on April 1.  To learn what all the concern is about, check out [John Markoff explains Conficker](http://bits.blogs.nytimes.com/2009/03/19/the-conficker-worm-april-fools-joke-or-unthinkable-disaster/).

Now that you know what you're dealing with, you probably want more information. My colleagues at the University of Illinois have gathered some excellent links to Conficker resources. Check out
[University of Illinois resources for the Conficker Downandup Virus](http://www.library.uiuc.edu/it/helpdesk/virusalert.html).