---
layout: post
title: "Ubuntu Clock Screenlet"
date: 2009-12-01 21:33:28
link: http://www.edthedev.com/2009/ubuntu-clock-screenlet/
categories:
- Solutions
published: true
comments: true
---
I'm not particularly impressed with any of the available Ubuntu clock widgets. Luckily, I found a better option.
If you want a nice clock in Ubuntu, you may choose to:



1. Install Screenlets (Applications - Ubuntu Software Center)
2. Run Screenlets (Applications - Accessories - Screenlets)
3. Launch an extra copy of the 'Output' screenlet (Select 'Output', then press 'Launch/Add')
4.Configure the 'Output' screenlet with these settings: (Right-click the grey box that appeared somewhere on your desktop.)
  a. Under Properties - Options - Screenlet
  a.i. Scale: 5
  b. Under Properties - Options - Options
  b.i. Command to run: date +%I:%M
  b.ii. Update interval seconds: 15
  b.iii. Width: 50
  b.iv.  Height: 25


Now drag it to wherever you want, and enjoy.