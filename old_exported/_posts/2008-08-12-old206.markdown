---
layout: post
title: "Revisiting Interface Design"
date: 2008-08-12 10:55:40
link: http://www.edthedev.com/2008/old206/
categories:
- Programming
published: true
comments: true
---
Here's an article on interface design that was recommended to me by a colleague who is an interface design specialist (and a very good one). Check it out at [Glyphobet.net](http://glyphobet.net/blog/essay/269).