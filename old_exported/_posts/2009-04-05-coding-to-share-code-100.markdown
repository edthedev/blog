---
layout: post
title: "Coding to Share Code 100"
date: 2009-04-05 05:47:01
link: http://www.edthedev.com/?p=10520
categories:
- Random Geekiness
published: false
comments: true
---
The security-minded will realize that I have just introduced my first vulnerability. I appear to be letting un-authenticated users run SQL commands against my database. In reality, because I did my database access through [CodeIgniter's Data Access Library](http://codeigniter.com/user_guide/database/index.html), ...