---
layout: post
title: "Programming is a complicated yet noble profession"
date: 2011-02-21 19:08:05
link: http://www.edthedev.com/2011/programming-is-a-complicated-yet-noble-profession/
categories:
- Programming
published: true
comments: false
---
"I have spent hundreds of thousands of dollars on software, tools, books, and equipment that tell me what I want to hear -- that programming is a complicated yet noble profession which involves mastering lots of technical details and being plugged into the latest technology. But programming is helping people, not any of that stuff." - [Daniel B Markham](http://www.whattofix.com/blog/archives/2011/02/programmer-as-m.php)