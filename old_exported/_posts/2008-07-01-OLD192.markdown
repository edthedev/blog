---
layout: post
title: "Fun With Vim"
date: 2008-07-01 14:53:54
link: http://www.edthedev.com/2008/OLD192/
published: true
comments: true
---
[Cats Who Code](
http://www.catswhocode.com/) have a great [list of Vim commands](http://www.catswhocode.com/blog/web-development/100-vim-commands-every-programmer-should-know-11) that is well worth checking out.