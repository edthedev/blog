---
layout: post
title: "Illinois homepage usability"
date: 2010-07-30 07:20:01
link: http://www.edthedev.com/2010/illinois-homepage-usability/
categories:
- Random Geekiness
published: true
comments: true
---
[Today's XKCD](http://xkcd.com/773/) is absolutely spot on. I'm happy to say that the [Illinois homepage](http://illinois.edu) is much improved on this score. My colleagues did a usability study a year ago and learned and addressed the exact information XKCD is teasing about. If you visit the Illinois homepage, notice that everything XKCD put in the right circle is available within 2 clicks. Maybe one of our team is feeding XKCD's author ideas. In any case, we're laughing pretty hard at today's comic.