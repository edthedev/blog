---
layout: post
title: "Cancel a print job, seriously"
date: 2007-11-22 09:22:37
link: http://www.edthedev.com/2007/old13/
categories:
- Solutions
published: true
comments: true
---
So your family member printed their entire 10,000 page autobiography, only to realize that he or she mis-spelled the title page. No problem. Just bring up the print manager, right click on the print job, and select Cancel.

Now your printer will 'cancel' printing that 10,000 page autobiography, just as soon as it's done printing the remaining 9,999 pages. What?! You don't want to wait until the printer is good and ready to handle your request?

Well the first thing to realize is that printers think very slowly. Anything you ask them to do, it's going to take some time thinking about it. Be patient.

But what about that little unresponsive 'Print jobs' window that usually ignores every command you give it?
It's running on your super-fast computer, not in the printer's itty-bitty-printer-brain. So what's it's excuse?

There is no excuse. But there is a solution.
If you want to cancel all pending print jobs and start over from scratch, bring up your trusty Command Prompt, or PowerShell prompt, and type:
<code>net stop spooler</code>
And you will see:
<code>The Print Spooler service is stopping.
The Print Spooler service was stopped successfully.</code>

Then type:
<code>net start spooler</code>
And you will see:
<code>
The Print Spooler service is starting.
The Print Spooler service was started successfully.
</code>

Most printers are actually quite clever (in a slow way)- but they won't act clever when the print spooler (the code on your computer) gets confused and feeds them lots of bad instructions at really high speed. Using this trick to reset the print spooler is sometimes all it takes to turn a little monster of a printer into a little angel.

But remember, your printer's itty-bitty-printer-brain is going to take time to digest the fact that the print spooler on your computer isn't abusing it anymore. Give the printer itself the time that it needs, and you'll find it does alright, once the print spooler is behaving.