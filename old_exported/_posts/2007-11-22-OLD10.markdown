---
layout: post
title: "List your installed certificates using PowerShell"
date: 2007-11-22 09:18:56
link: http://www.edthedev.com/2007/OLD10/
published: true
comments: true
---
Code signing is a very good practice. But very few of us practice it, often because we simply don't know much or anything about it. This is the first of several posts that will attempt to demystify the code signing experience and the related Windows technologies.

In order to sign code, you need to have a code signing certificate. "Where do I get a code signing certificate?", you may ask. You can create one for yourself. I'll teach you how to do so in a future blog entry. Note: Not all code signing certificates are created equal. Unless you're a very popular entity, using a self-generated code singing certificate doesn't mean much to end users (who have never heard of you). For professional applications, you should purchase a code signing certificate from a well known Certificate Authority.

For now, et's just learn how to check whether you have any code signing certificates installed on your computer. 

Although you and I need someone to teach us all about certificates, Windows PowerShell is born with an understanding.

This code will list the contents of your current user's certificate store:
<code>
ls cert:\CurrentUser\My
</code>

<code>cert:\CurrentUser\My</code> is an explorable path that responds to many of the commands that you use on directories. Just keep in mind that it is not actually a directory - PowerShell is just being nice by letting you use many of the same commands to explore it.