---
layout: post
title: "Recent Commits January 14, 2012"
date: 2012-01-14 17:58:05
link: http://www.edthedev.com/2012/recent-commits-january-14-2012/
categories:
- Example Code
published: true
comments: false
---
<ul class="scrd_digest">
- [Automerge](https://bitbucket.org/edthedev/edthedev/changeset/1bdd63fdc0b5)
<div>Automerge
  
  
  
  
  
  
  - [
  kindle/website2kindle
  ](https://bitbucket.org/edthedev/edthedev/src/1bdd63fdc0b5/kindle/website2kindle)(19 lines added, 0 lines removed)
  
  
  - [
  text/clean-numbers
  ](https://bitbucket.org/edthedev/edthedev/src/1bdd63fdc0b5/text/clean-numbers)(61 lines added, 0 lines removed)
  
  
  - [
  text/textbar
  ](https://bitbucket.org/edthedev/edthedev/src/1bdd63fdc0b5/text/textbar)(50 lines added, 0 lines removed)
  
  
  - [
  vim/hardcore_mode
  ](https://bitbucket.org/edthedev/edthedev/src/1bdd63fdc0b5/vim/hardcore_mode)(0 lines added, 8 lines removed)
  
  </div>

- [Added rsync script to pack up source code for release. Gets the little details correct that I otherwise might forget.](https://bitbucket.org/edthedev/edthedev/changeset/56ede5f5c190)
<div>Added rsync script to pack up source code for release. Gets the little details correct that I otherwise might forget.
  
  
  
  
  
  
  - [
  packaging/release-source
  ](https://bitbucket.org/edthedev/edthedev/src/56ede5f5c190/packaging/release-source)(14 lines added, 0 lines removed)
  
  </div>

- [Nicer handling of two screens. Includes ability to name the monitors.](https://bitbucket.org/edthedev/edthedev/changeset/288fc59bdad5)
<div>Nicer handling of two screens. Includes ability to name the monitors.
  
  
  
  
  
  
  - [
  config/twoScreens
  ](https://bitbucket.org/edthedev/edthedev/src/288fc59bdad5/config/twoScreens)(10 lines added, 3 lines removed)
  
  </div>

- [Fix for zeroes.](https://bitbucket.org/edthedev/edthedev/changeset/467052916e82)
<div>Fix for zeroes.
  
  
  
  
  
  
  - [
  text/clean-numbers
  ](https://bitbucket.org/edthedev/edthedev/src/467052916e82/text/clean-numbers)(5 lines added, 2 lines removed)
  
  </div>

- [Added scripts.](https://bitbucket.org/edthedev/edthedev/changeset/42477037cff1)
<div>Added scripts.
  
  
  
  
  
  
  - [
  text/clean-numbers
  ](https://bitbucket.org/edthedev/edthedev/src/42477037cff1/text/clean-numbers)(58 lines added, 0 lines removed)
  
  
  - [
  text/textbar
  ](https://bitbucket.org/edthedev/edthedev/src/42477037cff1/text/textbar)(50 lines added, 0 lines removed)
  
  </div>

- [Automerged separate files...why is Hg stupid about this?](https://bitbucket.org/edthedev/edthedev/changeset/82c10c940bf8)
<div>Automerged separate files...why is Hg stupid about this?
  
  
  
  
  
  
  - [
  coding/newscript
  ](https://bitbucket.org/edthedev/edthedev/src/82c10c940bf8/coding/newscript)(0 lines added, 9 lines removed)
  
  
  - [
  vimrc_edthedev
  ](https://bitbucket.org/edthedev/edthedev/src/82c10c940bf8/vimrc_edthedev)(1 lines added, 0 lines removed)
  
  </div>

- [Added PyDoc to vim shortcuts.](https://bitbucket.org/edthedev/edthedev/changeset/2a288c2da674)
<div>Added PyDoc to vim shortcuts.
  
  
  
  
  
  
  - [
  coding/newscript
  ](https://bitbucket.org/edthedev/edthedev/src/2a288c2da674/coding/newscript)(0 lines added, 9 lines removed)
  
  
  - [
  vimrc_edthedev
  ](https://bitbucket.org/edthedev/edthedev/src/2a288c2da674/vimrc_edthedev)(1 lines added, 0 lines removed)
  
  </div>


Digest powered by [RSS Digest](http://www.rssdigestpro.com)
