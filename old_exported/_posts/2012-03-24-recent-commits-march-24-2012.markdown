---
layout: post
title: "Recent Commits March 24, 2012"
date: 2012-03-24 17:42:23
link: http://www.edthedev.com/2012/recent-commits-march-24-2012/
categories:
- Example Code
published: true
comments: false
---
<ul class="scrd_digest">
- [Scoreboard gets cleared nicely, without clearing the rest of the screen.](https://bitbucket.org/edthedev/edthedev/changeset/2266010710b6)
<div>Scoreboard gets cleared nicely, without clearing the rest of the screen.
  
  
  
  
  
  
  - [
  javascript/basketball.html
  ](https://bitbucket.org/edthedev/edthedev/src/2266010710b6/javascript/basketball.html)(36 lines added, 21 lines removed)
  
  </div>

- [Removed colors to make basketball more visible.](https://bitbucket.org/edthedev/edthedev/changeset/bfccfa4f6a08)
<div>Removed colors to make basketball more visible.
  
  
  
  
  
  
  - [
  javascript/basketball.html
  ](https://bitbucket.org/edthedev/edthedev/src/bfccfa4f6a08/javascript/basketball.html)(13 lines added, 11 lines removed)
  
  </div>

- [Added the basics of the basketball game.](https://bitbucket.org/edthedev/edthedev/changeset/491d11b973cd)
<div>Added the basics of the basketball game.
  
  
  
  
  
  
  - [
  javascript/basketball.html
  ](https://bitbucket.org/edthedev/edthedev/src/491d11b973cd/javascript/basketball.html)(28 lines added, 12 lines removed)
  
  </div>

- [Added cool drift.](https://bitbucket.org/edthedev/edthedev/changeset/517210643849)
<div>Added cool drift.
  
  
  
  
  
  
  - [
  javascript/basketball.html
  ](https://bitbucket.org/edthedev/edthedev/src/517210643849/javascript/basketball.html)(18 lines added, 3 lines removed)
  
  </div>

- [Clicking erases the ball trails.](https://bitbucket.org/edthedev/edthedev/changeset/9e301ac646e6)
<div>Clicking erases the ball trails.
  
  
  
  
  
  
  - [
  javascript/basketball.html
  ](https://bitbucket.org/edthedev/edthedev/src/9e301ac646e6/javascript/basketball.html)(3 lines added, 0 lines removed)
  
  </div>

- [Launch from the click point. PLUS....
Colorball!!!](https://bitbucket.org/edthedev/edthedev/changeset/25e6502c4f99)
<div>Launch from the click point. PLUS....
Colorball!!!
  
  
  
  
  
  
  - [
  javascript/basketball.html
  ](https://bitbucket.org/edthedev/edthedev/src/25e6502c4f99/javascript/basketball.html)(6 lines added, 6 lines removed)
  
  </div>

- [The ball now moves to the mouse position when you click on it.](https://bitbucket.org/edthedev/edthedev/changeset/8807da20bc41)
<div>The ball now moves to the mouse position when you click on it.
  
  
  
  
  
  
  - [
  javascript/basketball.html
  ](https://bitbucket.org/edthedev/edthedev/src/8807da20bc41/javascript/basketball.html)(30 lines added, 10 lines removed)
  
  </div>

- [Let's play basketball.](https://bitbucket.org/edthedev/edthedev/changeset/fbb701746e7b)
<div>Let's play basketball.
  
  
  
  
  
  
  - [
  javascript/basketball.html
  ](https://bitbucket.org/edthedev/edthedev/src/fbb701746e7b/javascript/basketball.html)(82 lines added, 0 lines removed)
  
  </div>

- [Added example javascript bouncing ball code.](https://bitbucket.org/edthedev/edthedev/changeset/439c04e26d51)
<div>Added example javascript bouncing ball code.
  
  
  
  
  
  
  - [
  config/cleanup-all-the-downloads
  ](https://bitbucket.org/edthedev/edthedev/src/439c04e26d51/config/cleanup-all-the-downloads)(2 lines added, 0 lines removed)
  
  
  - [
  javascript/bouncingball.html
  ](https://bitbucket.org/edthedev/edthedev/src/439c04e26d51/javascript/bouncingball.html)(82 lines added, 0 lines removed)
  
  </div>


Digest powered by [RSS Digest](http://www.rssdigestpro.com)
