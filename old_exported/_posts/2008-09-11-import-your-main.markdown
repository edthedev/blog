---
layout: post
title: "Import Your Main?"
date: 2008-09-11 08:46:21
link: http://www.edthedev.com/2008/import-your-main/
categories:
- Programming
published: true
comments: true
---
Chris Siebenmann says that, in [Python](http://www.python.org/), [your main program should be importable](http://utcc.utoronto.ca/~cks/space/blog/python/ImportableMain). He makes an interesting case for it; and is kind enough to provide example code layout.

I'll give this a try and get back to you on whether it revolutionized my coding lifestyle.

- Edward