---
layout: post
title: "External Hard Drive Backup Solution"
date: 2008-02-27 11:27:10
link: http://www.edthedev.com/2008/OLD116/
published: true
comments: true
---
I just got my new external hard drive case.
![Picture of the case](http://edthedev.com/files/exhd.jpg)


As you can see from the picture above, it's very nice looking.

Stay tuned. I will update this article with code, tools and instructions for my complete backup solution - as soon as it is finished.