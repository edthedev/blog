---
layout: post
title: "National Treasure II"
date: 2007-12-25 20:47:16
link: http://www.edthedev.com/2007/OLD90/
published: true
comments: true
---
We just got back from seeing [National Treasure II][1].
[1]: http://www.imdb.com/title/tt0465234/ "IMDB - National Treasure: Book of Secrets"
  
![NT2 Poster](http://edthedev.com/files/National-Treasure-2-Poster.jpg)

My Recommendation is go see it.

Sure it's an action movie, but the humor is what you'll remember. [Justin Bartha][2] makes his role much more fun than anticipated.
[2]: http://www.imdb.com/name/nm0058581/ "Justin Bartha at IMBD"
The words 'National Treasure' are in the title and [Jerry Bruckheimer][3] is in the credits so come with your suspension of disbelief prepared for overdrive. And wear your anti-glare glasses because our five stars don't know how to keep their flashlights pointed away from the camera. Other than than, the bulk of this film is fully watchable and continuously entertaining.
[3]: http://www.imdb.com/company/co0026281/ "JB at IMDB"

Oh, and don't get too excited about _page 47_ - it smells like a lame setup for a sequel.