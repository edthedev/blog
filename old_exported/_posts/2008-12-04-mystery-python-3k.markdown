---
layout: post
title: "Mystery Python 3k"
date: 2008-12-04 09:00:43
link: http://www.edthedev.com/?p=10377
categories:
- Random Geekiness
published: false
comments: true
---
Python finally made the jump to the big 3.0.

According to [Version Numbers Explained](http://www.elsop.com/wrc/humor/prog_ver.htm), this is the "Hey, we finally got it right!" release.

I read [Python 3.0 What's New](http://docs.python.org/dev/3.0/whatsnew/3.0.html), and I feel positive about this.

The switch to Binary and String data being two different things (and causing a TypeError when mixed up) is a stroke of genius. As professional programmers, we see things the way things really are. The Python guys are reminding us that we can use abstraction to make things work the way they should, instead of the way they tend to. In a virtual world, water can flow uphill. Kudos to the Python team - Python is giving us a lot to look forward to.

Speaking of which, [Lean Development Methods](http://www.eweek.com/index2.php?option=content&task=view&id=50677&pop=1&hide_ads=1&page=0&hide_js=1) looks like the next thing to look forward to. The link goes to a cool article predicting the future of software development. It's a very idealist view; but I joined the software business because it's a place where idealism yields results.

This new meme excites me, because my professional passion was already leaning in that direction. The question has filled my spare thoughts on several weekends: Can I build reliable tools that meet our team's most critical needs at blazing speeds? The default answer is no. Really quickly developed tools tend to be really insecure and failure prone. But as a community, we're building some excellent tools, and the dream is creeping into reach.

- Edward