---
layout: post
title: "Recent Commits January 7, 2012"
date: 2012-01-07 17:34:04
link: http://www.edthedev.com/2012/recent-commits-january-7-2012/
categories:
- Example Code
published: true
comments: false
---
<ul class="scrd_digest">
- [Automagic merge-fail.](https://bitbucket.org/edthedev/edthedev/changeset/7f46e5aa4217)
<div>Automagic merge-fail.
  
  
  
  
  
  
  - [
  config/builddotfiles.py
  ](https://bitbucket.org/edthedev/edthedev/src/7f46e5aa4217/config/builddotfiles.py)(83 lines added, 0 lines removed)
  
  </div>

- [Added a script that pulls a website and converts it into a Kindle document, then mails it to Kindle.](https://bitbucket.org/edthedev/edthedev/changeset/b718e76bfd22)
<div>Added a script that pulls a website and converts it into a Kindle document, then mails it to Kindle.
  
  
  
  
  
  
  - [
  kindle/website2kindle
  ](https://bitbucket.org/edthedev/edthedev/src/b718e76bfd22/kindle/website2kindle)(19 lines added, 0 lines removed)
  
  
  - [
  vim/hardcore_mode
  ](https://bitbucket.org/edthedev/edthedev/src/b718e76bfd22/vim/hardcore_mode)(0 lines added, 8 lines removed)
  
  </div>

- [Automerged..](https://bitbucket.org/edthedev/edthedev/changeset/a3c6c71efd7d)
<div>Automerged..
  
  
  
  
  
  
  - [
  config/cleanup-all-the-downloads
  ](https://bitbucket.org/edthedev/edthedev/src/a3c6c71efd7d/config/cleanup-all-the-downloads)(7 lines added, 0 lines removed)
  
  
  - [
  config/roku
  ](https://bitbucket.org/edthedev/edthedev/src/a3c6c71efd7d/config/roku)(22 lines added, 5 lines removed)
  
  
  - [
  org/aliases
  ](https://bitbucket.org/edthedev/edthedev/src/a3c6c71efd7d/org/aliases)(30 lines added, 0 lines removed)
  
  
  - [
  org/logit
  ](https://bitbucket.org/edthedev/edthedev/src/a3c6c71efd7d/org/logit)(51 lines added, 0 lines removed)
  
  
  - [
  org/orglib.pyc
  ](https://bitbucket.org/edthedev/edthedev/src/a3c6c71efd7d/org/orglib.pyc)(0 lines added, 0 lines removed)
  
  
  - [
  text/send-to-wordpress
  ](https://bitbucket.org/edthedev/edthedev/src/a3c6c71efd7d/text/send-to-wordpress)(54 lines added, 0 lines removed)
  
  
  - [
  vimrc_edthedev
  ](https://bitbucket.org/edthedev/edthedev/src/a3c6c71efd7d/vimrc_edthedev)(5 lines added, 0 lines removed)
  
  </div>

- [Made lock executable.](https://bitbucket.org/edthedev/edthedev/changeset/04278df12b1f)
<div>Made lock executable.
  
  
  
  
  
  
  - [
  config/lock
  ](https://bitbucket.org/edthedev/edthedev/src/04278df12b1f/config/lock)(0 lines added, 0 lines removed)
  
  </div>

- [Added script that links my dotfiles from my SSL-ed private Mercurial repository into my home directory.
Latest script update: It detects when the local hostname is in a file and links appropriately.
i.e. .muttrc-delaport-desktop will link to ~/.muttrc when the script is run on a host named delaport-desktop.](https://bitbucket.org/edthedev/edthedev/changeset/28b5d8eb860c)
<div>Added script that links my dotfiles from my SSL-ed private Mercurial repository into my home directory.
Latest script update: It detects when the local hostname is in a file and links appropriately.
i.e. .muttrc-delaport-desktop will link to ~/.muttrc when the script is run on a host named delaport-desktop.
  
  
  
  
  
  
  - [
  config/builddotfiles.py
  ](https://bitbucket.org/edthedev/edthedev/src/28b5d8eb860c/config/builddotfiles.py)(83 lines added, 0 lines removed)
  
  </div>

- [Updated logit to use orglib for all functionality.](https://bitbucket.org/edthedev/edthedev/changeset/114a8022a067)
<div>Updated logit to use orglib for all functionality.
  
  
  
  
  
  
  - [
  org/logit
  ](https://bitbucket.org/edthedev/edthedev/src/114a8022a067/org/logit)(8 lines added, 10 lines removed)
  
  </div>

- [Added simplest remind functionality back to public repository.](https://bitbucket.org/edthedev/edthedev/changeset/2ba7d5972fd8)
<div>Added simplest remind functionality back to public repository.
  
  
  
  
  
  
  - [
  org/aliases
  ](https://bitbucket.org/edthedev/edthedev/src/2ba7d5972fd8/org/aliases)(30 lines added, 0 lines removed)
  
  
  - [
  org/logit
  ](https://bitbucket.org/edthedev/edthedev/src/2ba7d5972fd8/org/logit)(53 lines added, 0 lines removed)
  
  
  - [
  org/orglib.pyc
  ](https://bitbucket.org/edthedev/edthedev/src/2ba7d5972fd8/org/orglib.pyc)(0 lines added, 0 lines removed)
  
  </div>

- [automerge yay](https://bitbucket.org/edthedev/edthedev/changeset/3b633fa231a6)
<div>automerge yay
  
  
  
  
  
  
  - [
  coding/svn-ignore
  ](https://bitbucket.org/edthedev/edthedev/src/3b633fa231a6/coding/svn-ignore)(6 lines added, 0 lines removed)
  
  
  - [
  coding/svn-st-all
  ](https://bitbucket.org/edthedev/edthedev/src/3b633fa231a6/coding/svn-st-all)(37 lines added, 0 lines removed)
  
  
  - [
  concept/keyring.py
  ](https://bitbucket.org/edthedev/edthedev/src/3b633fa231a6/concept/keyring.py)(38 lines added, 0 lines removed)
  
  
  - [
  config/cleanup-all-the-downloads
  ](https://bitbucket.org/edthedev/edthedev/src/3b633fa231a6/config/cleanup-all-the-downloads)(0 lines added, 0 lines removed)
  
  
  - [
  config/lock
  ](https://bitbucket.org/edthedev/edthedev/src/3b633fa231a6/config/lock)(3 lines added, 0 lines removed)
  
  
  - [
  edthedev/org.py
  ](https://bitbucket.org/edthedev/edthedev/src/3b633fa231a6/edthedev/org.py)(914 lines added, 0 lines removed)
  
  
  - [
  text/list-questions
  ](https://bitbucket.org/edthedev/edthedev/src/3b633fa231a6/text/list-questions)(39 lines added, 0 lines removed)
  
  
  - [
  text/make_header
  ](https://bitbucket.org/edthedev/edthedev/src/3b633fa231a6/text/make_header)(23 lines added, 13 lines removed)
  
  
  - [
  vimrc_edthedev
  ](https://bitbucket.org/edthedev/edthedev/src/3b633fa231a6/vimrc_edthedev)(15 lines added, 1 lines removed)
  
  </div>

- [Added script for wordpress file.](https://bitbucket.org/edthedev/edthedev/changeset/75e619dc4238)
<div>Added script for wordpress file.
  
  
  
  
  
  
  - [
  text/send-to-wordpress
  ](https://bitbucket.org/edthedev/edthedev/src/75e619dc4238/text/send-to-wordpress)(12 lines added, 3 lines removed)
  
  
  - [
  vimrc_edthedev
  ](https://bitbucket.org/edthedev/edthedev/src/75e619dc4238/vimrc_edthedev)(5 lines added, 0 lines removed)
  
  </div>


Digest powered by [RSS Digest](http://www.rssdigestpro.com)
