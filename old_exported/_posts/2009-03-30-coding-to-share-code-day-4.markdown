---
layout: post
title: "Coding to Share Code - Day 4"
date: 2009-03-30 00:40:51
link: http://www.edthedev.com/?p=10480
categories:
- Example Code
- Coding to Share Code
- Programming
published: false
comments: true
---
Thanks to the success of [Day 3](http://www.edthedev.com/2009/coding-to-share-code-day-3/) I like the way my pages look. Good looks are nothing unless they're backed by substance. And by 'substance' I mean 'data'. 

[CodeIgniter](http://codeigniter.com) shares the [PHP](http://php.net) pedigree of being very 'easy-going'. It doesn't bother to try to load your database until you specifically ask it to. It was nice to get to play with views right away, but it feels a little silly not to have anywhere to store data.

I created a database on [Day 1](http://www.edthedev.com/2009/coding-to-share-code-day-1) but I didn't connect anything to it; so it isn't doing anyone any good yet. 

To tell CodeIgniter that I want it to load my database, I updated line 42 of '/system/application/config/autoload.php' from:


$autoload['libraries'] = array();


to:


$autoload['libraries'] = array('database');


Now when I reload my test site, it gives an error message. It cannot find the database. This is quite natural, since I haven't told CodeIgniter where to look for the database.

Next I updated '/system/application/config/database.php' to tell it the host name, database name, user name, and password for my database. I created set database password to something long and meaningless, since I only had to tell it once to MySQL and once to CodeIgniter, and then I could forget it forever.

Once all that was done, I reloaded my test page, and the error message went away. If I had not done this in two steps I would have no idea whether it worked!

So after all that work, my application can find it's database. We're finished with Day 4, and the application still doesn't do anything useful. A building with only a frame is a lovely spectacle, even thought it cannot keep rain off of your head.