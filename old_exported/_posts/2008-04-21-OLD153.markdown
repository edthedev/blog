---
layout: post
title: "CSS Needs a Reset"
date: 2008-04-21 09:29:49
link: http://www.edthedev.com/2008/OLD153/
published: true
comments: true
---
[Meyerweb](http://meyerweb.com/) has a [great CSS style-sheet](http://meyerweb.com/eric/tools/css/reset/) for you to try. It's totally blank - that is, it not adding style, just removing inconsistencies. If it works as described, this is a great starting point for good styles.