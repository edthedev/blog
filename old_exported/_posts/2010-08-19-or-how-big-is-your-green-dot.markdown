---
layout: post
title: "Or how loud is your green dot?"
date: 2010-08-19 08:25:42
link: http://www.edthedev.com/2010/or-how-big-is-your-green-dot/
categories:
- Solutions
published: true
comments: false
---
Seth Godin asks [How big is your red zone?](http://sethgodin.typepad.com/seths_blog/2010/08/how-big-is-your-red-zone.html)

I like this article, because one of my passions in life is striving to be a really loud green dot.*

* Yea, I had to wait a quarter of a lifetime to say that; and yea, it was worth the wait.