---
layout: post
title: "colorLine"
date: 2010-02-21 18:09:40
link: http://www.edthedev.com/2010/colorline/
categories:
- Example Code
published: true
comments: true
---
Why? Because it's awesome.

<code>
from turtle import *
def colorLine(distance, colors):
...  for x in range(0, distance, 1):
...  color(colors[x % len(colors)])
...  forward(20)
>>> colorLine(400, ["red", "blue", "purple", "pink", "white"])
</code>