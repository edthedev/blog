---
layout: post
title: "$180 million per year scam software company"
date: 2010-03-25 19:24:49
link: http://www.edthedev.com/2010/180-million-per-year-scam-software-company/
categories:
- Security
published: true
comments: true
---
A company earned $180 million in a year by publishing scare-ware scam software. 
From the article, this company had a human resources department, a dedicated IT team, and a full call center.
Evil software is now a large and profitable business: http://www.reuters.com/article/idUSTRE62N29T20100324
As this trend continues, it's going to get harder to tell the online bad guys apart from legitimate businesses. Maybe the public will finally learn how to use the trust technologies that have been available for many years.