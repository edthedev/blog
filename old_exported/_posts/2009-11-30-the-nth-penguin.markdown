---
layout: post
title: "The Nth Penguin"
date: 2009-11-30 18:02:23
link: http://www.edthedev.com/2009/the-nth-penguin/
categories:
- Security
- Random Geekiness
published: true
comments: true
---
<DL>
<DT>
Idiom: The early bird gets the worm.
<DD>Meaning: Many opportunities are just waiting for the first person to come along and claim them.
<DT>
Idiom: The first penguin is the one that gets eaten.
<DD>Meaning: Be careful; sometimes there's a reason everyone else is waiting.
<DT>
Idiom: The last penguin doesn't get any shrimp.
<DD>Meaning: If you're too careful, you're going to miss out on the thing you wanted.
<DT>
Idiom: Be the second penguin.
<DD>Meaning: For a good opportunity, find someone who did it wrong and improve on their method.

<DT>Idiom: Be the (n+1)th penguin, where n is the predicted number of seals.
<DD>Meaning: Even the second penguin can get eaten.

<DT>Idiom: Be the ((prob.nearby * prob.hungry * num.seals)+1)th penguin.
<DD>Meaning: Math can be handy, if you do it quickly enough that you haven't already missed all of the shrimp and quite a bit of the tobogganing and other penguin shenanigans.
</DL>