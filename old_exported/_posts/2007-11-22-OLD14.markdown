---
layout: post
title: "Edit your PowerShell profile"
date: 2007-11-22 09:23:19
link: http://www.edthedev.com/2007/OLD14/
published: true
comments: true
---
1. At your powershell prompt, type:
<code>
notepad.exe $profile
</code>

2. Add whatever you want to your PowerShell profile.
<code>
function Hello-World ( )
{
 print "Hello World!"
}
</code>
Any PowerShell command you put in this file will run every time you start PowerShell.
The [prompt function](http://www.boyandpanda.com/?q=node/36) that I taught you to create runs <i>between</i> every command you enter.
Any other function that you create here will be available for you to call any time from your PowerShell prompt!

3. Save the file.
Ctrl+S or Menu - File - Save

4. You can re-open your PowerShell profile any time you want by typing
<code>
notepad.exe $profile
</code>
at your PowerShell prompt.

5. It doesn't work! I wrote a function, saved my profile, and I can't use my function until I close and restart PowerShell! 

Yea, sorry. Your profile only runs once when you start PowerShell.
But wait! There's more!
Run this command to re-run your profile (making PowerShell aware of that cool function you just wrote).
<code>
.$profile
</code>
Yes, that's a period at the beginning.

PS Where is my profile anyways? I want to email a copy to a friend, or make a backup, or something clever like that.
You can display the full path to your profile by typing:
<code>
print $profile
</code>
Better yet, let's open Windows File Explorer at the directory of our profile.
<code>
explorer.exe (get-item $profile).DirectoryName
</code>