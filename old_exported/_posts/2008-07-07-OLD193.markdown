---
layout: post
title: "It is still not okay to make your entire website in Flash"
date: 2008-07-07 09:11:10
link: http://www.edthedev.com/2008/OLD193/
published: true
comments: true
---
[Bruce Clay](Bruceclay.com) reminds us that [Flash websites are still a bad idea](http://www.bruceclay.com/blog/archives/2008/07/dont_build_your_site_in_flash.html).