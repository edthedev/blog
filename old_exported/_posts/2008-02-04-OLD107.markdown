---
layout: post
title: "Update your Google Calendar through Launchy"
date: 2008-02-04 15:17:34
link: http://www.edthedev.com/2008/OLD107/
published: true
comments: true
---
1. Install [Launchy][2]
[2]: http://www.launchy.net/
1. Install [Python][3]
[3]: http://www.python.org/ftp/python/2.5.1/python-2.5.1.msi
2. Install the Google Calendar Python API from [http://code.google.com/p/gdata-python-client/][1]
[1]: http://code.google.com/p/gdata-python-client/
  * Extract the zip file contents into C:\Python25\Lib\gdata
  * Open a command prompt in that folder.
  * Run this command: python setup.py install

4. Create a new python file at C:\scripts\python\gcalquickadd.py
and put the following code in it. Update the information below with your Gmail account where is currently says 'youremail@gmail.com'.

<code>
#!/usr/bin/python

# Created by Edward Delaporte
# http://www.edthedev.com
#!/usr/bin/python
import gdata.calendar.service
import gdata.service
import atom.service
import gdata.calendar
import sys
import getpass

yourEmail = 'youremail@gmail.com'
client = gdata.calendar.service.CalendarService()
client.email = yourEmail
client.password = getpass.getpass()
client.source = 'Google-Calendar_Python_Sample-1.0'
client.ProgrammaticLogin()
entry = sys.argv[1:]
content = ' '.join(entry)
event = gdata.calendar.CalendarEventEntry()
event.content = atom.Content(text=content)
event.quick_add = gdata.calendar.QuickAdd(value='true');

new_event = client.InsertEvent(event, '/calendar/feeds/default/private/full')
</code>


5. Open your Launchy configuration options; and add the following command to the Runner plugin:

<code>
Name: gcal
Program: C:\scripts\python\gcalquickadd.py
Arguments: $$
</code>


6. Give it a try! In launchy, you can type gcal, hit tab, and enter your calendar item according to Google's 'Quick-Add' format (i.e. 'Dinner at 5pm'). A command window will pop up and prompt you to enter your Google password. Enter your password (it will be hidden as you type), then go check your Google Calendar to see that your agenda item was added properly.