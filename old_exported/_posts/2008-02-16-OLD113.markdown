---
layout: post
title: "How to Install Python on Windows"
date: 2008-02-16 19:30:31
link: http://www.edthedev.com/2008/OLD113/
published: true
comments: true
---
1. Download and run the [Python Windows Installer][1] from [http://www.python.org/download/][2]
[1]:http://www.python.org/ftp/python/2.5.2/python-2.5.2.msi
[2]: http://www.python.org/download/
2. Add the Python home directory to your PATH environment variable.


Right click 'My Computer'
Select 'Properties'
Choose the 'Advanced' tab
The bottom panel is named 'System Variables'
In the list below 'System Variables', select 'Path'
Press 'Edit'
The 'Edit System Vaiable' dialog will appear.
Copy this text:
C:\Python25; 
And paste it at the beginning of the 'variable value'. Make sure it appears there in addition to whatever was there before.
The final value of 'variable value' should be at least:
c:\ruby\bin;C:\WINDOWS\system32;C:\WINDOWS;C:\WINDOWS\System32\Wbem;C:\WINDOWS\system32\WindowsPowerShell\v1.0;
And may be much longer.
Press 'Ok' buttons until you are back at your Desktop.

