---
layout: post
title: "Mozilla protects you from serious Java vulnerabilities"
date: 2010-04-22 05:22:23
link: http://www.edthedev.com/2010/mozilla-protects-you-from-serious-java-vulnerabilities/
categories:
- Security
published: true
comments: true
---
If you think your internet browsing experience is safer just because you're using Mozilla Firefox, (and you have it set to update itself automatically); then you're absolutely correct:
http://blogs.pcmag.com/securitywatch/2010/04/mozilla_disables_insecure_java.php

Disabling another system's 'feature' is always a debatable move. But as someone who watches the vulnerability reports from Secunia.com, I applaud Mozilla's decision to fix what Sun/Oracle hasn't been willing to fix.

Java's standard behavior is to leave the vulnerable Java version installed in case a (possibly malicious) website asks for it. Mozilla is now turning off access to all but the latest installed version of Java. It's a breakdown in cooperation between the two organizations, but I approve anyway because it will drastically reduce drive by download virus infection rates.

Update: My buddy Rob points out that the specific vulnerability that spurred Mozilla to this action has already been patched by Oracle/Sun: 
http://java.sun.com/javase/6/webnotes/6u20.html

Update: It's probably also worth  clarifying for anyone who found the article too long to read - the actions of both Oracle and Mozilla are making your web browser safer without reducing it's feature set. So you win all around, if you're a Firefox user.