---
layout: post
title: "Molly&#039;s song"
date: 2010-08-07 21:25:12
link: http://www.edthedev.com/2010/mollys-song/
categories:
- Random Geekiness
published: true
comments: true
---

[Billion Dollar View](http://escapepod.org/2010/08/05/ep252-billion-dollar-view/) is another classic Escape Pod story. If you have any time for radio, check it out. Have some kleenex on hand though - it may cause eye leaks.
