---
layout: post
title: "Regular Expressions in .Net"
date: 2010-03-25 20:44:57
link: http://www.edthedev.com/2010/regular-expressions-in-net/
categories:
- Programming
published: true
comments: true
---
For once I'm not actually posting this just for my future reference - I use RegularExpressions.info so often that I think I could never forget it.

I'm sure you love that website as much as I do; but you might not realize that they have some great pages dedicated to individual languages, such as .Net: 
http://www.regular-expressions.info/dotnet.html

Update: The 'MaxSplits' argument of the 'Regex.Split()' function does not behave the way the argument of the same name performs in the function of the same name in Java and Python. User be warned.