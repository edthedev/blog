---
layout: post
title: "Reading - 7 Habits of Highly Effective People"
date: 2009-03-31 22:28:06
link: http://www.edthedev.com/2009/reading-7-habits-of-highly-effective-people/
categories:
- Random Geekiness
published: true
comments: true
---
I'm playing with my new [Amazon WordPress add-on](http://www.edthedev.com/2009/amazon-reloaded-for-wordpress/).

<a href="http://www.amazon.com/Habits-Highly-Effective-People/dp/0743269519%3FSubscriptionId%3D02E5W5871AJF7PMMMS82%26tag%3Dedthedev-20%26linkCode%3Dxm2%26camp%3D2025%26creative%3D165953%26creativeASIN%3D0743269519">{% img http://ecx.images-amazon.com/images/I/51WQECVJG4L._SL160_.jpg %}</a>
I'm currently reading 'The 7 Habits of Highly Effective People'. It's full of great advice, but don't expect to read it through in one sitting. It's a good pick-up and put-down book. I like reading it in ten minute intervals; taking time in between to internalize it's advice.