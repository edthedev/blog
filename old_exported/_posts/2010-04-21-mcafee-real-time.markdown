---
layout: post
title: "Watching McAfee&#039;s mess in real time."
date: 2010-04-21 12:02:13
link: http://www.edthedev.com/2010/mcafee-real-time/
categories:
- Random Geekiness
published: true
comments: true
---
What people are saying about McAfee today:
http://search.twitter.com/search?q=mcafee (those who can spell it)
http://search.twitter.com/search?q=mcaffee (those who cannot)

McAfee's Twitter account: http://twitter.com/mcafee
UPDATE: McAfee's response: http://siblog.mcafee.com/support/mcafee-response-on-current-false-positive-issue/

McAfee stock, how has it done today? http://www.google.com/finance?client=ob&q=NYSE:MFE

If you had shorted McAfee to the tune of $1700, you would have made back your $8 trade fee in about two hours.

McAfee vs Symantec stock: http://tinyurl.com/2635et5, watch how they trend together until this morning.

UPDATE: An article in SC Magazine (http://tinyurl.com/33comgg) has an aftermath response by McAfee, and some additional indication that CITES Security was the first to report the problem to McAfee.

From the article, here's McAfee's aftermath response:
“Mistakes happen. No excuses. The nearly 7,000 employees of McAfee are focused right now on two things, in this order. First, help our customers who have been affected by this issue get back to business as usual. And second, once that is done, make sure we put the processes in place so this never happens again."