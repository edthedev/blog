---
layout: post
title: "Coding to Share Code 5"
date: 2009-04-05 05:50:38
link: http://www.edthedev.com/?p=10523
categories:
- Coding to Share Code
published: false
comments: true
---
Since I decided to adopt a new style-sheet over at [Ed The Dev .com](http://edthedev.com), [Test. Ed The Dev .com](http://test.edthedev.com) no longer matches it. How do I fix this? By breaking it entirely. [Mike The Coder](http://mikethecoder.com/) (whose site name I applaud, of course) has created a sort of rapid development framework for CSS, called [BlueTrip](http://bluetrip.org/). So [Test. Ed The Dev .com](http://test.edthedev.com) will be changing appearance over the next few days.