---
layout: post
title: "Command Your Computer with Your Voice"
date: 2009-03-25 18:56:55
link: http://www.edthedev.com/2009/command-your-computer-with-your-voice/
categories:
- Solutions
published: true
comments: true
---
I downloaded the Windows Vista speech recognition update. It works surprisingly well; and it's the right price too - it's free. 

If you're using Windows Vista, you should can grab it and give it a try. [Download Vista Voice Recognition](http://www.microsoft.com/downloads/details.aspx?FamilyID=fad62198-220c-4717-b044-829ae4f7c125&displaylang=en)

Be sure to walk through the tutorial. It's the easiest way to learn to use it, and the software learns your voice as you work through it - so everyone wins.

Enjoy!

- Edward