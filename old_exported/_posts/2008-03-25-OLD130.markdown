---
layout: post
title: "Exploring WMI through PowerShell"
date: 2008-03-25 12:11:39
link: http://www.edthedev.com/2008/OLD130/
published: true
comments: true
---
WMI classes are typcially collections. Of the members of these collections, you typcially only want to work with one. 
Calling <code>Get-WMIObject $className</code> will give you every bit of information about every one of them.

To explore in a cleaner manner, add the following two functions to your PowerShell profile. This function will get just the names of the collection members. Use it to decide which collection member you would like to work with.


function List-WMI( [string] $class )
{
  $strComputer = "."
  $items = get-wmiobject -class $class -namespace "root\cimv2" -computername $strComputer
  foreach ($objItem in $items) {
  write-host $objItem.Name
  }  
}



Usage example:
<code>
List-WMI('Win32\_Desktop')
</code>
or 
<code>
List-WMI 'Win32\_NetworkAdapter'
</code>

Next, you can use this function to grab just the item from the collection that you want.


function Get-WMI-Named( [string] $class, [string] $name )
{
  return get-wmiobject -class $class -filter "Name='$name'"
}


Usage example:
<code>
Get-WMI-Named('Win32\_Desktop', 'Edward')
</code>

Now use the <code>Get-Member</code> function to learn what methods and properties the object you're examining has.


Get-WMI-Named('Win32\_Desktop', 'Edward') | Get-Member

