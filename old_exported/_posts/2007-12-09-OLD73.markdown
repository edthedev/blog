---
layout: post
title: "NinerPaint"
date: 2007-12-09 05:18:47
link: http://www.edthedev.com/2007/OLD73/
published: true
comments: true
---
[NinerPaint][1] is the best paint program available for the PalmOS. Here's a couple of things I created with it:

![An animation of a boy watching another boy float away from the ground.][2]
![A colorful abstract painting composed of several tangent circles.][3]


[1]: http://www.ninerpaint.com/gallery.html "The NinerPaint image gallery"
[2]: http://www.edthedev.com/files/lighter_than_air.gif "Lighter than Air"
[3]: http://www.edthedev.com/files/circles.bmp "Circles"