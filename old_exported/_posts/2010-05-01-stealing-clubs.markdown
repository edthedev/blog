---
layout: post
title: "Stealing clubs"
date: 2010-05-01 11:03:50
link: http://www.edthedev.com/2010/stealing-clubs/
categories:
- Random Geekiness
published: true
comments: true
---
[qt:http://www.edthedev.com/wp-content/uploads/2010/05/passing-pins.mov 210 170]
Here's a video of Ben and I stealing clubs. Much thanks to [my father ](http://www.valufoto.com/) for catching this on film with his iPhone. Hopefully we will have film of ourselves doing this same trick with knives sometime soon.

The clubs are [PX3 Sirius](http://www.yoyosam.com/servlet/the-4505/Juggling-Club-Higgins-Bros/Detail) and yes, they really are that crazily orange in real life.