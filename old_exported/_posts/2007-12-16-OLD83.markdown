---
layout: post
title: "Iron Man Theme Song"
date: 2007-12-16 09:59:39
link: http://www.edthedev.com/2007/OLD83/
published: true
comments: true
---
In honor of the new movie, I've decided to share my tribute to Iron man.
This is to be sung to the tune of 'Iron Man' by Black Sabbath

He's the Iron man,
does whatever an iron can.

Alone on the shelf, 
spends his time all by himself.

He wears iron pants,
maybe why he cannot dance.

Now he's venting steam,
works to press a perfect seam.

He will fold your shirt,
if you touch him it will hurt.