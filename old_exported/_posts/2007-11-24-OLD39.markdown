---
layout: post
title: "Controlling our allergies"
date: 2007-11-24 21:14:08
link: http://www.edthedev.com/2007/OLD39/
published: true
comments: true
---
In planning our new home together, my wife and I have started taking special care of our shared allergies.

On dust mites:
Here's a rather long winded article that I found helpful.
http://www.drgreene.org/body.cfm?id=21&action=detail&ref=1320

And here's a much shorter one:
http://www.checnet.org/healthehouse/education/top10-detail.asp?Top10_Cat_ID=18

Full disclosure: My father works for a company that makes one of the very best filtered vacuums available. I have one and love it dearly.

- Edward