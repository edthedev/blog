---
layout: post
title: "How (and Why) to Sign Your Code"
date: 2007-12-20 11:42:07
link: http://www.edthedev.com/2007/OLD87/
published: true
comments: true
---
The nuts and bolts of code signing aren't taught well by most available references. Here's a few that do a decent job.

[This][1] is a rare gem on MSDN - an article that actually covers it's subject matter in detail.

[Here][2] is another nice article on the subject.

[1]: http://msdn2.microsoft.com/en-us/library/ms537361.aspx#Digital_Signatures "Microsoft Article on Code Signing"

[2]: http://www.pantaray.com/signcode.html "Code Signing Walkthough"