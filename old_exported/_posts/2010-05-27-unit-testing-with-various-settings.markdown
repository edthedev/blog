---
layout: post
title: "Unit testing with various settings"
date: 2010-05-27 09:15:02
link: http://www.edthedev.com/?p=11012
categories:
- Random Geekiness
published: false
comments: true
---
#!/usr/bin/python
# Python Version 2.5

__author__ = "Edward Delaporte, Research Programmer, University of Illinois"
__copyright__ = "Copyright (C) 2010 University of Illinois. All rights reserved."
__date__ = "$Date: 2010-03-05 15:53:00 -0600 (Fri, 05 Mar 2010) $"
__license__ = "University of Illinois Open Source License"
__version__ = "$Revision$"
__url__ = "$URL$"
__last_change__ = "$Author: delaport $"

import unittest
from edu.illinois.cites.security.ldap import LDAPConnector, ADQuery, LDAPEntry, ADEntry 

class Test(unittest.TestCase):

  
  def __init__(self, testname, testSettings):
  super(TestJira, self).__init__(testname)
  self.user = testSettings['user']
  self.jira_password = testSettings['password']

  def setUp(self):
  pass

  def tearDown(self):
  pass

  def testStudentType(self):
  netId = 'delaport'
  ldap_connection = LDAPConnector.LDAPConnector(ldap_url, ldap_user, ldap_password)
  personFinder = ADQuery.ADQuery()
  
  
  personFinder.netId = netId
  
  person = ldap_connection.performQuery(personFinder)  
  
  studentIsGradStudent = person.IsGradStudent
  
  assertEquals(studentIsGradStudent, False)

if __name__ == '__main__':
  
  # Test settings
  password = getpass.getpass('LDAP password: ')
  settings = {'user':'delaport',
  'password':password,
  }
  
  # Build test suite.
  suite = unittest.TestSuite()
  suite.addTest(TestJira("testStudentType", settings))
  
  # Run test suite.
  unittest.TextTestRunner(verbosity=5).run(suite)