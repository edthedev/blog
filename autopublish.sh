# Copy all new content in.
cp -R /home/vagrant/Dropbox/notes/publish/* /home/vagrant/blog/content/

# Save all new content.
cd /home/vagrant/blog/
git commit -a -m 'Closing out blog changes.'
git push

# Build website
mkdir -p ~/blog/output
make html

# Publish website
rsync output/* uiucedward@cairo.dreamhost.com:~/edward.delaporte.us/ --recursive --progress
rsync images/* uiucedward@cairo.dreamhost.com:~/edward.delaporte.us/images/ --recursive --progress
