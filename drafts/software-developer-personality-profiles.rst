software developer personality profiles
=======================================
:date: 2013-09-30

Software developer individual programming styles can be divided up along many criteria, and sometimes, it can be amusing and professionally useful, to figure out just which way a programmer you are working with leans.

Light and Dark
---------------
Let's start with fundamentals: The Light Side and the Dark Side.

Follow the Light:
    Strives to create software that reliably implements the requested specification.
    Create unit tests that actually pass.
    Tends to lobby for stronger testing and implementation processes.
    May provide an installer package that actually, once in awhile, works.
    Sincerely believes that the next project will be simpler for everyone.*

Turn to the Dark Side:
    Throws out the spec, and crams together something that meets the underlying need in a suprisingly clever way that even the original developer won't be able to safely make changes to, even just a few hours later.
    Answers the phone when you call at 2 am on Sunday morning.
    Shoulder-surfed  your production password a long time ago, and is willing to use it to help out in a pinch.
    Sincerely believes that the next project will be simpler for everyone.*

Both approaches are called for in certain situations, and both lead, as does all software, inevitably to regret.

* Unfounded optimism is a core trait of all practicing software developers.
