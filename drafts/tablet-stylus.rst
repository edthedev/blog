Testing my Pogo with my ViewSonic G Android Tablet.
===================================================

:date: 2012-11-11
:tags: android, art
:author: Edward Delaporte

Pogo styles is much too short. I fixed this with a chopstick and some electrical tape. I have patented the idea - the idea qualifies for a patent because it is non-obvious. If it was obvious, someone would have made a stylus that fits in the hand and costs less than $30. Hm...maybe someone else patented it already, and charges $20 as a license fee.

    ..image: hack_stylus.jpg

    ..image: brush_test.jpg

Either the Pogo is quite bad, or my early generation Android tablet is quite bad. It's nothing near as nice as working with an iPad. I should probably try my new stylus on the iPad and report back. (Previous tests of the Pogo on iPad were stopped short due to hand cramps.)
