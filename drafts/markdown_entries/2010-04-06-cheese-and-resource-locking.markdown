---
layout: post
title: "Cheese and Resource Locking"
date: 2010-04-06 20:13:01
link: http://www.edthedev.com/2010/cheese-and-resource-locking/
categories:
- Programming
published: true
comments: true
---
Although I too am a fan of really good resource locking algorithms; I suspect that this gentleman's attempt to get his wife excited about them is just going to lead to her being a little less quick to fix the printer for him next time he needs help. The article is probably funny to anyone, and it's hilarious if you've ever had to implement or fix a resource locking algorithm.
http://reprog.wordpress.com/2010/03/30/a-brief-yet-helpful-lesson-on-elementary-resource-locking-strategy/