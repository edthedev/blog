---
layout: post
title: "How to build a stadium in iPhone Simci"
date: 2010-07-03 11:30:57
link: http://www.edthedev.com/2010/how-to-build-a-stadium-in-iphone-simcity/
categories:
- Random Geekiness
published: true
comments: true
---
There's a bug in the iPhone version of Simcity; it's simple but very annoying.

The problem is that the *'Citizens demand a stadium'* news item is much more frequent than the* 'Here's a stadium for you to accept' * petition.

What's the difference you may ask? Well, **without finding the 'petition' version, you can never build a stadium**.

Normally that's not a problem, because for most special buildings, the news item is either attached to the related 'petition' or alternates with the petition often enough that you'll come across the 'petition' version in the news ticker eventually. But for some reason **the 'build a stadium petition' never appears in your news feed**.

So if you are tired of your citizens endlessly requesting an *impossible * stadium, follow these instructios:

1. Click the '...' button on your map, then select the 'advisors' button (it has a speech bubble); on the 'advisors' page, click the 'petitioners' button (it has a clipboard). You should now be on the 'Petitioners' page.

2. Now scroll through your many open petitions until you find one with the word 'stadium*' in it. On the petition itself click the 'Accept (thumbs up)' button.

3. Now go to your city buildings menu and select 'Gifts (wrapped present)' and then scroll back and forth until you see a huge $75,000 stadium. 

Now you can finally <del>shut those stupid citizens up</del> satisfy those voters with their stadium! 

*If someone will comment with the exact wording of the petition title, I will gladly update this article.