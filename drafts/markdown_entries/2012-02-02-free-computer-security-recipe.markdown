---
layout: post
title: "Free Computer Security Recipe"
date: 2012-02-02 18:20:32
link: http://www.edthedev.com/2012/free-computer-security-recipe/
categories:
- Random Geekiness
published: true
comments: false
---
Due to my background in computer security, people ask me - "What should I do to protect my computer?" 

That's not actually true. People ask me "What should I do to remove the virus on my computer?" I wish they would ask me the first question.

Here's the answer to the first question. Everything on this list is free.

- Don't use any music sharing applications. Music sharing applications such as LimeWire are a top source of virus infection. Just don't use them.

- Install [McAfee SiteAdvisor][1] - It's free, and it provides dramatically more protection than any other single piece of software. Malicious websites are another common source of infection. SiteAdvisor protects you. /About the author: McAfee provides the AV for my workplace. They do a decent job, too./ 

- [Mozilla Firefox][2] - It's free, and it's much safer than Internet Explorer.

- [Gmail][3] - Email is the #3 source of virus infection. Gmail scans every incoming email for viruses.

- Install [Ubuntu][4], Apple OSX, Windows Vista or Windows 7. Install any operating system that is not Windows XP. Some viruses literally just call up your computer and ask it whether the computer is stupid enough to install the virus directly. Windows XP falls for this attack the most often, and is the most common target of this attack. Every virus knows many ways to break into a Windows XP computer.

- Turn on [Windows automatic updates][5]. Viruses are getting smarter every hour. Your computer needs to keep up.

[1] http://www.siteadvisor.com/ "McAfee SiteAdvisor"
[2] http://www.mozilla.org/en-US/firefox/new/ "Mozilla Firefox"
[3] https://mail.google.com "Google Mail"
[4] http://www.ubuntu.com/ubuntu/why-use-ubuntu "Why use Ubuntu?"
[5] http://windows.microsoft.com/en-US/windows/help/windows-update "Help with Windows Automatic Updates"