---
layout: post
title: "The Dangers of Federation Consoles"
date: 2007-12-17 20:03:38
link: http://www.edthedev.com/2007/OLD84/
published: true
comments: true
---
I'm compiling [this list][1] of the many fatalities that have been associated with command consoles exploding in science fiction, particularly Star Trek. This is my civic duty to influence our world for the better.

Please login and comment here if you have an incident to add to the list as it grows.

[1]: http://edthedev.com/doku/doku.php?id=public:fatal_accidents_with_federation_consoles "Accidents with Star Trek command consoles."