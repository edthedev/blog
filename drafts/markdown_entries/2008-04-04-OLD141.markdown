---
layout: post
title: "What&#039;s most important in Web Design"
date: 2008-04-04 08:47:35
link: http://www.edthedev.com/2008/OLD141/
published: true
comments: true
---
[Web Design From Scratch][1] has [this great article][2] that suggests which elements of a web page should be most strongly presented, according to what the web page reader is more interested in noticing first.

[1]: http://www.webdesignfromscratch.com
[2]: http://www.webdesignfromscratch.com/attention_map.cfm

It's a handy guide. I intend to check it next time I need to make decisions about the presentations of those page elements.

- Edward