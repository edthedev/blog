---
layout: post
title: "Gmail bookmarklet"
date: 2009-11-12 17:30:52
link: http://www.edthedev.com/2009/gmail-bookmarklet/
categories:
- Example Code
- Solutions
published: true
comments: true
---
[Bookmarklets](http://en.wikipedia.org/wiki/Bookmarklet) are amazing.
And few are as awesome as the 'Gmail This' bookmarklet.
Drag the link below to your toolbar to start enjoying the awesomeness:

[Gmail this](http://mail.google.com/mail/?ui=1&view=cm&fs=1&tf=1&to=&su=)