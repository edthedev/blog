---
layout: post
title: "OpenID on WordPress"
date: 2010-02-26 23:00:53
link: http://www.edthedev.com/2010/openid-on-wordpress/
categories:
- Solutions
published: true
comments: true
---
I've been very amused by [OpenID](http://openid.net/get-an-openid/) for awhile now, but haven't had a chance to use it. 

The animation project that Ben (of [Ben-writes.com](http://ben-writes.com)) and I are working on was the perfect excuse to [try out OpenID](http://boynpanda.com/2010/use-openid-to-login-and-post-comments).

So if you want to post comments over at [boynpanda.com](http://boynpanda.com), try using your OpenID!