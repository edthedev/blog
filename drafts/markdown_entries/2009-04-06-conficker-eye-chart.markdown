---
layout: post
title: "Conficker Eye Chart"
date: 2009-04-06 15:16:03
link: http://www.edthedev.com/2009/conficker-eye-chart/
categories:
- Random Geekiness
published: true
comments: true
---
This is so cool. You may remember that I showed you [How to check for conficker](http://www.edthedev.com/2009/how-to-check-for-conficker/). Now an even easier (and just plain cool) way is available. Check out the [Conficker Eye Chart](http://www.cites.illinois.edu/security/antivirus/conficker_eyechart.html). To make sure it works properly, you should clear your browser cache before visiting. You can thank [Joe Stewart](http://www.joestewart.org) for the innovation.