---
layout: post
title: "Readme driven development"
date: 2010-08-23 13:49:30
link: http://www.edthedev.com/2010/readme-driven-development/
categories:
- Programming
published: true
comments: false
---
Tom Preston-Werner advocates for [Readme Driven Development](http://tom.preston-werner.com/2010/08/23/readme-driven-development.html).

Points worth highlighting: 

"If you're working with a team of developers you get even more mileage out of your Readme. If everyone else on the team has access to this information before you've completed the project, then they can confidently start work on other projects that will interface with your code. Without any sort of defined interface, you have to code in serial or face reimplementing large portions of code."

And most importantly, "It's a lot simpler to have a discussion based on something written down. It's easy to talk endlessly and in circles about a problem if nothing is ever put to text. The simple act of writing down a proposed solution means everyone has a concrete idea that can be argued about and iterated upon."