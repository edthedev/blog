---
layout: post
title: "Where to live agile"
date: 2009-02-23 11:15:36
link: http://www.edthedev.com/?p=10433
categories:
- Random Geekiness
published: false
comments: true
---
I was playing with [Google Trends](http://www.google.com/trends), and I tried the search term 'Agile Development'.
The most surprising thing I saw was where people where searching from:

1. 	Bangalore, India	
2. 	San Francisco, CA, USA	
3. 	Washington, DC, USA	
4. 	Sydney, Australia	
5. 	London, United Kingdom	
6. 	Chicago, IL, USA	
7. 	Toronto, Canada	
8. 	Atlanta, GA, USA	
9. 	New York, NY, USA	
10. 	Mumbai, India

California and India leading the charts on a software development term is no surprise; but I was surprised that Washington, Sydney and London all outranked Chicago.

I'm off to try other geek terms and see which city wins the prize for being most geek-curious.