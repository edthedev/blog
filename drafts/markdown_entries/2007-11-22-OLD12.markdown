---
layout: post
title: "Create a web link using HTML"
date: 2007-11-22 09:21:56
link: http://www.edthedev.com/2007/OLD12/
published: true
comments: true
---
The internet link is one of the most amazing inventions of our time. Thank you Tim Berners-Lee.

Let's learn to use it.
<code>
<a href="http://www.boyandpanda.com/?q=node/30">Edward's Wedding Photos</a>
</code>
[Edward's Wedding Photos](http://www.boyandpanda.com/?q=node/30)

As always, feel free to post questions as a 'comment', and I'll answer them and update this tutorial to be more clear.