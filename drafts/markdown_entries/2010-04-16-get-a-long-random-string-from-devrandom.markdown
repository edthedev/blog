---
layout: post
title: "Get a long random string from /dev/random"
date: 2010-04-16 10:58:24
link: http://www.edthedev.com/2010/get-a-long-random-string-from-devrandom/
categories:
- Example Code
- Solutions
published: true
comments: true
---
Use word count to make sure you're getting the length you expected.
{% blockquote %}<code>cat /dev/urandom| tr -dc 'a-zA-Z0-9-_!@#$%^&*()_+{}|:<>?=' | fold -w 1048 | head -n1 | wc -m
</code>{% endblockquote %}
And use this to actually fetch the long string.
{% blockquote %}<code>cat /dev/urandom| tr -dc 'a-zA-Z0-9-_!@#$%^&*()_+{}|:<>?=' | fold -w 1048 | head -n1 | wc -m</code>{% endblockquote %}
Here's a much simpler example, in case you just want a quick four digit number:
{% blockquote %}cat /dev/urandom| tr -dc '0-9' | fold -w 4 | head -n1{% endblockquote %}