---
layout: post
title: "Get rid of the annoying Mac boot up sound"
date: 2009-12-13 20:55:42
link: http://www.edthedev.com/2009/get-rid-of-the-annoying-mac-boot-up-sound/
categories:
- Solutions
published: true
comments: true
---
Macintosh is a lot nicer than Windows in a lot of ways. Making stupid noises while booting up is, unfortunately, a throw-back to the worst that Windows has to offer.

If you ever need to boot your computer during a meeting or in a library, grab the Mac StartupSound.prefPane freeware over at [Arcana Research - Software & Download](http://www5e.biglobe.ne.jp/~arcana/software.en.html).