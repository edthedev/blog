---
layout: post
title: "Google on Design Dreams"
date: 2008-05-01 08:25:03
link: http://www.edthedev.com/2008/OLD167/
published: true
comments: true
---
While somewhat lacking in actual advice, [this article](http://googleblog.blogspot.com/2008/04/what-makes-design-googley.html) could be handy as a reference for Web Developers making tough choices about interfaces.

Enjoy!
- Edward