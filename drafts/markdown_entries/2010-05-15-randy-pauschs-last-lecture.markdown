---
layout: post
title: "Cool People: Randy Pausch"
date: 2010-05-15 00:10:12
link: http://www.edthedev.com/2010/randy-pauschs-last-lecture/
categories:
- Random Geekiness
published: true
comments: true
---
"Wait long enough, and people will surprise and impress you. If you're angry at them, you haven't given them enough time." - Jon Snoddy

Quoted from [Randy Pausch's last lecture](http://www.youtube.com/watch?v=ji5_MqicxSo).