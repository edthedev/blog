---
layout: post
title: "Insanely useful Django tips"
date: 2009-01-17 06:28:52
link: http://www.edthedev.com/2009/insanely-useful-django-tips/
categories:
- Programming
published: true
comments: true
---
[Django](http://www.djangoproject.com/) is quickly becoming my favorite [rapid application development](http://searchsoftwarequality.techtarget.com/sDefinition/0,,sid92_gci214246,00.html) environment for its practicality and its security. [Glen Stansberry](http://webjackalope.com) has written a very aptly named article, [10 Insanely Useful Django Tips](http://nettuts.com/web-roundups/10-insanely-useful-django-tips/). Be sure to have a notepad ready to take notes when you read it.