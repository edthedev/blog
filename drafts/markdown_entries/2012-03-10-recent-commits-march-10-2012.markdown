---
layout: post
title: "Recent Commits March 10, 2012"
date: 2012-03-10 17:36:21
link: http://www.edthedev.com/2012/recent-commits-march-10-2012/
categories:
- Example Code
published: true
comments: false
---
<ul class="scrd_digest">
- [forgot the end. oops.](https://bitbucket.org/edthedev/edthedev/changeset/37c199364fdb)
<div>forgot the end. oops.
  
  
  
  
  
  
  - [
  minecraft/publish-map
  ](https://bitbucket.org/edthedev/edthedev/src/37c199364fdb/minecraft/publish-map)(7 lines added, 4 lines removed)
  
  </div>

- [Much nicer publish script - now with loop](https://bitbucket.org/edthedev/edthedev/changeset/7a2cce9b70dd)
<div>Much nicer publish script - now with loop
  
  
  
  
  
  
  - [
  minecraft/publish-map
  ](https://bitbucket.org/edthedev/edthedev/src/7a2cce9b70dd/minecraft/publish-map)(23 lines added, 11 lines removed)
  
  </div>

- [Fix to not leave extra tgz files lying around.](https://bitbucket.org/edthedev/edthedev/changeset/06309b65bb8c)
<div>Fix to not leave extra tgz files lying around.
  
  
  
  
  
  
  - [
  minecraft/cron.example
  ](https://bitbucket.org/edthedev/edthedev/src/06309b65bb8c/minecraft/cron.example)(1 lines added, 1 lines removed)
  
  
  - [
  minecraft/publish-map
  ](https://bitbucket.org/edthedev/edthedev/src/06309b65bb8c/minecraft/publish-map)(4 lines added, 2 lines removed)
  
  </div>

- [Fixed publish map](https://bitbucket.org/edthedev/edthedev/changeset/e2746db057df)
<div>Fixed publish map
  
  
  
  
  
  
  - [
  minecraft/backup-worlds-local
  ](https://bitbucket.org/edthedev/edthedev/src/e2746db057df/minecraft/backup-worlds-local)(6 lines added, 4 lines removed)
  
  
  - [
  minecraft/publish-map
  ](https://bitbucket.org/edthedev/edthedev/src/e2746db057df/minecraft/publish-map)(3 lines added, 2 lines removed)
  
  </div>


Digest powered by [RSS Digest](http://www.rssdigestpro.com)
