---
layout: post
title: "Coding to Share Code - Day 3"
date: 2009-03-30 00:40:42
link: http://www.edthedev.com/?p=10475
categories:
- Example Code
- Coding to Share Code
- Programming
- Random Geekiness
published: false
comments: true
---
Since [the work area is ready](http://www.edthedev.com/2009/coding-to-share-code-day-3coding-to-share-code-day-2/), I decided to start experimenting with [CodeIgniter](http://codeigniter.com).

When you're learning something new, be sure to start fiddling in an area that gives you quick and thorough feedback.

If you're programming, that means experimenting with the user interface first. So I started by creating some [CodeIgniter Views](http://codeigniter.com/user_guide/general/views.html).

I modified '/system/application/views/welcome_message.php'. As expected, the welcome test changed. But I noticed something I didn't like: welcome_message.php contained the entire HTML for the welcome message. So if I decided to add some 'EdTheDev' branding, I might have to copy and paste it into every future view. Branding shouldn't be critical, but it was important to me, and turned out to be interesting to do.

I dug around a bit in the [CodeIgniter Documentation](http://codeigniter.com/user_guide/) and found that CodeIgniter does headers and footers without much fuss.

In '/system/application/views/', I created 
header.php and footer.php. Someday I might put clever PHP code in them; but today they're just plain HTML. I put a couple of links in the footer, and I included my EdTheDev.com CSS theme in the header.

Then I updated my welcome_message.php file to be the following:


<?php echo $this->load->view('footer'); ?>
This is my welcome page.
<?php echo $this->load->view('footer'); ?>



Now my CodeIgniter welcome page looks like the rest of my website, and so will every future CodeIgniter view I write.