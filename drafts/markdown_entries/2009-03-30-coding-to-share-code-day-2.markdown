---
layout: post
title: "Coding to Share Code - Day 2"
date: 2009-03-30 00:40:29
link: http://www.edthedev.com/?p=10474
categories:
- Example Code
- Coding to Share Code
- Programming
- Random Geekiness
published: false
comments: true
---
I installed [CodeIgniter](http://codeigniter.com) simply by uploading the PHP source. [PHP](http://php.net) has a terrible security track record, but people use it because often [it just works](http://c2.com/cgi/wiki?ItJustWorks). This was one of those times.

Installing my [MySQL](http://mysql.com) test database was a dream, thanks to [Dreamhost](http://dreamhost.com)'s web panel tools. There are times when they definitely live up to their name.

I visit [test.EdTheDev.com](http://test.edthedev.com), and I see the CodeIgniter welcome message.

This is the software equivalent of a ground-breaking. Nothing useful has been done yet, but at least we know we have a place set aside to work.