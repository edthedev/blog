---
layout: post
title: "Dybbuk in the bottle"
date: 2009-10-12 18:34:56
link: http://www.edthedev.com/2009/dybbuk-in-the-bottle/
categories:
- Random Geekiness
published: true
comments: true
---
I've been really enjoying listening to the free fiction audio podcasts over at Podcastle and EscapePod.

I'm posting a link to this story because I'm certain to bring it up in conversation sometime, and this way one of us can just surf over to EdTheDev.com to dig up the link.

'Dybbuk in the bottle' is a fable about a a contest between a Jewish farmer and a demon. The ending is just as satisfying as any fable you have ever heard; but it's the quality of the storytelling along the way that really sets it apart.  

http://podcastle.org/2009/09/18/podcastle-070-the-dybbuk-in-the-bottle/