---
layout: post
title: "Why abstract object factories suck"
date: 2010-03-20 21:18:39
link: http://www.edthedev.com/2010/why-abstract-object-factories-suck/
categories:
- Programming
published: true
comments: true
---
Following up on how much I love Java, here's an article about what I hate about Java (and it's quite funny):
http://soren.overgaard.org/writings/why-frameworks-suck/