---
layout: post
title: "&#039;Privacy&#039; is not the same as &#039;secret&#039;."
date: 2010-04-06 20:08:08
link: http://www.edthedev.com/2010/privacy-is-not-the-same-as-secret/
categories:
- Security
published: true
comments: true
---
Bruce Schneier says that keeping things private is not the same thing as keeping them secret.

http://www.schneier.com/blog/archives/2010/04/privacy_and_con.html