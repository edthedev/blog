---
layout: post
title: "Developer Tool Roundup"
date: 2012-01-28 17:29:39
link: http://www.edthedev.com/2012/tools/
categories:
- Random Geekiness
published: true
comments: false
---
I'm training new colleagues at work, so my mind has been reviewing all the tools that make the job easier. As long as they're on my mind, I may as well share them.

Balsamiq for Mockups
--------------------

[Balsamiq][1] is a very fast interface mock-up tool that has been making my life easier lately. 

I could talk your ear off about it's features, but your time would probably be better spent [trying their live web demo][2].

Umlet for Flow Diagrams
-----------------------
I feel that I've tried everything for making flow diagrams. I once thought I hated doing application flow diagrams; but when I met [Umlet][3], I learned that I just hate spending more time doing flow diagrams than is necessary.

There are other nice things about Umlet: It's free, it runs everywhere and it uses an open file format. But I'd be lying if I said than any of that was as important as the speed. Umlet is really, really fast.

Vim for Editing
----------------
[Vim][4] is incredibly difficult to learn, but the speed makes up for it. It does everything any other editor can do, but with twice the learning curve and a tenth the time. If you're only doing a bit of light editing, give it a pass. But if you're going to spend a significant amount of time editing, learning Vim pays off in spades.

Developer Toolbar for Testing
-----------------------------
[Developer Toolbar][5] is another tool that no one gives up once they've discovered it. You'll learn it faster by just installing it than you would by reading about it here. Suffice to say, it tells you *everything* you could want to know about a webpage. It's also now [available for Google Chrome][6]

[1]: http://www.balsamiq.com/ "Balsamiq Mockups"
[2]: http://builds.balsamiq.com/b/mockups-web-demo/ "Balsamiq Demo"
[3]: http://www.umlet.com/ "Umlet"
[4]: http://vim.org/ "Vim"
[5]: https://addons.mozilla.org/en-US/firefox/addon/web-developer/
"Web Developer Toolbar for Firefox"
[6]: https://chrome.google.com/webstore/detail/bfbameneiokkgbdmiekhjnmfkcnldhhm "Web Developer Toolbar for Chrome"