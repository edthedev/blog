---
layout: post
title: "The Changing Fate of Piracy"
date: 2009-03-27 16:11:38
link: http://www.edthedev.com/2009/the-changing-fate-of-piracy/
categories:
- Security
- Solutions
published: true
comments: true
---
If you use or know anyone who uses BitTorrent or Limewire, Yahoo Tech News has [an article you should read](http://tech.yahoo.com/news/ap/20090326/ap_on_hi_te/tec_at_t_internet_piracy).

The recording industry claims that they're just in it to educate users. While I doubt the purity of their motives; I believe that the more each consumer knows, the safer they will be. The RIAA still recently stuck by their $150,000 penalty for a single song download, so there is a lot at stake. 

I recommend giving the article a quick read. It will help you become familiar with the issues, and with why and how piracy is changing. As you read, consider how much you trust your ISP. I encourage you to trust your service provider, but trust them appropriately by knowing their policies.