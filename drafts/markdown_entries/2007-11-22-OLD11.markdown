---
layout: post
title: "Browsing the Windows Certificates Store"
date: 2007-11-22 09:19:55
link: http://www.edthedev.com/2007/OLD11/
published: true
comments: true
---
From the Start Menu, select 'Run'; or open a PowerShell prompt or another Windows console.

Type
<code>mmc</code> and hit enter.

The Microsoft Management Console will open.

If you have never used it before, it's basically empty. It needs to be told what we want it to manage. In this case, we will ask it to let us see our installed certificates.

From the File menu, select 'Add/Remove Snap-in...'.

On the 'Standalone' tab, press the 'Add' button.

Select the 'Certificates' option, and press 'Add'.

Select 'My User acccount' and press 'Finish'.

Now close the 'Add/Remove Snap-in' window to return to the main Microsoft Mangement Console window.

From the tree at the left, select Certificates, and browse into the 'Personal' directory. 

You can double-click a certificate in the list at the right to see it's details.