---
layout: post
title: "There Are No Winners"
date: 2008-02-23 05:56:08
link: http://www.edthedev.com/2008/OLD114/
published: true
comments: true
---
[Webkinz][1] appears to have a server down, and shows the children this very unfortunate error message.

[1]: webkinz.com

![There are no winners](http://edthedev.com/files/therearenowinners.png)

Now the children know, "There are no winners".