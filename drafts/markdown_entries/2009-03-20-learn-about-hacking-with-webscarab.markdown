---
layout: post
title: "Learn About Hacking with WebScarab"
date: 2009-03-20 08:02:50
link: http://www.edthedev.com/2009/learn-about-hacking-with-webscarab/
categories:
- Security
published: true
comments: true
---
I'm working with some really cool computer security open source tools, and I know some of you will be interested, so I posted a tutorial.

WebScarab is a tool for testing the security of your own web applications, and can teach you a great deal about how the internet works.

Before you try it, you need to know one of the white-hat mantras:

Don't use these tools on any website other than your own. It is legally equivalent to trying to break someone's window, and you might succeed.

But I'm not sharing this just to annoy you; you really can use the tools safely and legally. OWasp provides a vulnerable website that you can run from computer, called WebGoat. So you can run and 'attack' WebGoat in order to learn WebScarab.

So why learn WebScarab? Because you'll learn a ton about how HTTP works, how web-servers work, and internet security. Even if you don't plan to be a professional geek, it'll make  you more savvy and safer online.

The instructions at the [OWasp WebScarab Project](http://www.owasp.org/index.php/Category:OWASP_WebScarab_Project) leave a lot to be desired; so I'll break it down for you.

Get WebScarab from http://dawes.za.net/rogan/webscarab/webscarab-current.zip.
Extract the zip. Run WebScarab by double clicking the .jar file.

Run Firefox. Configure Webscarab as your Firefox proxy by setting: Tools - Options - Advanced - Connection - Settings - Proxy - LocalHost, 8008

Get WebGoat from http://code.google.com/p/webgoat/.
Extract the zip. 
Disconnect from the internet (Your computer is very vulnerable while running WebGoat).
Run WebGoat by double-clicking the .bat file.

Visit: http://localhost/WebGoat/attack

Login with:
user name: webgoat
password: webgoat

You will see a brief 'What is WebGoat' page.
Press the 'Start WebGoat' button at the bottom.

Now you will see a page with tutorials that you can walk through with WebGoat and WebScarab. These pages will tell you most of what you need to know.

The YGN Ethical Hacking Group has WebGoat solution videos:
http://yehg.net/lab/pr0js/training/webgoat.php

And here are some sites they use as part of their solutions: 
http://h4k.in/encoding/

Enjoy!