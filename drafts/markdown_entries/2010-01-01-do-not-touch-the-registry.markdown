---
layout: post
title: "Do not touch the registry"
date: 2010-01-01 17:50:28
link: http://www.edthedev.com/2010/do-not-touch-the-registry/
categories:
- Random Geekiness
published: true
comments: true
---
I had to look up this gem to share it with someone again. Thanks to Microsoft's 'The Scripting Guys' for summarizing the standard warning about touching the Windows registry:

"Warning: Don’t ever change a value in the registry. Ever. We know we just told you to do that, but would you jump off a cliff if we told you to? Don’t ever change a value in the registry. Don’t even say the word registry. We know a guy once who said the word registry, and three days later he was hit by a bus. True story. As a matter of fact, you shouldn’t even have a registry on your computer. If you suspect that you do have a registry on your computer, call us and a trained professional will be dispatched to your office to remove the registry immediately. If you accidentally touch the registry, wash your hands with soap and water and call a doctor. Do not swallow the registry or get it in your eyes!"

from http://technet.microsoft.com/en-us/scriptcenter/dd939960.aspx