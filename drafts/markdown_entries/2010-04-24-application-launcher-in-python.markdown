---
layout: post
title: "Application launcher in Python"
date: 2010-04-24 07:32:53
link: http://www.edthedev.com/2010/application-launcher-in-python/
categories:
- Example Code
published: true
comments: true
---
I've been playing with writing application and python tool launchers to turn my EeePC into some kind of freakishly powerful custom PDA. The source code for my first two proofs-of-concept are included below. 

<del datetime="2011-08-08T15:03:22+00:00">You can grab the latest source at [http://svn.edthedev.com/projects/eeePy/](http://svn.edthedev.com/projects/eeePy/).</del>
UPDATE: I've migrated my open source projects to the awesome folks at BitBucket. https://bitbucket.org/edthedev/edthedev/src/b00613f8f7d9/Launcher/

### cLauncher.py


Curses application launcher




<code>
#!/usr/bin/env python

# Copyright Edward Delaporte 2010
# URI: http://edthedev.com

# Licensed under the Eclipse Public License v. 1.0
# http://www.opensource.org/licenses/eclipse-1.0.php

from os import system
from FavoriteApps import FavoriteApps
from cWindow import CursesWindow
from ClassInspector import ClassInspector

def runMethod(screen, method, historyFile=None):
  args = ClassInspector.getMethodArguments(FavoriteApps, method)
  if(len(args)==0):
  # Run the command.
  getattr(FavoriteApps, method)()
  else:
  argInputs = {}
  for arg in args:
  argInputs[arg]= getInput(screen, "Enter argument for %s" % arg)
  # Run the command with arguments.  
  getattr(FavoriteApps, method)(**argInputs)


myClasses = {'FavoriteApps':FavoriteApps}

window = CursesWindow()

selection = ''
while selection != 'exit':
  window.displayList(myClasses, 'Available classes:')
  selection = window.getInput("Choose a class: ")
  for className in myClasses:
  if selection in className:
  classObj = ClassInspector(myClasses[className])

  command = ''
  while command != 'exit':
  window.displayList(classObj.methods, 'Available commands:')
  # window.drawMethodMenu(classObj.methods)
  command = window.getInput("Enter a command: ") 
  for method in classObj.methods:
  if command in method:
  window.output("Running %s" % method)
  runMethod(window.screen, method)

del window
</code>




### ClassInspector.py


<code>
import inspect 

class ClassInspector(object):
  def __init__(self, classObj):
  self.classObj = classObj
  self.methods = self.getMethodsInClass(self.classObj)
  self.methodArgs = {}
  #for method in self.methods:
  #  self.methodsArgs[method] = self.getMethodArguments(self.classObj, method)

  @staticmethod
  def getMethodsInClass(classObj):
  results = []
  for name in dir(classObj):
  obj = getattr(classObj, name)
  if (inspect.ismethod(obj) or inspect.isfunction(obj)):
  results.append(name)
  return results

  @staticmethod
  def getMethodArguments(classObj, methodName):
  method = getattr(classObj, methodName)
  argDetails = inspect.getargspec(method)
  (args, _, _, defaults) = argDetails
  return args
</code>


UPDATE: I updated it to use the curses (plain text) library rather than requiring tkinter (graphics). If you're running Linux or Apple OSX you have curses installed already. If you're running Windows, you can install NCurses but may need to tweak the code a little. You can still find the tkinter version in SVN repository.