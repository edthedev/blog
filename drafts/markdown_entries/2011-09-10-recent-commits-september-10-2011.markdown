---
layout: post
title: "Recent Commits September 10, 2011"
date: 2011-09-10 17:30:08
link: http://www.edthedev.com/2011/recent-commits-september-10-2011/
categories:
- Example Code
published: false
comments: false
---
<ul class="scrd_digest">
- [Remapped DONE to /d](https://bitbucket.org/edthedev/edthedev/changeset/904c69fd00d9)
<div>Remapped DONE to /d</div>

- [Added support for converting @project tagged files to .org files.
Part of migrating toward an Emacs org mode friendly system; even though I may never use Emacs org mode...](https://bitbucket.org/edthedev/edthedev/changeset/748a8a569041)
<div>Added support for converting @project tagged files to .org files.
Part of migrating toward an Emacs org mode friendly system; even though I may never use Emacs org mode...</div>

- [Inbox script improvements.](https://bitbucket.org/edthedev/edthedev/changeset/4c646aaa4756)
<div>Inbox script improvements.</div>

- [Better use of shortcuts](https://bitbucket.org/edthedev/edthedev/changeset/890adfb6ccb8)
<div>Better use of shortcuts</div>

- [Bugfix](https://bitbucket.org/edthedev/edthedev/changeset/ab31c35e88d9)
<div>Bugfix</div>

- [Automerge](https://bitbucket.org/edthedev/edthedev/changeset/031e09c4f2a0)
<div>Automerge</div>

- [shorter next output
better inbox label (corrected)](https://bitbucket.org/edthedev/edthedev/changeset/c596b23bdc64)
<div>shorter next output
better inbox label (corrected)</div>

- [Much better minecraft client backup script. Woot.](https://bitbucket.org/edthedev/edthedev/changeset/dd053085eecc)
<div>Much better minecraft client backup script. Woot.</div>

- [Switched minecraft backup script to use rsync rather than cp](https://bitbucket.org/edthedev/edthedev/changeset/5d8e3d1aa8e4)
<div>Switched minecraft backup script to use rsync rather than cp</div>


Digest powered by [RSS Digest](http://www.rssdigestpro.com)
