---
layout: post
title: "My vimrc file"
date: 2010-05-29 13:40:40
link: http://www.edthedev.com/2010/my-vimrc-file/
categories:
- Solutions
published: true
comments: true
---


<code>
map! ^[OA ^[ka "For Solaris
map! ^[OB ^[ja
map! ^[OD ^[i
map! ^[OC ^[la
set nocompatible "Don't emulate old vi bugs.
set autoindent " Yay!
set smartindent
set tabstop=4
set shiftwidth=4
set expandtab
set showmatch "Parenthesis matching.
set vb t_vb= "Flash instead of beep.
set ruler
set nohls "Do not highlight search matches.
set incsearch "Search as I type
" Type :help options within vim to get a complete list of options.

set nowrap "Do not wrap long lines of code.
set textwidth=120
set foldmethod=marker

"Place all vim backup files into the user home directory.
set backupdir=~/.vimbackup
</code>

