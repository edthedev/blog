---
layout: post
title: "Stylin"
date: 2010-02-17 20:42:11
link: http://www.edthedev.com/?p=10677
categories:
- Random Geekiness
published: false
comments: true
---
<!-- /* this stylesheet is created by StrangeBanana (http://www.strangebanana.com)*/ body { background-color: rgb(85,171,75); color: rgb(215,149,223); font-family: Palatino, serif; border-left-width: 0px; border-right-width: 0px; border-top-width: 0px; border-bottom-width: 0px; border-left-style: none; border-right-style: none; border-top-style: solid; border-bottom-style: solid; border-color: rgb(198,158,203); }

#content { font-size: 96%; background-color: rgb(255,255,255); color: rgb(0,0,0); border-left-width: 0px; border-right-width: 0px; border-top-width: 0px; border-bottom-width: 0px; border-left-style: none; border-right-style: none; border-top-style: none; border-bottom-style: none; padding: 19px 19px 19px 19px; border-color: rgb(0,0,0); text-align: left; }

#menu { background-color: rgb(255,255,255); color: rgb(85,171,75); font-family: Verdana, Arial, Helvetica, sans-serif; font-style: normal; text-indent: 33; border-color: rgb(255,255,255); }

a.menuitem { font-size: 128%; background-color: rgb(0,0,0); color: rgb(125,216,114); font-variant: normal; text-transform: uppercase; font-weight: bold; margin-left: 5px; margin-right: 5px; margin-top: 5px; margin-bottom: 5px; border-left-width: 0px; border-right-width: 0px; border-top-width: 0px; border-bottom-width: 0px; border-left-style: none; border-right-style: none; border-top-style: none; border-bottom-style: none; border-color: rgb(167,221,161); text-align: center; }

#logo, #logo a { font-size: 158%; background-color: rgb(0,0,0); color: rgb(125,216,114); font-family: Palatino, serif; font-style: italic; font-variant: normal; text-transform: none; font-weight: bold; padding: 10px 10px 10px 10px; border-color: rgb(255,255,255); text-align: right; }

#trail, #trail a { font-size: 118%; background-color: rgb(0,0,0); color: rgb(215,149,223); font-style: normal; font-variant: normal; text-transform: none; font-weight: bold; border-left-width: 0px; border-right-width: 0px; border-top-width: 0px; border-bottom-width: 0px; border-left-style: none; border-right-style: none; border-top-style: none; border-bottom-style: none; padding: 14px 14px 14px 14px; border-color: rgb(0,0,0); text-align: center; }

#content a { color: rgb(85,171,75); text-decoration: none; }

#content a:hover, #content a:active { background-color: rgb(85,171,75); color: rgb(255,255,255); }

h1, h2, h3, h4, h5, h6 { font-family: Verdana, Arial, Helvetica, sans-serif; font-style: normal; font-variant: small-caps; text-transform: none; font-weight: normal; }

h1 { font-size: 127%; }

h2 { font-size: 115%; }

h3 { font-size: 103%; }

h4 { font-size: 91%; }

h5 { font-size: 79%; }

h6 { font-size: 67%; }

#menu a { text-decoration: none; }

.menuitem:hover { background-color: rgb(125,216,114); color: rgb(0,0,0); }

#trail a { text-decoration: none; }

#logo a { text-decoration: none; }

#trail a:hover { }

/* layout */ #menu		{width: 22%; position: relative; top: 0; left: 0; float: left; text-align: center;} .menuitem		{width: auto;} #content		{margin-left: 22%; width: auto} .menuitem		{display: block;}

@media print { #menu  {display: none;} #content  {padding: 0px;} #content a  {text-decoration: underline;} }

-->