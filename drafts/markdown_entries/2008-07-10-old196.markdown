---
layout: post
title: "Create Fast Flow Diagrams with Umlet"
date: 2008-07-10 12:33:41
link: http://www.edthedev.com/2008/old196/
categories:
- Programming
published: true
comments: true
---
[Umlet](http://www.umlet.com/) is the only flow diagram creation tool that I have used that follows the cardinal rule: It Just Works.

I'm very impressive with the way the interface teaches the user how to use it quickly and efficiently.

Enjoy!

- Edward