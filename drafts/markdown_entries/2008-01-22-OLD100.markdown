---
layout: post
title: "Chips on Credit"
date: 2008-01-22 12:34:03
link: http://www.edthedev.com/2008/OLD100/
published: true
comments: true
---
Is it economically impossible for me to get chips on credit? Vending machines do a marvelous job providing emergency chips to the hungry programmer (who may have encountered dip, embarking upon the [delicious cycle][1]). But alas, vending machines require hard currency, rather than processing digital credit - thus the delicious cycle is interrupted; reducing both producer and consumer satisfaction.

[1]: http://xkcd.com/140/ "The Delicious Cycle"