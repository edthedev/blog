---
layout: post
title: "A Better Thumb Drive Launcher"
date: 2008-12-02 20:58:07
link: http://www.edthedev.com/?p=10373
categories:
- Random Geekiness
published: false
comments: true
---
New recommendations:

PStart -- it's better than PortableApps Launcher, because it can add HTML files.

It still can't add web urls; which leads me to also recommend:

TiddlyWiki

Use a TiddlyWiki to manage your bookmarks. You can launch it from PStart; you can drag new links into the list; you can organize how your favorites are organized, tagged and presented; and it's fully searchable. Plus, you can choose from a bunch of cool looking TiddlyWiki skins.

Also, go grab EjectUSB - you can launch it from PStart, and it's far fewer clicks than the standard windows method of safely ejecting your USB drive.

Enjoy!

- Edward