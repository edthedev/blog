---
layout: post
title: "RSS Button Maker"
date: 2008-05-22 13:30:55
link: http://www.edthedev.com/2008/OLD180/
published: true
comments: true
---
This is pretty neat - a [web tool that creates RSS buttons](http://tools.dynamicdrive.com/button/)

It let me create this great image:
![RSS Feed](http://edthedev.com/files/rssfeed.gif)