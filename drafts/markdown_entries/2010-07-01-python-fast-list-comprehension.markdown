---
layout: post
title: "Python fast list comprehension"
date: 2010-07-01 09:49:00
link: http://www.edthedev.com/2010/python-fast-list-comprehension/
categories:
- Random Geekiness
published: true
comments: true
---
Here's a one line list comprehension that stops processing the list as soon as it finds a true member, and returns false if all members are false. Keep in mind that you could also use a lamba function to check for a more complex circumstance in the list.


<code>
mustBlock = next((True for rule in rules if rule.block == True), False)
</code>
