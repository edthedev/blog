---
layout: post
title: "Your data at Amazon?"
date: 2007-11-22 09:29:28
link: http://www.edthedev.com/2007/OLD17/
published: true
comments: true
---
Amazon now offers a data storage service exclusively to developers:
http://www.amazon.com/exec/obidos/tg/browse/-/16427261/

This comes to mind as a very cheap way to run household backups, if only someone would write an Open Source backup application that uses it. We'll see how the coming months use my time - I might come back to this idea (and if I do, I'll write the backup application).

- Edward