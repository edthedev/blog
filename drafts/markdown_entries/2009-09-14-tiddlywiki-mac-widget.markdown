---
layout: post
title: "TiddlyWiki Mac Widget"
date: 2009-09-14 17:25:07
link: http://www.edthedev.com/2009/tiddlywiki-mac-widget/
categories:
- Random Geekiness
published: true
comments: true
---
This makes me unspeakably happy. TiddlyWiki is the coolest way to track your random stray thoughts. It's a Wiki that runs entirely out of a flat file on your computer, in your browser.

Jonathan Paisley has taken TiddlyWiki to it's next step in awesomeness, by turning it into a Mac Widget.

You can download it form his website at:[ TiddlyWiki Things](http://www.jpaisley.com/dcs/tiddlywiki/).