---
layout: post
title: "Fix DokuWiki&#039;s default syntax"
date: 2008-03-17 15:49:48
link: http://www.edthedev.com/2008/OLD127/
published: true
comments: true
---
[DokuWiki][1] is a terrific wiki, except that it removes line breaks for no fathomable reason. Fortunately, there's a [plugin that fixes the problem][2].
[1]: http://wiki.splitbrain.org/wiki:dokuwiki
[2]: http://wiki.splitbrain.org/plugin:linebreak