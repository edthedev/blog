---
layout: post
title: "Oracle Create Table Syntax - A Clear Example"
date: 2008-01-04 13:24:56
link: http://www.edthedev.com/2008/OLD93/
published: true
comments: true
---
The internet seems to have a shortage of hands-on Oracle create table examples, so here's mine. It's a create script for an Oracle Database table that keeps track of 'Computers'.
I repeat each line with an explanation below. I hope you all find it helpful!

- Edward

Here's the script:
<code>
create table Computers (
Computer_ID number not null,
Owner_ID number not null,
Added_Date date not null,
Serial_Number varchar(128),
Our_Property number(1) not null,
Description varchar(255),
CONSTRAINT noDuplicateComputers UNIQUE (Computer_ID, Inventory_Tag)
)
</code>

And here's a the line by line details:
<code>Computer_ID number not null,</code>
Computer ID is the primary key for the table. That's why it's featured in this line as well: <code>CONSTRAINT noDuplicateComputers UNIQUE (Computer_ID, Inventory_Tag)</code>

<code>Owner_ID number not null,</code>
Owner ID is a foreign key, so it can't be null.

<code>Serial_Number varchar(128),</code>
Serial Number is a text string that, in our case, is a unique identifier for any computer that has one. Here's the line that makes it unique (you saw this above too): 
<code>CONSTRAINT noDuplicateComputers UNIQUE (Computer_ID, Inventory_Tag)</code>

<code>Added_Date date not null,</code>
It's a good practice to keep track of when data is entered. So this is a required field as well. I'll post how to make this field update automatically with a trigger later.

<code>Our_Property number(1) not null,</code>
Here's a nice example of a required boolean field.

<code>Description varchar(255),</code>
Ah yes, the catch-all 'Description' field that we all love so dearly.