---
layout: post
title: "Finding the MAC address on Android"
date: 2011-03-16 19:30:13
link: http://www.edthedev.com/2011/finding-the-mac-address-on-android/
categories:
- Solutions
published: true
comments: false
---
I needed to find the MAC address on my Motorola Droid phone in order to register with the [U of I Computer Registration anti-theft system](https://compreg.cites.illinois.edu).

I didn't really find specific instructions, so I'm posting some. I'm assuming that other Android phones will be similar.

1. Go to the home screen.
2. Hit the menu key.
3. Choose 'settings' on the menu that pops up.
4. Scroll all the way down and select 'About phone'.
5. Select 'status'.
6. You'll have to scroll down a bit again, but you should see 'Wi-Fi MAC address' in the list; along with other identifying information.