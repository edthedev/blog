---
layout: post
title: "My Favorite Thumb Drive Applications"
date: 2008-11-22 14:58:07
link: http://www.edthedev.com/2008/my-favorite-thumb-drive-applications/
categories:
- Solutions
published: true
comments: true
---
Last week, I shared how to convert a thumb drive into a [portable Django development environment](http://www.edthedev.com/2008/thumb-drive-dev-environment/). Since then, I have found my thumb drive useful for a lot more than just writing Python code.

So here's a list of my favorite apps for when I'm not writing code. As is typical for things I cover, these applications are all freeware.

Of course, you still probably want the [PortableApps Core](http://sourceforge.net/project/downloading.php?groupname=portableapps&filename=PortableApps.com_Platform_Setup_1.1.exe&use_mirror=voxel). Although I find [PStart](http://www.pegtop.net/start/) is very nice as well. Technically, you could even run both next to each-other. :)

Two obvious choices are [Portable Firefox](http://portableapps.com/news/2008-11-12_-_firefox_portable_3.0.4) and [Portable Thunderbird](http://portableapps.com/news/2008-11-21_-_thunderbird_portable_2.0.0.18_rev_2), but I honestly don't use either one. I find that both launch quite slowly from my thumb drive, and I typically have both a safe browser and a way to access my mail from every computer I use.

So I recommend ignoring the two most popular Mozilla applications. Skip to the surprise gem of the bunch: [Portable Sunbird](http://portableapps.com/apps/office/sunbird_portable). I'm too lazy to install and configure Sunbird on every computer I visit - it's just too much work. But now that I installed and configured it once on my thumbdrive, I rely on it a lot. If you only get two applications for your thumb drive, this should be it.

I said 'two' didn't I? That's because I believe that text editing is sacred - it's the most important thing we do with computers. So pick one of the following three: [Portable Notepad++](), [Portable GVim](), or [Portable AbiWord](http://portableapps.com/apps/office/abiword_portable). Abiword is the most like Microsoft Word, or WordPad, so it's the most logical choice for most people. If your normally prefer Notepad, then give Notepad++ a try. If you already love Gvim, click the link and enjoy. If you have never heard of Gvim, then you probably don't want it.

Now that we have covered the 'must have', let's talk about other recommendations I have.

[KeePass Portable](http://portableapps.com/apps/utilities/keepass_portable) is a great way to safely store your passwords. Just be sure to choose a _very_ strong master password if you choose to use it. This is a thumb drive, and the odds are that you will lose it someday.

I'm just trying this now myself, but [GnuCash Portable](http://portableapps.com/apps/office/gnucash_portable) looks like a great idea. For me, the hardest thing about budgeting, is that I never have my transaction logs, my budget software, and my free time at the same computer. Maybe GnuCash Portable can change that. Be careful how you use this - keeping financial records on a thumb drive is basically a bad idea. Be careful what information you store in this application. No one really cares how many times you have been to Burger King this week; but a thief would love to get your name and account numbers just by stealing your thumb drive. (So don't enter your real name or account numbers into the software.) I should also mention that this is the biggest download on the list, be prepared to let it take some time to install.

I won't strongly recommend most of the rest of what you will find at the [Portable Apps Applications List](http://portableapps.com/apps). A lot of it is very clever, but most of it isn't vital.