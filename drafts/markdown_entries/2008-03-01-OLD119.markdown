---
layout: post
title: "Bang! Variant: Ghost Town"
date: 2008-03-01 08:33:41
link: http://www.edthedev.com/2008/OLD119/
published: true
comments: true
---
[Bang!: Dodge City Card Game](http://www.amazon.com/gp/product/B0006HCW08?ie=UTF8&tag=edthedev-20&linkCode=as2&camp=1789&creative=9325&creativeASIN=B0006HCW08){% img http://www.assoc-amazon.com/e/ir?t=edthedev-20&l=as2&o=1&a=B0006HCW08 %} is a favorite game of my friends. Consequentially, we play it an awful lot, and have had time to come up with some amusing variant rules.

The first one is a required house rule:
Any player who takes on a Western Accent, or any other 'in character' affectation, and promises to maintain it for the rest of the game, may draw an extra card on their first turn. Absolutely try this rule. You'll have even your most shy friends doing their best to speak in a Western drawl. It turns an amusing game into a night of uproarious laughter that people will remember. The extra card on turn one has almost no effect on the game outcome, but don't tell any of your players that!

The second variant has a name: Ghost Town

Dead players become ghosts, and play their turns normally, but with the following changes:
Dead players become ghosts.
1. Obviously, a ghost can't shoot anyone. Ghosts are limited to doing spooky and annoying things, not killing people. The Renegade gets a one turn exception, (dramatic return from the dead) but we'll get to that later.
2. Ghosts play with normal distance rules, but are 'dead' as far as any other player's distance is concerned.
2. Ghosts may play Bang! cards as 'spooks'. A 'spook' may be played in response to someone else playing a card, that card is then discarded having no effect. Bang! cards played by 'living' players may not be 'spooked'. Beer cards, Miss cards, various equipment, and jail cards should get 'spooked' a lot. This rule will dramatically shorten the game once there are many ghosts.
3. Ghostbusting: Living players may play a 'bang' card against a ghost to force them to stop playing cards until the end of their next turn. The ghost may attempt to avoid the bang as normal to avoid the bang. If they cannot avoid the bang!, place it face up in front of them. While it is there, they cannot play cards. During their next turn, their only action will be to discard the bang.
4. Beer: Beer has many healing properties, but it cannot bring back the dead. A ghost has no use for Beer.

Winning: Ghosts count as dead for win conditions. The Sheriff will never get to play as a ghost, since his death ends the game. The Renegade must be alive at the end of the game to win. The deputy may win as a Ghost, so long as the Sheriff survives. The Outlaws may win even if after they all are ghosts.