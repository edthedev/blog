---
layout: post
title: "Freeing up space on Windows hard drives"
date: 2010-06-22 19:14:49
link: http://www.edthedev.com/2010/freeing-up-space-on-windows-hard-drives/
categories:
- Solutions
published: true
comments: true
---
[WinDirStat](http://windirstat.info/) is freeware for Windows that creates a visualization of the largest files on your hard drives. It's priceless for freeing up that extra gigabyte that you need to install that new game.

 
It found that my World of Warcraft installer files were wasting 8GB of
space, even though I had removed World of Warcraft, itself.

Also, there's a MAC version called [Disk Inventory X](http://windirstat.info/), which is also quite awesome.