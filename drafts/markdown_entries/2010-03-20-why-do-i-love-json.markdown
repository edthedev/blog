---
layout: post
title: "Why do I love JSON?"
date: 2010-03-20 21:20:20
link: http://www.edthedev.com/2010/why-do-i-love-json/
categories:
- Programming
published: true
comments: true
---
One of the things I loved in the Android plugin for Eclipse (and about Android in general), is the way XML can be used to manage parts of the application that are actually data. XML still sucks, but the Eclipse plugin keeps you from actually having to read XML unless you really wanted to.

That said, I would still rather be using JSON than XML. Here's an article that explains on of the reasons:
http://stereolambda.wordpress.com/2010/03/19/why-is-json-so-popular-developers-want-out-of-the-syntax-business/