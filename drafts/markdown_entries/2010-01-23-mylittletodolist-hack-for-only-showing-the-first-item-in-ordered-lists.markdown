---
layout: post
title: "MyLittleTodoList hack for only showing the first item in ordered lists"
date: 2010-01-23 17:47:02
link: http://www.edthedev.com/?p=10676
categories:
- Random Geekiness
published: false
comments: true
---
# My little hack...
  $q = $db->dq("SELECT title FROM {$db->prefix}todolist $inner WHERE title LIKE '_.%' $sqlWhere ORDER BY title ASC LIMIT 1");
  $r = $q->fetch_assoc($q);
  list($lowest_number, $task_text) = explode('.', $r['title'], 2);
  # $lowest_number = $r['title'];
  # $sqlWhere .= " AND title LIKE '$lowest_number' ";  
  $sqlWhere .= " AND title LIKE '$lowest_number.%' ";  
  # End of my hack...