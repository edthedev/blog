---
layout: post
title: "Recent Commits January 28, 2012"
date: 2012-01-28 17:33:44
link: http://www.edthedev.com/2012/recent-commits-january-28-2012/
categories:
- Example Code
published: true
comments: false
---
<ul class="scrd_digest">
- [Added SVG and PNG dice.](https://bitbucket.org/edthedev/edthedev/changeset/4363c2266b84)
<div>Added SVG and PNG dice.
  
  
  
  
  
  
  - [
  bottle/605px-Octahedron_svg.png
  ](https://bitbucket.org/edthedev/edthedev/src/4363c2266b84/bottle/605px-Octahedron_svg.png)(0 lines added, 0 lines removed)
  
  
  - [
  bottle/d10.png
  ](https://bitbucket.org/edthedev/edthedev/src/4363c2266b84/bottle/d10.png)(0 lines added, 0 lines removed)
  
  
  - [
  bottle/d10.svg
  ](https://bitbucket.org/edthedev/edthedev/src/4363c2266b84/bottle/d10.svg)(163 lines added, 138 lines removed)
  
  
  - [
  bottle/d12.png
  ](https://bitbucket.org/edthedev/edthedev/src/4363c2266b84/bottle/d12.png)(0 lines added, 0 lines removed)
  
  
  - [
  bottle/d12.svg
  ](https://bitbucket.org/edthedev/edthedev/src/4363c2266b84/bottle/d12.svg)(849 lines added, 127 lines removed)
  
  
  - [
  bottle/d20.png
  ](https://bitbucket.org/edthedev/edthedev/src/4363c2266b84/bottle/d20.png)(0 lines added, 0 lines removed)
  
  
  - [
  bottle/d20.svg
  ](https://bitbucket.org/edthedev/edthedev/src/4363c2266b84/bottle/d20.svg)(38 lines added, 125 lines removed)
  
  
  - [
  bottle/d4.png
  ](https://bitbucket.org/edthedev/edthedev/src/4363c2266b84/bottle/d4.png)(0 lines added, 0 lines removed)
  
  
  - [
  bottle/d4.svg
  ](https://bitbucket.org/edthedev/edthedev/src/4363c2266b84/bottle/d4.svg)(95 lines added, 134 lines removed)
  
  
  - [
  bottle/d6.png
  ](https://bitbucket.org/edthedev/edthedev/src/4363c2266b84/bottle/d6.png)(0 lines added, 0 lines removed)
  
  
  - [
  bottle/d6.svg
  ](https://bitbucket.org/edthedev/edthedev/src/4363c2266b84/bottle/d6.svg)(69 lines added, 131 lines removed)
  
  
  - [
  bottle/d8.png
  ](https://bitbucket.org/edthedev/edthedev/src/4363c2266b84/bottle/d8.png)(0 lines added, 0 lines removed)
  
  
  - [
  bottle/d8.svg
  ](https://bitbucket.org/edthedev/edthedev/src/4363c2266b84/bottle/d8.svg)(79 lines added, 133 lines removed)
  
  </div>

- [merge](https://bitbucket.org/edthedev/edthedev/changeset/17bdbfebea4f)
<div>merge
  
  
  
  
  
  
  - [
  bottle/bottle
  ](https://bitbucket.org/edthedev/edthedev/src/17bdbfebea4f/bottle/bottle)(1 lines added, 0 lines removed)
  
  
  - [
  bottle/bottle.wsgi
  ](https://bitbucket.org/edthedev/edthedev/src/17bdbfebea4f/bottle/bottle.wsgi)(10 lines added, 0 lines removed)
  
  
  - [
  bottle/cardgame.py
  ](https://bitbucket.org/edthedev/edthedev/src/17bdbfebea4f/bottle/cardgame.py)(122 lines added, 0 lines removed)
  
  
  - [
  bottle/d10.svg
  ](https://bitbucket.org/edthedev/edthedev/src/17bdbfebea4f/bottle/d10.svg)(177 lines added, 0 lines removed)
  
  
  - [
  bottle/d12.svg
  ](https://bitbucket.org/edthedev/edthedev/src/17bdbfebea4f/bottle/d12.svg)(177 lines added, 0 lines removed)
  
  
  - [
  bottle/d2.svg
  ](https://bitbucket.org/edthedev/edthedev/src/17bdbfebea4f/bottle/d2.svg)(177 lines added, 0 lines removed)
  
  
  - [
  bottle/d20.svg
  ](https://bitbucket.org/edthedev/edthedev/src/17bdbfebea4f/bottle/d20.svg)(177 lines added, 0 lines removed)
  
  
  - [
  bottle/d4.svg
  ](https://bitbucket.org/edthedev/edthedev/src/17bdbfebea4f/bottle/d4.svg)(177 lines added, 0 lines removed)
  
  
  - [
  bottle/d6.svg
  ](https://bitbucket.org/edthedev/edthedev/src/17bdbfebea4f/bottle/d6.svg)(177 lines added, 0 lines removed)
  
  
  - [
  bottle/d8.svg
  ](https://bitbucket.org/edthedev/edthedev/src/17bdbfebea4f/bottle/d8.svg)(177 lines added, 0 lines removed)
  
  
  - [
  bottle/index.py
  ](https://bitbucket.org/edthedev/edthedev/src/17bdbfebea4f/bottle/index.py)(122 lines added, 0 lines removed)
  
  
  - [
  bottle/roller.css
  ](https://bitbucket.org/edthedev/edthedev/src/17bdbfebea4f/bottle/roller.css)(39 lines added, 0 lines removed)
  
  
  - [
  bottle/roller.html
  ](https://bitbucket.org/edthedev/edthedev/src/17bdbfebea4f/bottle/roller.html)(18 lines added, 0 lines removed)
  
  
  - [
  bottle/roller.py
  ](https://bitbucket.org/edthedev/edthedev/src/17bdbfebea4f/bottle/roller.py)(193 lines added, 0 lines removed)
  
  
  - [
  bottle/setupbottle
  ](https://bitbucket.org/edthedev/edthedev/src/17bdbfebea4f/bottle/setupbottle)(8 lines added, 0 lines removed)
  
  
  - [
  bottle/starcraftii
  ](https://bitbucket.org/edthedev/edthedev/src/17bdbfebea4f/bottle/starcraftii)(1 lines added, 0 lines removed)
  
  
  - [
  bottle/targetgame
  ](https://bitbucket.org/edthedev/edthedev/src/17bdbfebea4f/bottle/targetgame)(77 lines added, 0 lines removed)
  
  
  - [
  vimrc_edthedev
  ](https://bitbucket.org/edthedev/edthedev/src/17bdbfebea4f/vimrc_edthedev)(3 lines added, 1 lines removed)
  
  </div>

- [Much nicer CSS.
Shows image with latest roll - regardless of player.](https://bitbucket.org/edthedev/edthedev/changeset/e3bfdf728525)
<div>Much nicer CSS.
Shows image with latest roll - regardless of player.
  
  
  
  
  
  
  - [
  bottle/d10.svg
  ](https://bitbucket.org/edthedev/edthedev/src/e3bfdf728525/bottle/d10.svg)(177 lines added, 0 lines removed)
  
  
  - [
  bottle/d12.svg
  ](https://bitbucket.org/edthedev/edthedev/src/e3bfdf728525/bottle/d12.svg)(177 lines added, 0 lines removed)
  
  
  - [
  bottle/d2.svg
  ](https://bitbucket.org/edthedev/edthedev/src/e3bfdf728525/bottle/d2.svg)(177 lines added, 0 lines removed)
  
  
  - [
  bottle/d20.svg
  ](https://bitbucket.org/edthedev/edthedev/src/e3bfdf728525/bottle/d20.svg)(177 lines added, 0 lines removed)
  
  
  - [
  bottle/d4.svg
  ](https://bitbucket.org/edthedev/edthedev/src/e3bfdf728525/bottle/d4.svg)(177 lines added, 0 lines removed)
  
  
  - [
  bottle/d6.svg
  ](https://bitbucket.org/edthedev/edthedev/src/e3bfdf728525/bottle/d6.svg)(177 lines added, 0 lines removed)
  
  
  - [
  bottle/d8.svg
  ](https://bitbucket.org/edthedev/edthedev/src/e3bfdf728525/bottle/d8.svg)(177 lines added, 0 lines removed)
  
  
  - [
  bottle/roller.css
  ](https://bitbucket.org/edthedev/edthedev/src/e3bfdf728525/bottle/roller.css)(34 lines added, 16 lines removed)
  
  
  - [
  bottle/roller.html
  ](https://bitbucket.org/edthedev/edthedev/src/e3bfdf728525/bottle/roller.html)(11 lines added, 7 lines removed)
  
  
  - [
  bottle/roller.py
  ](https://bitbucket.org/edthedev/edthedev/src/e3bfdf728525/bottle/roller.py)(27 lines added, 20 lines removed)
  
  
  - [
  bottle/static/d20.svg
  ](https://bitbucket.org/edthedev/edthedev/src/e3bfdf728525/bottle/static/d20.svg)(0 lines added, 177 lines removed)
  
  </div>

- [CSS attached to template.](https://bitbucket.org/edthedev/edthedev/changeset/9b8e91be7f68)
<div>CSS attached to template.
  
  
  
  
  
  
  - [
  bottle/d20.svg
  ](https://bitbucket.org/edthedev/edthedev/src/9b8e91be7f68/bottle/d20.svg)(0 lines added, 177 lines removed)
  
  
  - [
  bottle/roller.css
  ](https://bitbucket.org/edthedev/edthedev/src/9b8e91be7f68/bottle/roller.css)(21 lines added, 12 lines removed)
  
  
  - [
  bottle/roller.html
  ](https://bitbucket.org/edthedev/edthedev/src/9b8e91be7f68/bottle/roller.html)(2 lines added, 2 lines removed)
  
  
  - [
  bottle/static/d20.svg
  ](https://bitbucket.org/edthedev/edthedev/src/9b8e91be7f68/bottle/static/d20.svg)(177 lines added, 0 lines removed)
  
  </div>

- [Split code, style and templates..](https://bitbucket.org/edthedev/edthedev/changeset/890af0670da9)
<div>Split code, style and templates..
  
  
  
  
  
  
  - [
  bottle/bottle
  ](https://bitbucket.org/edthedev/edthedev/src/890af0670da9/bottle/bottle)(1 lines added, 1 lines removed)
  
  
  - [
  bottle/bottle.wsgi
  ](https://bitbucket.org/edthedev/edthedev/src/890af0670da9/bottle/bottle.wsgi)(4 lines added, 2 lines removed)
  
  
  - [
  bottle/dice.py
  ](https://bitbucket.org/edthedev/edthedev/src/890af0670da9/bottle/dice.py)(0 lines added, 191 lines removed)
  
  
  - [
  bottle/main.css
  ](https://bitbucket.org/edthedev/edthedev/src/890af0670da9/bottle/main.css)(0 lines added, 12 lines removed)
  
  
  - [
  bottle/roller.css
  ](https://bitbucket.org/edthedev/edthedev/src/890af0670da9/bottle/roller.css)(12 lines added, 0 lines removed)
  
  
  - [
  bottle/roller.html
  ](https://bitbucket.org/edthedev/edthedev/src/890af0670da9/bottle/roller.html)(14 lines added, 0 lines removed)
  
  
  - [
  bottle/roller.py
  ](https://bitbucket.org/edthedev/edthedev/src/890af0670da9/bottle/roller.py)(186 lines added, 0 lines removed)
  
  </div>

- [Renamed homepage to dice. Fixed import path.](https://bitbucket.org/edthedev/edthedev/changeset/c7993b64969b)
<div>Renamed homepage to dice. Fixed import path.
  
  
  
  
  
  
  - [
  bottle/bottle.wsgi
  ](https://bitbucket.org/edthedev/edthedev/src/c7993b64969b/bottle/bottle.wsgi)(2 lines added, 2 lines removed)
  
  
  - [
  bottle/dice.py
  ](https://bitbucket.org/edthedev/edthedev/src/c7993b64969b/bottle/dice.py)(191 lines added, 0 lines removed)
  
  
  - [
  bottle/homepage.py
  ](https://bitbucket.org/edthedev/edthedev/src/c7993b64969b/bottle/homepage.py)(0 lines added, 191 lines removed)
  
  
  - [
  bottle/setupbottle
  ](https://bitbucket.org/edthedev/edthedev/src/c7993b64969b/bottle/setupbottle)(3 lines added, 0 lines removed)
  
  </div>

- [Removing old versions of files.](https://bitbucket.org/edthedev/edthedev/changeset/ec061bedb47a)
<div>Removing old versions of files.
  
  
  
  
  
  
  - [
  games/d20.svg
  ](https://bitbucket.org/edthedev/edthedev/src/ec061bedb47a/games/d20.svg)(0 lines added, 177 lines removed)
  
  
  - [
  games/homepage.py
  ](https://bitbucket.org/edthedev/edthedev/src/ec061bedb47a/games/homepage.py)(0 lines added, 191 lines removed)
  
  </div>

- [Moved all bottle.py stuff to its own directory.](https://bitbucket.org/edthedev/edthedev/changeset/c2207918e162)
<div>Moved all bottle.py stuff to its own directory.
  
  
  
  
  
  
  - [
  bottle/bottle
  ](https://bitbucket.org/edthedev/edthedev/src/c2207918e162/bottle/bottle)(1 lines added, 0 lines removed)
  
  
  - [
  bottle/bottle.wsgi
  ](https://bitbucket.org/edthedev/edthedev/src/c2207918e162/bottle/bottle.wsgi)(8 lines added, 0 lines removed)
  
  
  - [
  bottle/cardgame.py
  ](https://bitbucket.org/edthedev/edthedev/src/c2207918e162/bottle/cardgame.py)(122 lines added, 0 lines removed)
  
  
  - [
  bottle/d20.svg
  ](https://bitbucket.org/edthedev/edthedev/src/c2207918e162/bottle/d20.svg)(177 lines added, 0 lines removed)
  
  
  - [
  bottle/homepage.py
  ](https://bitbucket.org/edthedev/edthedev/src/c2207918e162/bottle/homepage.py)(191 lines added, 0 lines removed)
  
  
  - [
  bottle/index.py
  ](https://bitbucket.org/edthedev/edthedev/src/c2207918e162/bottle/index.py)(122 lines added, 0 lines removed)
  
  
  - [
  bottle/main.css
  ](https://bitbucket.org/edthedev/edthedev/src/c2207918e162/bottle/main.css)(12 lines added, 0 lines removed)
  
  
  - [
  bottle/setupbottle
  ](https://bitbucket.org/edthedev/edthedev/src/c2207918e162/bottle/setupbottle)(5 lines added, 0 lines removed)
  
  
  - [
  bottle/starcraftii
  ](https://bitbucket.org/edthedev/edthedev/src/c2207918e162/bottle/starcraftii)(1 lines added, 0 lines removed)
  
  
  - [
  bottle/targetgame
  ](https://bitbucket.org/edthedev/edthedev/src/c2207918e162/bottle/targetgame)(77 lines added, 0 lines removed)
  
  
  - [
  games/homepage.py
  ](https://bitbucket.org/edthedev/edthedev/src/c2207918e162/games/homepage.py)(30 lines added, 2 lines removed)
  
  </div>

- [Added a d20 svg image that appears after rolling. Need to add a number.](https://bitbucket.org/edthedev/edthedev/changeset/fd5fe332c51b)
<div>Added a d20 svg image that appears after rolling. Need to add a number.
  
  
  
  
  
  
  - [
  games/d20.svg
  ](https://bitbucket.org/edthedev/edthedev/src/fd5fe332c51b/games/d20.svg)(177 lines added, 0 lines removed)
  
  
  - [
  games/homepage.py
  ](https://bitbucket.org/edthedev/edthedev/src/fd5fe332c51b/games/homepage.py)(18 lines added, 4 lines removed)
  
  </div>


Digest powered by [RSS Digest](http://www.rssdigestpro.com)
