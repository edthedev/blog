---
layout: post
title: "Hemipiphany"
date: 2009-11-12 16:31:22
link: http://www.edthedev.com/2009/hemipiphany/
categories:
- Random Geekiness
published: true
comments: true
---
<DL>
<DT>
Hemipiphany</DT>
<DD>
The moment when one discovers either the root of the problem or the solution to it, but not both.
</DD>
</DL>