---
layout: post
title: "Castor on troubled waters"
date: 2009-10-12 18:45:26
link: http://www.edthedev.com/2009/castor-on-troubled-waters/
categories:
- Random Geekiness
published: true
comments: true
---
As long as I'm thinking about my absolute favorite podcasts, let me post another that I am sure to recommend in person sometime. 

'Castor on troubled waters' is a grandiose pirate tale that spans decades, but takes just a few minutes to enjoy. If you enjoy a vivid imagination and a clever hero, this could become your favorite fish story just as it became mine!

  http://podcastle.org/2009/08/04/pc064-castor-on-troubled-waters/