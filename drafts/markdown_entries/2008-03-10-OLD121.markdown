---
layout: post
title: "Web scraping with Python"
date: 2008-03-10 13:37:59
link: http://www.edthedev.com/2008/OLD121/
published: true
comments: true
---
This [decent article][1] about grabbing the contents of a web page in Python is worth highlighting here because it is the only mention of this _on the entire internet_. It will point you to this [incredibly insufficient documentation][2], but it will also point you to  this [much more useful documentation][3].
[1]: http://penkin.wordpress.com/2007/11/26/web-scraping-with-python-part-1-http-requests/
[2]: http://wwwsearch.sourceforge.net/mechanize/
[3]: http://wwwsearch.sourceforge.net/ClientForm/