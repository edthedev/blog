---
layout: post
title: "Jump to line with Visual Studio"
date: 2010-04-14 08:35:34
link: http://www.edthedev.com/?p=10817
categories:
- Random Geekiness
published: false
comments: true
---
Ctrl+G works for 'go to line number' in Visual Studio. Who knew?

Some helpful blogger over at MSDN knew: http://blogs.msdn.com/piyush/archive/2007/03/16/useful-visual-studio-shortcut-keys.aspx

Thanks piyushjain, whoever you are!

http://blogs.msdn.com/piyush/archive/2007/03/16/useful-visual-studio-shortcut-keys.aspx