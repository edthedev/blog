---
layout: post
title: "Lost Another Identity"
date: 2008-04-18 15:21:07
link: http://www.edthedev.com/2008/OLD152/
published: true
comments: true
---
Trust is probably the least recognized currency underpinning this nation's economy.

The [Better Business Bureau](http://welcome.bbb.org/), one of the vanguards of that trust, published some statistics from polling identity theft victims. [Javelin Strategy & Research](http://www.javelinstrategy.com/) helped.

One of the unexpected results: Your home is a greater risk to your identity than the business world is.
![Identity Theft Chart](http://edthedev.com/files/bbb-family-id-theft-chart.jpg)

In the 'pat yourself on the back' category, we learn people who use software to track their money tend to lose 1/10 as much money to identity theft than people who use paper.

![Paper Vs Software](http://edthedev.com/files/idtheftloss.gif)

Read the full article [on the BBB website](http://www.bbb.org/ALERTS/article.asp?ID=565).