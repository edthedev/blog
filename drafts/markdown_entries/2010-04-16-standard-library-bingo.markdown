---
layout: post
title: "Standard Library Bingo"
date: 2010-04-16 13:15:41
link: http://www.edthedev.com/2010/standard-library-bingo/
categories:
- Example Code
published: true
comments: true
---
As Google gets worse and worse at seeing through spam to identify standard libraries, I find it compelling to create a cheat sheet. 
Be sure to check the 'last updated' date on this post before you decide to use it. If this hasn't been updated in awhile (because I've joined the first colony ship on it's way to Alpha Centauri), you may want to look for a more recent version.

<dl>
<dt>Convert a string into bytes</dt>
<dd>Java - [String.getBytes("US-ASCII" or "UTF-8")](http://java.sun.com/j2se/1.4.2/docs/api/java/lang/String.html#getBytes(java.lang.String))</dd>
<dd>C# 

<code>System.Text.UTF8Encoding stringToBytes = new System.Text.UTF8Encoding();
Byte[] inputBytes = stringToBytes.GetBytes(input);
</code>

  
</dd>
<dt>Check Endian-ness of your system.</dt>
<dd>Java - Emulates a big endian system </dd>
<dd>Java - Let's you inspect the underlying chip with java.nio.ByteOrder.nativeOrder() </dd>
<dd>C# - System.BitConverter.IsLittleEndian </dd>
<dd>C# - IPAddress.HostToNetworkOrder </dd>
<dt>SHA One way hash</dt>
<dd>C# - System.Security.Cryptography.SHA256Managed</dd>
<dd>Java - [java.security.MessageDigest](http://java.sun.com/j2se/1.4.2/docs/api/java/security/MessageDigest.html)</dd>
</dl>