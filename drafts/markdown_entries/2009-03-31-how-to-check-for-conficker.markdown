---
layout: post
title: "How to Check for Conficker"
date: 2009-03-31 17:51:40
link: http://www.edthedev.com/2009/how-to-check-for-conficker/
categories:
- Security
published: true
comments: true
---
Conficker (also known as DownAndUp) is a virus that is set to 'go off' tomorrow. It has infected millions of computers, but world-wide that isn't a large percentage of total computers. And Conficker seems to be much better at infecting workplace computers than home computers. But even so, it would still be nice to know whether your computer is infected.

Luckily, there's an easy way to test for Conficker. In order to defend itself against removal, Conficker messes up your ability to reach several security expert websites. If you're able to open http://sans.org/ (SANS are good guys with some great information about protecting yourself from viruses) in your web browser, you're not infected. If you can't open it, there could be other causes, but a Conficker infection is a likely explanation.