---
layout: post
title: "Create your own Bootable Ubuntu CD"
date: 2008-03-29 06:24:43
link: http://www.edthedev.com/2008/OLD135/
published: true
comments: true
---
A bootable Ubuntu CD is a great tool to have. It can let you browse the internet safely from a virus infected Windows ME workstation (is there any other kind?). It can give you quick access to a GUI Partition manager (GParted). And if you're tired of Windows ME, it can install Ubuntu for you.

So here's instructions to make your own.

2. Download an ISO image of the [Ubuntu 7][2] or the latest version. Don't worry that you don't know what an ISO is, we'll get to that.
[2]: http://www.ubuntu.com/getubuntu/download
3. Download and install [Infra Recorder for Windows][3]. 
[3]: http://infrarecorder.sourceforge.net/?page_id=5

  Explanation: Infra Recorder lets you use an ISO image file (.iso) to create a CD with very precise contents. The ISO file contains thousands of files, which will be placed in exactly the right places on the CD. Sure, you could just go download every one of those files by hand and place it in just the right place, but don't bother. Infra Recorder and the correct .iso file does this for you.

4. Use Infra Recorder to burn the Ubuntu ISO image to a blank CD. Start with the toolbar button tagged 'Burn ISO image', then browse to the .iso file. Then press 'Ok' a bunch of times without changing any of the default options.
5. The tray will eject automatically when it's finished.
6. Label your Bootable Ubuntu CD.
7. Use your bootable Ubuntu CD for fun and profit.
8. Enjoy!

- Edward