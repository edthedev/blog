---
layout: post
title: "Use VBScript to Detect If .Net 2 is Installed"
date: 2008-11-14 13:01:15
link: http://www.edthedev.com/2008/use-vbscript-to-detect-if-net-2-is-installed/
categories:
- Example Code
published: true
comments: true
---
Coding against the .Net 2.0 framework gives a healthy number of substantial advantages; but when you're deploying to unknown machines, you need a way to tell whether .Net 2.0 is installed; without relying on .Net 2.0 to do the detection itself.

So without further ado, I present a quick VBScript that can do that for you.


<code>

set shell = WScript.CreateObject("WScript.Shell")
sysRoot = shell.ExpandEnvironmentStrings("%systemroot%")
' sysRoot = shell.ExpandEnvironmentStrings("%windir%")
path = sysRoot + "\Microsoft.NET\Framework"
set fs = CreateObject("Scripting.FileSystemObject")
GotDotNet2 = False
On Error Resume Next
set folder = fs.GetFolder(path)
If Err = 0 Then
Set re = new regexp
re.Pattern = "^v2"
For each item in folder.SubFolders
If re.Test(item.Name) Then
GotDotNet2 = True
' Msgbox item.Name
End If
Next
End If
Msgbox GotDotNet2

</code>

Naturally, you might choose to do something with the value in 'GotDotNet2', rather than displaying it.

Enjoy!

- Edward