---
layout: post
title: "Recent Commits February 4, 2012"
date: 2012-02-04 17:30:16
link: http://www.edthedev.com/2012/recent-commits-february-4-2012/
categories:
- Example Code
published: true
comments: false
---
<ul class="scrd_digest">
- [Roller is now portable - you may combine it will other Bottle.py apps running on the same server.](https://bitbucket.org/edthedev/edthedev/changeset/bcb9d3f3973b)
<div>Roller is now portable - you may combine it will other Bottle.py apps running on the same server.
  
  
  
  
  
  
  - [
  bottle/roller.py
  ](https://bitbucket.org/edthedev/edthedev/src/bcb9d3f3973b/bottle/roller.py)(21 lines added, 15 lines removed)
  
  </div>

- [Private dice rolls now appear annonymously in the chat record.
Private dice rolls no longer appear giant sized.](https://bitbucket.org/edthedev/edthedev/changeset/25770c15c4cd)
<div>Private dice rolls now appear annonymously in the chat record.
Private dice rolls no longer appear giant sized.
  
  
  
  
  
  
  - [
  bottle/roller.css
  ](https://bitbucket.org/edthedev/edthedev/src/25770c15c4cd/bottle/roller.css)(1 lines added, 0 lines removed)
  
  
  - [
  bottle/roller.py
  ](https://bitbucket.org/edthedev/edthedev/src/25770c15c4cd/bottle/roller.py)(21 lines added, 17 lines removed)
  
  </div>

- [better homepage](https://bitbucket.org/edthedev/edthedev/changeset/007f5fedb750)
<div>better homepage
  
  
  
  
  
  
  - [
  bottle/bottle.wsgi
  ](https://bitbucket.org/edthedev/edthedev/src/007f5fedb750/bottle/bottle.wsgi)(1 lines added, 0 lines removed)
  
  
  - [
  bottle/cardgame.py
  ](https://bitbucket.org/edthedev/edthedev/src/007f5fedb750/bottle/cardgame.py)(2 lines added, 2 lines removed)
  
  
  - [
  bottle/roller.py
  ](https://bitbucket.org/edthedev/edthedev/src/007f5fedb750/bottle/roller.py)(0 lines added, 1 lines removed)
  
  </div>

- [I do not rfemember...](https://bitbucket.org/edthedev/edthedev/changeset/260740ab70de)
<div>I do not rfemember...
  
  
  
  
  
  
  - [
  bottle/roller.css
  ](https://bitbucket.org/edthedev/edthedev/src/260740ab70de/bottle/roller.css)(13 lines added, 0 lines removed)
  
  
  - [
  bottle/roller.py
  ](https://bitbucket.org/edthedev/edthedev/src/260740ab70de/bottle/roller.py)(11 lines added, 1 lines removed)
  
  </div>

- [No, banana can't grow...alone.](https://bitbucket.org/edthedev/edthedev/changeset/f5aa77b4c9e2)
<div>No, banana can't grow...alone.
  
  
  
  
  
  
  - [
  bottle/roller.py
  ](https://bitbucket.org/edthedev/edthedev/src/f5aa77b4c9e2/bottle/roller.py)(1 lines added, 1 lines removed)
  
  </div>

- [Banana can't grow alone.](https://bitbucket.org/edthedev/edthedev/changeset/868413e1b75d)
<div>Banana can't grow alone.
  
  
  
  
  
  
  - [
  bottle/roller.py
  ](https://bitbucket.org/edthedev/edthedev/src/868413e1b75d/bottle/roller.py)(2 lines added, 2 lines removed)
  
  </div>

- [Switched out input type=image for button with embedded image. Better browser compatability.](https://bitbucket.org/edthedev/edthedev/changeset/2be7e3f24ebc)
<div>Switched out input type=image for button with embedded image. Better browser compatability.
  
  
  
  
  
  
  - [
  bottle/roller.css
  ](https://bitbucket.org/edthedev/edthedev/src/2be7e3f24ebc/bottle/roller.css)(4 lines added, 1 lines removed)
  
  
  - [
  bottle/roller.py
  ](https://bitbucket.org/edthedev/edthedev/src/2be7e3f24ebc/bottle/roller.py)(14 lines added, 3 lines removed)
  
  </div>

- [The timestamps were detracting from play value.
May contemplate adding them back in, in a sparse and un-obtrusive manner...or not.
Maybe as a GM button to add a timestamp.](https://bitbucket.org/edthedev/edthedev/changeset/64a955cf20eb)
<div>The timestamps were detracting from play value.
May contemplate adding them back in, in a sparse and un-obtrusive manner...or not.
Maybe as a GM button to add a timestamp.
  
  
  
  
  
  
  - [
  bottle/roller.py
  ](https://bitbucket.org/edthedev/edthedev/src/64a955cf20eb/bottle/roller.py)(2 lines added, 2 lines removed)
  
  </div>

- [Added private rolls.](https://bitbucket.org/edthedev/edthedev/changeset/860ab1652709)
<div>Added private rolls.
  
  
  
  
  
  
  - [
  bottle/roller.py
  ](https://bitbucket.org/edthedev/edthedev/src/860ab1652709/bottle/roller.py)(15 lines added, 8 lines removed)
  
  </div>


Digest powered by [RSS Digest](http://www.rssdigestpro.com)
