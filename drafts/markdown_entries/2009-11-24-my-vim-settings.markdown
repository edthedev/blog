---
layout: post
title: "My vim settings."
date: 2009-11-24 20:02:54
link: http://www.edthedev.com/2009/my-vim-settings/
categories:
- Programming
- Solutions
published: false
comments: true
---
Because you may ask, here are the typical contents of my .vimrc file.

map! ^[OA ^[ka "For Solaris
map! ^[OB ^[ja
map! ^[OD ^[i
map! ^[OC ^[la
set nocompatible "Don't emulate old vi bugs.
set autoindent " Yay!
set smartindent
set tabstop=4
set shiftwidth=4
set expandtab
set showmatch "Parenthesis matching.
set vb t_vb= "Flash instead of beep.
set ruler
set nohls "Do not highlight search matches.
set incsearch "Search as I type
" Type :help options within vim to get a complete list of options.

set nowrap "Do not wrap long lines of code.
set textwidth=120
set foldmethod=marker

"Place all vim backup files into the user home directory.
set backupdir=~/.backup

If you need to stay up to the minute, I've published a Google Doc here: [http://docs.google.com/Doc?docid=0ASW2zii9ulnyZGdzY2ZyODZfNzJoc2N3dzZncA&hl=en](http://docs.google.com/Doc?docid=0ASW2zii9ulnyZGdzY2ZyODZfNzJoc2N3dzZncA&hl=en)