---
layout: post
title: "Facebook privacy issues"
date: 2010-05-15 00:03:01
link: http://www.edthedev.com/2010/facebook-privacy-issues/
categories:
- Security
published: true
comments: true
---
The article is a little reactionary, but it does serve to highlight the [things about Facebook that you should be concerned about](http://www.wired.com/epicenter/2010/05/facebook-rogue/).

Maybe it's time to join me over at Google Buzz? All seven Google Buzz users are looking forward to your company. ;)