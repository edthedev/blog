---
layout: post
title: "Open Source Electorate"
date: 2010-02-20 15:26:55
link: http://www.edthedev.com/2010/open-source-electorate/
categories:
- Random Geekiness
published: true
comments: true
---
Without mind-reading it's impossible to fully audit the motives and rationality of our elected representatives. And even if we find them lacking, it can be hard to get rid of them or force them to change with the times. And once we've gotten rid of a flawed one, it's hard to find a suitably qualified replacement.

Open source software addresses all of these problems. Because it's open, disagreeing parties can fully examine it's rationale and have an open discussion of possible solutions. Flawed code can be patched or branched. And even when a piece of code is ready for retirement, it's replacement can benefit greatly from reading the original source.

So I propose that we open our governing seats to open source artificial intelligences that can pass the Turing test. The salary normally paid for the position will now be used to purchase a short term support contract.

Update policies, patching schedules and service response times will be determined at election time, by way of campaign promises. And since all eligible candidates must be open source, it will be possible to re-elect the same piece of code, but with a different support company.