---
layout: post
title: "Cool People: Geoffrey K. Pullum"
date: 2010-06-04 17:05:08
link: http://www.edthedev.com/2010/how-dr-seuss-would-teach-the-halting-problem/
categories:
- Programming
published: true
comments: true
---
In a feat of awesome nerd-ness, [Geoffrey K. Pullum](http://ling.ed.ac.uk/~gpullum/index.html)
wrote the [Computability Theory Halting Problem](http://en.wikipedia.org/wiki/Halting_problem) [described in Dr. Seuss style](http://ebiquity.umbc.edu/blogger/2008/01/19/how-dr-suess-would-prove-the-halting-problem-undecidable/).