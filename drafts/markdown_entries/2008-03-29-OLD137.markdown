---
layout: post
title: "Enlightenment through Ubuntu"
date: 2008-03-29 14:07:35
link: http://www.edthedev.com/2008/OLD137/
published: true
comments: true
---
I found a [great article][1] about installing the [Enlightenment window manager][2] on Ubuntu.

[1]: http://www.howtogeek.com/howto/ubuntu/install-enlightenment-on-ubuntu-linux/
[2]: http://en.wikipedia.org/wiki/Enlightenment_(window_manager)