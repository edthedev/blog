---
layout: post
title: "Calling Stored Procedures from Python"
date: 2008-03-13 14:13:50
link: http://www.edthedev.com/2008/OLD125/
published: true
comments: true
---
[Catherine Devlin][1] and [Paul Moore][2] wrote a very good explanation of how to call an Oracle stored procedure from Python using [cx_Oracle][21]. You can read it [at this link][1].

[1]: http://catherinedevlin.blogspot.com/2006/04/stored-procedures-from-cxoracle.html
[2]: http://arkenstone.blogspot.com/
[21]: http://www.python.net/crew/atuining/cx_Oracle/

Rather than sending you searching, I'll gladly summarize and elaborate here. As a bonus, I'll give you longer examples in case you're searching for a correction to a minor detail that isn't working. If you find my examples too confusing, check [Catherine Devlin's article][1], it has just the important bits of code.

Here's an example of calling a stored procedure named 'InsertUser' that takes two variables inName, an input variable, and outUserId, an output variable.


import cx_Oracle
dsn = cx_Oracle.makedsn("myServer.com", 1521, "myDbName")
conn = cx_Oracle.Connection("%s/%s@%s" % ("USER", "PASSWORD", dsn))
cur = conn.cursor()
IdReturned = 0 # This tells cx_Oracle that we're expecting a number.

# And here is the line of code that does the heavy lifting:
nameReturned, IdReturned = cur.callproc('insertUser',['nameToInsert', IdReturned ]')

# Oddly, cx_Oracle will populate nameReturned with 'nameToInsert'.
# But we do not care. It's IdReturned that we want.
cur.close()
conn.commit() # If you're following along in an interactive session, you won't see any results at your database until this call happens.
conn.close()



If you need the rest of the picture, the Stored Procedure might look like this:


create or replace PROCEDURE INSERTUSER
( inName IN VARCHAR2,
  outUserId OUT Number
) AS
BEGIN
  select USERIDSEQ.nextval into outUserId from dual;
  insert into USERS (User_Id, Name) values (outUserId, inName);
END INSERTUSER;



And the sequence, USERIDSEQ, referenced inside INSERTUSER, might look like this:


CREATE SEQUENCE  "myDbName"."USERIDSEQ"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER NOCYCLE;

