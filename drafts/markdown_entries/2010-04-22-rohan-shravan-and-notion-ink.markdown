---
layout: post
title: "Rohan Shravan and Notion Ink"
date: 2010-04-22 18:30:04
link: http://www.edthedev.com/2010/rohan-shravan-and-notion-ink/
categories:
- Random Geekiness
published: true
comments: true
---
Rohan Shravan is a dreamer and a visionary. As I learn more about him from his blog, I become more excited about the Notion Ink Adam, and what else Rohan may come up with. Is it too early to declare him the next Steve Jobs?

"Doesn’t matter if I am not able to realize my dreams. I can do something for everyone else out there. Do something for a kid to learn music, challenge gravity. Let students see reactions, smile in awe when they look at such wonderful engineering creations of man. Let engineers test their concepts. And let people like me spend time in communication. One device which can help many without telling them it’s helping. One device which quietly slips into their lives and becomes their best friend." - Rohan Shravan

from his blog post at: 
http://notionink.wordpress.com/2010/02/18/where-did-it-all-start-anyway/#comments