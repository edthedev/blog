---
layout: post
title: "Recent Commits December 17, 2011"
date: 2011-12-17 17:42:41
link: http://www.edthedev.com/2011/recent-commits-december-17-2011/
categories:
- Example Code
published: true
comments: false
---
<ul class="scrd_digest">
- [Live updates to minecraft scripts, copied from the minecraft server.](https://bitbucket.org/edthedev/edthedev/changeset/16f013f82465)
<div>Live updates to minecraft scripts, copied from the minecraft server.
  
  
  
  
  
  
  - [
  minecraft/backup_worlds
  ](https://bitbucket.org/edthedev/edthedev/src/16f013f82465/minecraft/backup_worlds)(20 lines added, 0 lines removed)
  
  
  - [
  minecraft/make_maps
  ](https://bitbucket.org/edthedev/edthedev/src/16f013f82465/minecraft/make_maps)(3 lines added, 7 lines removed)
  
  
  - [
  minecraft/merge-minecraft-worlds
  ](https://bitbucket.org/edthedev/edthedev/src/16f013f82465/minecraft/merge-minecraft-worlds)(72 lines added, 0 lines removed)
  
  
  - [
  minecraft/minecraft
  ](https://bitbucket.org/edthedev/edthedev/src/16f013f82465/minecraft/minecraft)(1 lines added, 5 lines removed)
  
  </div>

- [Updated map function.](https://bitbucket.org/edthedev/edthedev/changeset/4289c1b4afa5)
<div>Updated map function.
  
  
  
  
  
  
  - [
  minecraft/make_maps
  ](https://bitbucket.org/edthedev/edthedev/src/4289c1b4afa5/minecraft/make_maps)(7 lines added, 3 lines removed)
  
  </div>

- [Nicer droid backup scripts.](https://bitbucket.org/edthedev/edthedev/changeset/3ab5e5490896)
<div>Nicer droid backup scripts.
  
  
  
  
  
  
  - [
  android/backup-droid-full
  ](https://bitbucket.org/edthedev/edthedev/src/3ab5e5490896/android/backup-droid-full)(1 lines added, 1 lines removed)
  
  
  - [
  android/backup-droid-quick
  ](https://bitbucket.org/edthedev/edthedev/src/3ab5e5490896/android/backup-droid-quick)(4 lines added, 5 lines removed)
  
  
  - [
  android/pull-droid-pictures
  ](https://bitbucket.org/edthedev/edthedev/src/3ab5e5490896/android/pull-droid-pictures)(1 lines added, 0 lines removed)
  
  
  - [
  android/sync-droid-music
  ](https://bitbucket.org/edthedev/edthedev/src/3ab5e5490896/android/sync-droid-music)(10 lines added, 6 lines removed)
  
  </div>

- [Removed all org mode code. See http://bitbucket.org/edthedev/org.py](https://bitbucket.org/edthedev/edthedev/changeset/e32d493782d1)
<div>Removed all org mode code. See http://bitbucket.org/edthedev/org.py
  
  
  
  
  
  
  - [
  edthedev/org-web
  ](https://bitbucket.org/edthedev/edthedev/src/e32d493782d1/edthedev/org-web)(0 lines added, 7 lines removed)
  
  
  - [
  edthedev/org.py
  ](https://bitbucket.org/edthedev/edthedev/src/e32d493782d1/edthedev/org.py)(0 lines added, 883 lines removed)
  
  
  - [
  org/aliases
  ](https://bitbucket.org/edthedev/edthedev/src/e32d493782d1/org/aliases)(0 lines added, 30 lines removed)
  
  
  - [
  org/archive
  ](https://bitbucket.org/edthedev/edthedev/src/e32d493782d1/org/archive)(0 lines added, 26 lines removed)
  
  
  - [
  org/asciidown
  ](https://bitbucket.org/edthedev/edthedev/src/e32d493782d1/org/asciidown)(0 lines added, 30 lines removed)
  
  
  - [
  org/cleanFileName
  ](https://bitbucket.org/edthedev/edthedev/src/e32d493782d1/org/cleanFileName)(0 lines added, 26 lines removed)
  
  
  - [
  org/done
  ](https://bitbucket.org/edthedev/edthedev/src/e32d493782d1/org/done)(0 lines added, 38 lines removed)
  
  
  - [
  org/due
  ](https://bitbucket.org/edthedev/edthedev/src/e32d493782d1/org/due)(0 lines added, 21 lines removed)
  
  
  - [
  org/fixtags
  ](https://bitbucket.org/edthedev/edthedev/src/e32d493782d1/org/fixtags)(0 lines added, 39 lines removed)
  
  
  - [
  org/hud
  ](https://bitbucket.org/edthedev/edthedev/src/e32d493782d1/org/hud)(0 lines added, 4 lines removed)
  
  
  - [
  org/hud.cgi
  ](https://bitbucket.org/edthedev/edthedev/src/e32d493782d1/org/hud.cgi)(0 lines added, 40 lines removed)
  
  
  - [
  org/journal
  ](https://bitbucket.org/edthedev/edthedev/src/e32d493782d1/org/journal)(0 lines added, 12 lines removed)
  
  
  - [
  org/logit
  ](https://bitbucket.org/edthedev/edthedev/src/e32d493782d1/org/logit)(0 lines added, 53 lines removed)
  
  
  - [
  org/merge
  ](https://bitbucket.org/edthedev/edthedev/src/e32d493782d1/org/merge)(0 lines added, 80 lines removed)
  
  
  - [
  org/monday
  ](https://bitbucket.org/edthedev/edthedev/src/e32d493782d1/org/monday)(0 lines added, 26 lines removed)
  
  
  - [
  org/new_file
  ](https://bitbucket.org/edthedev/edthedev/src/e32d493782d1/org/new_file)(0 lines added, 42 lines removed)
  
  
  - [
  org/newnote
  ](https://bitbucket.org/edthedev/edthedev/src/e32d493782d1/org/newnote)(0 lines added, 51 lines removed)
  
  
  - [
  org/next
  ](https://bitbucket.org/edthedev/edthedev/src/e32d493782d1/org/next)(0 lines added, 115 lines removed)
  
  
  - [
  org/org
  ](https://bitbucket.org/edthedev/edthedev/src/e32d493782d1/org/org)(0 lines added, 145 lines removed)
  
  
  - [
  org/org.html
  ](https://bitbucket.org/edthedev/edthedev/src/e32d493782d1/org/org.html)(0 lines added, 72 lines removed)
  
  
  - [
  org/org_browse
  ](https://bitbucket.org/edthedev/edthedev/src/e32d493782d1/org/org_browse)(0 lines added, 2 lines removed)
  
  
  - [
  org/projects
  ](https://bitbucket.org/edthedev/edthedev/src/e32d493782d1/org/projects)(0 lines added, 71 lines removed)
  
  
  - [
  org/sample
  ](https://bitbucket.org/edthedev/edthedev/src/e32d493782d1/org/sample)(0 lines added, 24 lines removed)
  
  
  - [
  org/status
  ](https://bitbucket.org/edthedev/edthedev/src/e32d493782d1/org/status)(0 lines added, 33 lines removed)
  
  
  - [
  org/summary
  ](https://bitbucket.org/edthedev/edthedev/src/e32d493782d1/org/summary)(0 lines added, 70 lines removed)
  
  
  - [
  org/tagcloud
  ](https://bitbucket.org/edthedev/edthedev/src/e32d493782d1/org/tagcloud)(0 lines added, 59 lines removed)
  
  
  - [
  org/textbar
  ](https://bitbucket.org/edthedev/edthedev/src/e32d493782d1/org/textbar)(0 lines added, 50 lines removed)
  
  
  - [
  org/weekend
  ](https://bitbucket.org/edthedev/edthedev/src/e32d493782d1/org/weekend)(0 lines added, 3 lines removed)
  
  
  - [
  org/wordpress.py
  ](https://bitbucket.org/edthedev/edthedev/src/e32d493782d1/org/wordpress.py)(0 lines added, 46 lines removed)
  
  
  - [
  org/wordpresslib.py
  ](https://bitbucket.org/edthedev/edthedev/src/e32d493782d1/org/wordpresslib.py)(0 lines added, 379 lines removed)
  
  </div>

- [Added the ability to search from the command line, in theory.](https://bitbucket.org/edthedev/edthedev/changeset/b41386fc18a0)
<div>Added the ability to search from the command line, in theory.
  
  
  
  
  
  
  - [
  config/roku
  ](https://bitbucket.org/edthedev/edthedev/src/b41386fc18a0/config/roku)(13 lines added, 4 lines removed)
  
  </div>

- [Automerge.](https://bitbucket.org/edthedev/edthedev/changeset/4e6871e39f69)
<div>Automerge.
  
  
  
  
  
  
  - [
  config/cleanup-all-the-downloads
  ](https://bitbucket.org/edthedev/edthedev/src/4e6871e39f69/config/cleanup-all-the-downloads)(1 lines added, 0 lines removed)
  
  
  - [
  edthedev/org.py
  ](https://bitbucket.org/edthedev/edthedev/src/4e6871e39f69/edthedev/org.py)(31 lines added, 21 lines removed)
  
  
  - [
  org/org
  ](https://bitbucket.org/edthedev/edthedev/src/4e6871e39f69/org/org)(5 lines added, 7 lines removed)
  
  
  - [
  text/display_missing
  ](https://bitbucket.org/edthedev/edthedev/src/4e6871e39f69/text/display_missing)(2 lines added, 2 lines removed)
  
  </div>

- [Added some more information for future upgrades.
Thank you to http://a-more-common-hades.blogspot.com/2011/07/control-roku-from-command-line.html](https://bitbucket.org/edthedev/edthedev/changeset/d3a302cd4130)
<div>Added some more information for future upgrades.
Thank you to http://a-more-common-hades.blogspot.com/2011/07/control-roku-from-command-line.html
  
  
  
  
  
  
  - [
  config/roku
  ](https://bitbucket.org/edthedev/edthedev/src/d3a302cd4130/config/roku)(59 lines added, 0 lines removed)
  
  </div>

- [Added a script that controls a local roku player.](https://bitbucket.org/edthedev/edthedev/changeset/599582886363)
<div>Added a script that controls a local roku player.
  
  
  
  
  
  
  - [
  config/aero_snap_left
  ](https://bitbucket.org/edthedev/edthedev/src/599582886363/config/aero_snap_left)(5 lines added, 1 lines removed)
  
  
  - [
  config/roku
  ](https://bitbucket.org/edthedev/edthedev/src/599582886363/config/roku)(29 lines added, 0 lines removed)
  
  </div>

- [Added aero snap commands.](https://bitbucket.org/edthedev/edthedev/changeset/f83d62dfa926)
<div>Added aero snap commands.
  
  
  
  
  
  
  - [
  config/aero_snap_full
  ](https://bitbucket.org/edthedev/edthedev/src/f83d62dfa926/config/aero_snap_full)(14 lines added, 0 lines removed)
  
  
  - [
  config/aero_snap_left
  ](https://bitbucket.org/edthedev/edthedev/src/f83d62dfa926/config/aero_snap_left)(16 lines added, 0 lines removed)
  
  
  - [
  config/aero_snap_right
  ](https://bitbucket.org/edthedev/edthedev/src/f83d62dfa926/config/aero_snap_right)(14 lines added, 0 lines removed)
  
  
  - [
  minecraft/copy-worlds-from
  ](https://bitbucket.org/edthedev/edthedev/src/f83d62dfa926/minecraft/copy-worlds-from)(14 lines added, 5 lines removed)
  
  </div>


Digest powered by [RSS Digest](http://www.rssdigestpro.com)
