---
layout: post
title: "Enable Window&#039;s Wireless connection manager"
date: 2008-03-28 21:25:49
link: http://www.edthedev.com/2008/OLD133/
published: true
comments: true
---
I had a laptop that stopped connecting properly to Wireless networks. After some research, I discovered that my 'Wireless Zero Configuration' Windows service was not starting properly. Whenever I started it manually, it worked fine. I no longer use that laptop, but I have learned since then that the problem I saw is not uncommon in Windows XP laptops.

If you're having the same issue, here's instructions to start yours manually, and to set it to (hopefully) start automatically on it's own in the future.

![1](http://edthedev.com/files/start_control_panel.jpg)

1. Use the Start Menu to open the 'Control Panel'.

 ![2](http://edthedev.com/files/control_panel_0.jpg)

2. From the Control Panel, open 'Administrative Tools'.

 ![3](http://edthedev.com/files/administrative_tools.jpg)

3. From Adminstrative Tools, open 'Services'.

 ![4](http://edthedev.com/files/services.jpg)

4. In Services, scroll down and double click 'Wireless Zero Configuration'.

 ![5](http://edthedev.com/files/wireless_zero_config.jpg)

5. In the Wireless Zero Configuration properties window,
  Change 'Startup Type' to 'Automatic' and press the 'Start' button if it is enabled (clickable).
  Then press Ok.

6. Close the various windows, and give your wireless connection another try.

Enjoy!

- Edward