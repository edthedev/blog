---
layout: post
title: "Documenting Python"
date: 2010-03-05 13:20:22
link: http://www.edthedev.com/?p=10707
categories:
- Random Geekiness
published: false
comments: true
---
### Useful reference

[http://www.cv.nrao.edu/~rduplain/pydoc/pydoctest/myclass.py](http://www.cv.nrao.edu/~rduplain/pydoc/pydoctest/myclass.py)

### Class declaration

<code> </code>

<code>

class ClassName(object):
  """
  Does what it does.

  Copyright
  License 

  @author

  Last updated by $Author$ (on $Date$)

  @version $Revision$ 

  Subversion URL: $URL$
  """
  __version__ = "$Revision$"

</code>