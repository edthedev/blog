---
layout: post
title: "Google Playground"
date: 2010-03-27 07:22:48
link: http://www.edthedev.com/2010/google-knows-how-to-play/
categories:
- Example Code
- Programming
published: true
comments: true
---
The Google code playground gets it exactly right. The panel in the upper left is for exploring their example code. The panel to it's right is for both reading and *tweaking* their example code. The panel on the bottom is for running it and seeing the output, including whatever changes you have made.
Well played Google, well played!
http://code.google.com/apis/ajax/playground/