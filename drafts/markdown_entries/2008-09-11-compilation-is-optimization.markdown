---
layout: post
title: "Compilation is Optimization"
date: 2008-09-11 11:09:58
link: http://www.edthedev.com/2008/compilation-is-optimization/
categories:
- Programming
published: true
comments: true
---
"Compilation will eventually come to be seen for what it is: merely an optimization tool, and one whose use is almost always premature." - Conway, in a [DevSource article about scripting languages](http://www.devsource.com/c/a/Languages/The-State-of-the-Scripting-Universe/2/).

This is one case where the old ways are not the best ways. I still encounter those who consider compiled languages somehow naturally superior to dynamic languages. But today the vast majority of expense in development projects is in the development time; not the execution time of the code. So languages that must be compiled in order to test are significantly inferior, economically speaking. It's nice to see this getting talked about.

Van Rossum says what I have been thinking, "There are, of course, areas where raw machine speed is too important to program directly in Python. This is typically addressed by coding a carefully chosen small portion of an application (say, 10-30%) in a lower-level language (like C or C++)."

As Conway says "Languages like Perl and Python and PHP are what hold their IT infrastructure together. They're the tools that allow organizations to move data between databases, between formats, between applications, between platforms."
A great deal of the problems that developers solve are getting data from one almost-but-not-perfect application to another. In these cases, scripting languages are the only budget-friendly solution.

And Conway very aptly wraps up my feelings on the subject, "If I could change one thing it would be the misperception that these languages are toys; that they're not suitable for "serious" development. There is any amount of evidence to the contrary. But the stereotype persists, and it keeps too many programmers from being able to use these extraordinary tools that would make their jobs easier, and their products better."