---
layout: post
title: "Free your thoughts with Freemind (for free!)"
date: 2008-04-30 19:46:37
link: http://www.edthedev.com/2008/OLD164/
published: true
comments: true
---
Although I use a [TiddlyWiki](http://www.tiddlywiki.com/) as my personal organizer, I lately find myself more and more dependant on [Freemind](http://freemind.sourceforge.net/wiki/index.php/Main_Page) for brainstorming and mind-dumping. It's especially useful when a thought process is feeling 'blocked' but won't leave me alone.

If you're not already familiar with mind mapping, check out [How to Mind Map](http://www.peterrussell.com/MindMaps/HowTo.php).

Mostly I use Mind Mapping as a 'write only' operation. I often don't even save the file when I'm finished. But my wife has gotten quite good at using it as a communication tool - sharing entire clouds of thought through a single file uploaded to our shared wiki space.

- Edward