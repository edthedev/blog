---
layout: post
title: "How I play World of Warcraft"
date: 2007-12-12 18:32:57
link: http://www.edthedev.com/2007/OLD79/
published: true
comments: true
---
I often feel a little bit less than heroic when I'm playing World of Warcraft.

<img height=500 alt="A warrior returns from his quest with the wrong item. Finding this out, he tells the elf to 'Dig through the bag and find something you like.'" src="http://www.edthedev.com/files/wowquest.png"  tag="Dig through the bag">