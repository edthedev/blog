---
layout: post
title: "WMI Explorer"
date: 2008-03-25 14:04:34
link: http://www.edthedev.com/2008/OLD131/
published: true
comments: true
---
The Scripting Guys at Microsoft have created an amazingly vital and under-advertised PowerShell script that lets you interactively explore the WMI. 
Download it [here][1]

[1]: http://thepowershellguy.com/blogs/posh/archive/2007/03/22/powershell-wmi-explorer-part-1.aspx

Then copy it somewhere convenient.
Then run it from your PowerShell prompt: <code>./WMIExplorer.ps1</code>


Now, you probably want to start by double clicking ROOT\CIMV2, the best stuff is in there.