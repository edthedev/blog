---
layout: post
title: "WPA on Ubuntu at Illinois"
date: 2009-12-13 20:55:56
link: http://www.edthedev.com/2009/wpa-on-ubuntu-at-illinois/
categories:
- Security
- Solutions
published: true
comments: true
---
Continuing with articles about connecting to the network at the University of Illinois from an Ubuntu computer, here's a hint about connecting to the UiWpa2 / IllinoisNet Wireless:  Ubuntu may not automatically choose a certificate authority for you; you may need to browse for a CA certificate.

Choose the Thawte Premium Server CA - it will work at Illinois (and quite a lot of other places, actually):
/etc/ssl/certs/Thawte_Premium_Server_CA.pem

As usual, enter your NetId for your username, and you can use your Active Directory password.

If you're still having trouble getting online, check out the [CITES Homepage](http://cites.illinois.edu) for more information on Illinois network resources.