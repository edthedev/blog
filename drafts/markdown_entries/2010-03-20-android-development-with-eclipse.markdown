---
layout: post
title: "Android Development with Eclipse"
date: 2010-03-20 21:28:06
link: http://www.edthedev.com/2010/android-development-with-eclipse/
categories:
- Example Code
- Programming
- Solutions
published: true
comments: true
---
Here's a guide to get you started developing Android applications with Eclipse.
The whole process took me about an hour tonight.

Step 1: Get Eclipse setup for Android development:
[Installing the Eclipse Android Plugin](http://developer.android.com/guide/developing/eclipse-adt.html)

Step 2: Write 'Hello World':
[Hello World for Android with Eclipse](http://developer.android.com/guide/tutorials/hello-world.html)

Step 3: Bask in the aura of how much nicer Java is than Objective-C, and how much simpler it is to read two web pages than watch 15 videos through iTunes.

It's hard to express how nice a first impression the Android plugin for Eclipse makes. It's a much nicer experience to work with than XCode. Of course, so is getting poked in the eye by a monkey; so not the most relevant comparison.