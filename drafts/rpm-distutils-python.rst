rpm distutils python
====================
:date: 2013-07-29

TODO: Update this with the additional configuration options for pre and preun commands, etc.

For the RPM file, the .spec explodes the source tarball into a build directory::

    %setup

For the RPM file, the .spec repeats the::

    configure 
    make install

commands from it's own build root.
(See %configure in the .spec file)

In the appname.spec file::

    %build
    %configure
    %{__make}


    %install
    %{__rm} -rf %{buildroot}
    %{__make} DESTDIR=%{buildroot} install

This is equivalent to::

    %build
    ./configure
    make

    %install
    rm -rf %{buildroot}
    make DESTDIR=%{buildroot} install

These steps are done to re-create the build root when the RPM is inflated.
If I understand correctly, RPM does the actual install from the build root to the final destination afterward.

Manually with distutils - set prefix and one other variable in the .spec file...

Idea::
    
    sdg_update_makefiles

OR::

    Python_compile_recursive 

which can be run by a single makefile in the top directory.

OR even better::

    ????

which is a standard industry command and can be run by the .spec file from the top directory with a single makefile...


