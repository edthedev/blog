software security blog
======================
:date: 2013-03-15

I have been reading 'Software Security: Building Security In' by Gary McGraw.

Three reasons that software security is growing as a problem, instead of shrinking:
    1. Connectivity - More connected systems means more attack vectors on each system. 
    2. Extensible systems - If the core isn't vulnerable, a plugin may still be.
    3. Complexity - Computer systems do more than they used to, and they are more complex as a result. More complexity yields more oversights, which can be vulnerabilities.

"The notion of a perimiter is quaint, outdated, and too simple to work."
Applications need to be able to defend themselves against eachother to a reasonable degree, and not rely exclusively on the assumption of a clean-room environment within the data center.


