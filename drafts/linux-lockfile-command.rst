The lockfile command
===========================
:date: 2013-02-14

One of the dangers of automation is that a script can potentially get started before the previous run of the same script finished. 

The lockfile command does a nice job preventing this problem.::

	10 * * * * lockfile -l 3600 /tmp/cron_script.lock && /services/service/bin/script && rm -f /tmp/cron_script.lock



