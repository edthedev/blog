blog entry good doing
----------------------
:status: draft
Created 2012-03-08

Things that I have found worth giving to.

TODO: Add links...

Project Goals
--------------
 - Free college mentoring for people whose parents did not attend college.
 - Progressively addresses needs using a metric that will always affect those with the need.

World Vision
-------------
 - Invests in communities in developing nations, where children have poor survival odds.
 - Sponsorship is whole life until adulthood. 
 - Kids that my parents sponsored have stayed in touch, graduated colleges, and become doctors and engineers.
 - My family's tradition: sponsor one kid per own child and one per grandkid.

Kiva.org microloans 
---------------------
 - personal experience of 100% repayment rate so far.
 - Investments are often in production capital (i.e. a cow) that will continue producing in the community for many years.

DonorsChoose.og
----------------
 - Similar to Kiva - pick an individual need and contribute to it.
 - It is possible to find classrooms with needs within your own local school district.

